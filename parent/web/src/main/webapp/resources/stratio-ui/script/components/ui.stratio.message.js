'use strict';

angular
	.module('StratioUI.components.message', [])
	.directive('stMessage', stMessage);

stMessage.$inject = ['TEMPLATE_URL'];
function stMessage(TEMPLATE_URL){
	var directive = {
		restrict: 'E',
		scope: {
			'type': '@',
			'visible': '@',
			'title': '@',
			'message': '@'
		},
		templateUrl: TEMPLATE_URL('components', 'message'),
		transclude: true,
		replace: true,
		link: link
	};

	return directive;

	function link(scope, element, attrs, ctrl, $transclude){
		scope.$watch(
			function(){return element.text().length;},
			function(){
				scope.hasTransclude = element.find('[ng-transclude]').text().replace(/[\n\r\t ]+/g, '') != '';
			});
    }
}
