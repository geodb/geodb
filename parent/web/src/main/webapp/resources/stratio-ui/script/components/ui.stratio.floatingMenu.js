'use strict';

angular
	.module('StratioUI.components.floatingMenu',[])
	.directive('stFloatingMenu', stFloatingMenu);

stFloatingMenu.$inject = ['TEMPLATE_URL', 'stToggleFloatingElement'];
function stFloatingMenu(TEMPLATE_URL, stToggleFloatingElement){

	var directive = {
		restrict: 'AE',
		require: 'ngModel',
		scope: {
			'toggleId': "@",
			'align': "@"
		},
		templateUrl: TEMPLATE_URL('components', 'floatingMenu'),
		transclude: true,
		replace: true,
		controller: stToggleFloatingElement
	};

	return directive;

}
