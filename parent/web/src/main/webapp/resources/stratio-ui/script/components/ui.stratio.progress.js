'use strict';

/**
 * @example  <st-progress value="10" max="50"></st-progress>
 */

angular
	.module('StratioUI.components.progress', [])
	.directive('stProgress', stProgress);

stProgress.$inject = ['TEMPLATE_URL', '$timeout'];
function stProgress(TEMPLATE_URL, $timeout){
	var directive = {
		restrict: 'E',
		scope: {
			"value": "=",
			"max": "@",
			"error" : "=",
			"variant" : "@",
			"onlyProgress" : "@"
		},
		templateUrl: TEMPLATE_URL('components', 'progress'),
		link: link,
		replace: true
	};

	return directive;

	function link(scope, element, attributes, ctrl){

		var inProgress = false;
		var maxValue = attributes.max || 100;
		var componentLoaded = false;
		
		scope.progressControl = false;

		if(typeof attributes.onlyProgress !== 'undefined'){
			scope.$watch('onlyProgress', function(newValue, oldValue){
				if(!inProgress && componentLoaded){
					scope.progressControl = maxValue;
					inProgress = true;
					setBackgroundFade(true);

					$timeout(function(){
						setElementTransition(false);
						scope.progressControl = 0;
						setBackgroundFade(false);
					}, 340);

					$timeout(function(){
						setElementTransition(true);
						scope.progressControl = false;
						inProgress = false;
					}, 360);
				}else{
					componentLoaded = true;
				}
			});
		}
		
		function setElementTransition(active){
			element.toggleClass('st-progress--no-transition', active !== true);
		}
		function setBackgroundFade(active){
			element.toggleClass('st-progress--background-fade', active);
		}
	}
}
