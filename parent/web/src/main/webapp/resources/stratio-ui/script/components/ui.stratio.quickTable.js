'use strict';

angular
	.module('StratioUI.components.quickTable',[])
	.directive('stQuickTable', stQuickTable);

stQuickTable.$inject = ['TEMPLATE_URL'];
function stQuickTable(TEMPLATE_URL){
	var directive = {
		restrict: 'AE',
		scope: {
			'data': '=',
			'scrollbar': '@'
		},
		templateUrl: TEMPLATE_URL('components', 'quickTable'),
		replace: true
	};

	return directive;
}
