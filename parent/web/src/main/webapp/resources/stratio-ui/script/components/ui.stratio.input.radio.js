'use strict';

/**
 * @example  <st-input-radio ng-model="foo" value="value"> <!-- transclude --> </st-input-radio>
 */

angular
	.module('StratioUI.components.input.radio', [])
	.directive('stInputRadio', stInputRadio);

stInputRadio.$inject = ['TEMPLATE_URL'];
function stInputRadio(TEMPLATE_URL){
	var directive = {
		restrict: 'E',
		scope: {
			"ngModel": "=",
			"value": "@"
		},
		templateUrl: TEMPLATE_URL('components', 'input.radio'),
		transclude: true,
		replace: true
	};

	return directive;
}
