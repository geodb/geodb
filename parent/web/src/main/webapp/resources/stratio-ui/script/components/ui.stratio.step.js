'use strict';

angular
	.module('StratioUI.components.step', [])
	.directive('stStep', stStep);

stStep.$inject = ['TEMPLATE_URL'];
function stStep(TEMPLATE_URL){
	var directive = {
		restrict: 'E',
		scope: true,
		templateUrl: TEMPLATE_URL('components', 'step'),
		transclude: true,
		replace: true
	};

	return directive;
}
