'use strict';

/**
 * @example  <st-input-switch ng-model="foo" required> <!-- transclude --> </st-input-switch>
 */

angular
	.module('StratioUI.components.input.switch', [])
	.directive('stInputSwitch', stInputSwitch);

stInputSwitch.$inject = ['TEMPLATE_URL'];
function stInputSwitch(TEMPLATE_URL){
	var directive = {
		restrict: 'E',
		scope: {
			"ngModel": "=",
			"required": "@"
		},
		templateUrl: TEMPLATE_URL('components', 'input.switch'),
		transclude: true,
		replace: true
	};

	return directive;
}
