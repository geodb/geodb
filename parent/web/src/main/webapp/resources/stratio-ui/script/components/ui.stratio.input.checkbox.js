'use strict';

/**
 * @example  <st-input-checkbox ng-model="foo" required> <!-- transclude --> </st-input-checkbox>
 */

angular
	.module('StratioUI.components.input.checkbox', [])
	.directive('stInputCheckbox', stInputCheckbox);

stInputCheckbox.$inject = ['TEMPLATE_URL'];
function stInputCheckbox(TEMPLATE_URL){
	var directive = {
		restrict: 'E',
		scope: {
			"ngModel": "=",
			"required": "@"
		},
		templateUrl: TEMPLATE_URL('components', 'input.checkbox'),
		transclude: true,
		replace: true
	};

	return directive;
}
