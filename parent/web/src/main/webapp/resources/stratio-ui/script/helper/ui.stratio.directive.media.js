(function(){

'use strict';

angular
	.module('StratioUI.directive.media', [])
	.directive('stMedia', stMedia);

function stMedia(){
	var directive = {
		restrict: 'A',
		scope: false,
		link: link
	};

	return directive;

	function link(scope, element, attributes, controller){
		var mediaQueries = {
			"big": '(min-width: 1560px)'
		}

		var config = scope.$eval(attributes.stMedia);

		angular.forEach(config, function(className, query){
			var notQuery = query.split('!');
			var isNot = false;

			if(notQuery.length == 2){
				query = notQuery.pop();
				isNot = true;
			}
			if(!mediaQueries[query]){
				return;
			}

			var mq = matchMedia(mediaQueries[query]);

			mq.addListener(checkMediaQuery);
			checkMediaQuery(mq);

			function checkMediaQuery(mql){
				element.toggleClass(className, mql.matches !== isNot);
			}
		});

	}
}

})();
