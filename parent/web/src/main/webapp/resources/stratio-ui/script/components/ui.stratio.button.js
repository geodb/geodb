(function($){

'use strict';

/**
 * @attribute variant (optional) [secondary | gray | transparent]
 * @attribute icon-right (optional)
 * @attribute icon (optional) class of font-icon
 * @attribute loading (optional) [true | false]
 * @example <st-button variant="secondary" icon="icon-info" icon-right> Message </st-button>
 */

angular
	.module('StratioUI.components.button', [])
	.directive('stButton', stButton);

stButton.$inject = ['TEMPLATE_URL', 'stPassAllAttributes', '$timeout', '$animate'];
function stButton(TEMPLATE_URL, stPassAllAttributes, $timeout, $animate){
	var directive = {
		restrict: 'E',
		scope: true,
		templateUrl: TEMPLATE_URL('components', 'button'),
		transclude: true,
		replace: true,
		link: link
	};
	
	return directive;

	function link(scope, elem, attrs, ctrl, transclude){
		var loadingControl = new LoadingControl(elem);

		$animate.enabled(elem, false);

		stPassAllAttributes(scope, elem, attrs);

		transclude(function(content){
			scope.hasTransclude = !!content.length;
		})
		
		elem[0].className += ' no-transition';

		setTimeout(function(){
			elem[0].className = elem[0].className.split('no-transition').join('');
		}, 100);
		

		loadingControl.toggle(scope.$eval(attrs.loading));

		scope.$watch(function(){return attrs.loading}, function(newValue, oldValue) {
			loadingControl.toggle(scope.$eval(newValue));
		});
    }

    function LoadingControl(elem){
    	var self = this;

    	var classNameLoading = 'st-button--loading';
    	var classNameLoadingHidden = 'st-button--loading-hidden';

    	var lastTimeout = null;

    	self.elem = elem;

    	self.toggle = function(visible){
    		if(visible){
    			clearTimeout(lastTimeout);
    			setVisibles(true, false);
    		}else{
    			setVisibles(true, true);

    			lastTimeout = setTimeout(function(){
    				setVisibles(false, false);
    			}, 500);
    		}
    	}

    	function setVisibles(loading, loadingHidden){
    		self.elem.toggleClass(classNameLoading, loading);
    		self.elem.toggleClass(classNameLoadingHidden, loadingHidden);
    	}
    }
}

})(jQuery);
