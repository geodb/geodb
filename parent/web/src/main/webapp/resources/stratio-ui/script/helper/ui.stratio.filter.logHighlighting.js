'use strict';

(function(){

angular
	.module('StratioUI.helper.filter.logHighlighting', [])
	.filter('stLogHighlighting', stLogHighlighting);

stLogHighlighting.$inject = ['$sce'];

function stLogHighlighting($sce){

	var higlights = {
		"date": {
			replace: '<span class="log-list__c-date">$1</span>',
			regexps: [
				/(\d{4}-\d{2}-\d{2}( |T)\d{2}:\d{2}:\d{2}[\.,]\d{3}(\+\d{4}|))/g,
				/([a-z]{3} \d{1,2}, \d{4} \d{1,2}:\d{1,2}:\d{1,2})/gi
			]
		},
		"ip": {
			replace: '<span class="log-list__c-ip"><strong>$1</strong>$2</span>',
			regexps: [
				/([1-2]?[0-9]{1,2}\.[1-2]?[0-9]{1,2}\.[1-2]?[0-9]{1,2}\.[1-2]?[0-9]{1,2})(:[1-6]?[0-9]{1,4})?/g
			]
		}
	}


	function filter(input, filter){
		var output = input;

		if(filter !== '' && typeof filter !== 'undefined'){
			output = output.replace(regExpIgnoreCase(filter), '<span class="log-list__c-filter">$1</span>');
		}

		angular.forEach(higlights, function(info, type){
			angular.forEach(info.regexps, function(regExp){
				if(input.match(regExp)){
					output = output.replace(regExp, info.replace);
				}
			});
		});

		return $sce.trustAsHtml(output);
	}

	function regExpIgnoreCase(word){
		var specials = /[.*+?|()\[\]{}\\$^]/g;
  		var output = (word || '').replace(specials, "\\$&");

  		return new RegExp("(" + output + ")", "gi");
	}

	return filter;
}

})();


