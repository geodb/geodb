'use strict';

/**
 * Replace element <ng-transclude/>
 * @example <directive> <ng-transclude/> </directive>
 */

angular
	.module('StratioUI.helper.cloneTransclude', [])
	.factory('stCloneTransclude', stCloneTransclude);

stCloneTransclude.$inject = ['stPassAllAttributes']
function stCloneTransclude(stPassAllAttributes){
	return function(scope, element, attributes, ctrls, transclude){
		stPassAllAttributes(scope, element, attributes);

		transclude(scope.$parent, function(clone){
			element.find("ng-transclude").replaceWith(clone);
		});
	}
}