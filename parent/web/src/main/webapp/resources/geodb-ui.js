(function(){

const _DEBUG_ = false;

var geodb = angular.module('geodb', [ 'ngResource', 'StratioUI', 'chart.js']);

geodb.constant('ChartPalette', {
	colors:{
		map: '#72C3B8',
		points: '#DF0049',
		heatmap: ['#ABCB4B','#72C3B8','#7C7D9A'],
		chart: ['#CDA7C6','#7C7D9A','#8FBCCC','#72C3B8','#879984','#ABCB4B','#C8CC7D','#F6EA26','#CDAB57','#CD8740']
	},
	formal:{
		map: '#998468',
		points: '#E88500',
		heatmap: ['#727991','#998468','#c2b6a5'],
		chart: ['#abafbe','#d7cec3','#8e94a8','#c2b6a5','#727991','#ae9d86','#5B6175','#998468','#454958','#7b6a53','#2e313c','#5d503e'],
	},
	corpBlue:{
		map: '#70778F',
		points: '#0035FF',
		heatmap: ['#434856','#70778F','#A9AEBC'],
		chart: ['#16181D','#2D3039','#434856','#5A5F72','#70778F','#8D92A5','#A9AEBC','#C6C9D2','#E2E4E9']
	},
	// http://colrd.com/palette/18823/
	BrBg11:{
		map: '#bf812d',
		points: '#EB8000',
		heatmap: ['#80cdc1','#dfc27d','#bf812d'],
		chart: ['#543005','#8c510a','#bf812d','#dfc27d','#f6e8c3','#c7eae5','#80cdc1','#35978f','#01665e','#003c30']
	},

	// http://colrd.com/palette/21837/
	sepia:{
		map: '#b59a85',
		points: '#D57D00',
		heatmap: ['#d5c6b0','#b59a85','#4c3f37'],
		chart:['#eeeade','#f5e6ca','#d5c6b0','#c5b399','#b59a85','#847767','#4c3f37']
	},

	// http://colrd.com/palette/19057/
	greenBlue:{
		map: '#41b6c4',
		points: '#225ea8',
		heatmap: ['#c7e9b4','#41b6c4','#225ea8'],
		chart: ['#edf8b1','#c7e9b4','#7fcdbb','#41b6c4','#1d91c0','#225ea8','#0c2c84']
	},

	pink:{
		map: '#F7737A',
		points: '#FF303B',
		heatmap: ['#F7737A','#ec9a91','#eeb59e'],
		chart:['#F7737A','#F57F81','#ee8b88','#ec9a91','#f0a798','#eeb59e','#eeddbb']
	}
});

geodb.run(function(ChartPalette){
	Chart.defaults.global.colours = ChartPalette[Object.keys(ChartPalette)[0]].chart;
	Chart.defaults.Doughnut.animationEasing = "easeOutExpo";
	Chart.defaults.Bar.legendTemplate = Chart.defaults.Radar.legendTemplate;

	if(!localStorage.getItem('ChartColours')){
		localStorage.setItem('ChartColours','colors');
	}
});

geodb.run(function(){
	$(window).resize(setMapDimensions);
	setTimeout(setMapDimensions, 100);
	
	setMapDimensions();
	init3dMap();

	function setMapDimensions(){
		$('.geodb-map').height($('.geodb-map-cont').outerHeight());
	}

	function init3dMap(){
		if($('.geodb-map--3d').length !== 0){
			$(window).on('mousemove', function(event){
				var max = 16;
				var wWidth = $(document.body).width();
				var rotate = (event.clientX / wWidth * 2 - 1) * max;
				$('.geodb-map-cont').css('transform', 'rotateX(30deg) rotateY(' + rotate + 'deg) rotateZ(' + (rotate * -0.5) + 'deg)');
			})
		}

	}
});

geodb.factory("ApiUtil", function($resource) {
	return $resource("/geodb/api/utils/:id");
});

geodb.factory("ApiAggregation", function($resource) {
	return $resource("/geodb/api/aggregation/:id", {}, {'query' : {method:'POST', isArray:true}});
});

geodb.factory("ApiChart", function($resource) {
	return $resource("/geodb/api/chart/filterday", {}, {'query' : {method:'POST', isArray:true}});
});

geodb.factory("ApiEventRealtime", function($resource) {
	return $resource("/geodb/api/event/realtime");
});

geodb.factory("ApiPrevisions", function($resource) {
	return $resource("/geodb/api/prevision/:id");
});

geodb.factory("UtilService", function(ApiUtil) {
	var service = {
		getUtil: getUtil,
		getUtilObject: getUtilObject
	};
	
	return service;
	
	function getUtil(id, callback){
		ApiUtil.query({
			id: id
		}, callback, onError);
	
		function onError(){
			if(MOCKS[id]){
				return callback(MOCKS[id]);
			}
		}
	}
	function getUtilObject(id, callback){
		ApiUtil.get({
			id: id
		}, callback, onError);
	
		function onError(){
			if(MOCKS[id]){
				return callback(MOCKS[id]);
			}
		}
	}
});

geodb.factory("AggregationService", function(ApiAggregation) {
	var service = {
		getAggregation: getAggregation
	};
	
	return service;
	
	function getAggregation(id, data, callback){
		if(_DEBUG_ && id === "selectAllCellsEveryDays"){
			callback(MOCKS.aggregationAllCells);
		}else{
			ApiAggregation.query({id: id}, data, callback);
		}
	}
});

geodb.factory("ChartService", function(ApiChart) {
	var service = {
		getChartByDay: getChartByDay,
		getChartByRange: getChartByRange
	};
	
	return service;
	
	function getChartByRange(cell, dayFrom, dayTo, callback){
		if(_DEBUG_){
			callback(MOCKS.filterday);
		}else{
			var data = {
				"cellId": cell || 'CLUSTER',
				"dayFrom": dayFrom,
				"dayTo": dayTo
			}
			ApiChart.query({}, data, callback);
		}
	}
	function getChartByDay(cell, day, callback){
		if(_DEBUG_){
			getChartByRange(cell, day, day, filterCallback);

			function filterCallback(data){
				callback(
					data.filter(function(x){
						return toDateId(x.day) === toDateId(day);
					})
				);
			}
		}else{
			getChartByRange(cell, day, day, callback);
		}
	}
});

geodb.factory("RealtimeService", function(ApiEventRealtime) {
	var service = {
		getRealtimeMap: getRealtimeMap
	};
	
	return service;
	
	function getRealtimeMap(callback){
		if(_DEBUG_){
			callback(MOCKS.realtime);
		}else{
			ApiEventRealtime.query({}, {}, callback);
		}
	}
});

geodb.factory("PrevisionsService", function(ApiPrevisions) {
	var service = {
		getMap: getMap
	};
	
	return service;
	
	function getMap(id, callback){
		ApiPrevisions.get({
			id: id
		}, callback, onError);

		function onError(){
			if(id && MOCKS[id.toLowerCase()]){
				return callback(MOCKS[id.toLowerCase()]);
			}
		}
	}
});

geodb.factory('LocalStorageChecker', function(){
	function LocalStorageChecker(property){
		var self = this;

		var localStorageParameter = property;
		var hasLocalStorage = !!window.localStorage;

		self.set = function(value){
			if(hasLocalStorage){
				localStorage.setItem(localStorageParameter, value);
			}
		}

		self.get = function(){
			if(hasLocalStorage){
				return localStorage.getItem(localStorageParameter);
			}
			return null;
		}
	}

	return LocalStorageChecker;
});

geodb.factory('ChangeColours', function(LocalStorageChecker, ChartPalette){
	var LSChartColours = new LocalStorageChecker('ChartColours');

	return function($scope){
		return function(){
			var colorList =  Object.keys(ChartPalette);
			var color = ChartPalette[LSChartColours.get()] ? LSChartColours.get() : colorList[0];
			var nextColorIndex = colorList[(colorList.indexOf(color) + 1) % colorList.length];

			LSChartColours.set(nextColorIndex);
			$scope.colours = ChartPalette[nextColorIndex].chart;
		}
	}
});

geodb.filter('translate', function() {
	return function(input) {
		var translation = _TRANSLATION_[input];
		return translation ? translation : input;
	};
});

geodb.controller("streamingCtrl", function($scope, UtilService, RealtimeService, ChartPalette, ChangeColours, LocalStorageChecker){
	var names = {};
	var rectangleMap = {};
	var heatMap = {
		w: 1000,
		h: 538,
		r: 18,
		canvas: $('#heat-map')
	};
	var heatmap = false;
	var mapPoints = false;
	var LSChartColours = new LocalStorageChecker('ChartColours');

	$scope.colorus = ChartPalette[LSChartColours.get()] ? ChartPalette[LSChartColours.get()].heatmap || [] : [];

	// Change colors
	$scope.changeColours = ChangeColours($scope);
	$scope.mapMaxValue = 0;

	$scope.map = {};
	$scope.rawMap = {};

	UtilService.getUtil('cellX', function(data){
		$scope.cellX = data;
	});
	UtilService.getUtil('cellY', function(data){
		$scope.cellY = data;
	});
	UtilService.getUtilObject('rectangleMap', function(data){
		rectangleMap = data;

		refreshHeatMap();
	});

	UtilService.getUtil('typesIncident', function(data){
		names['typeEvent'] = toObject(data);
		$scope.typesIncident = toSelect(data);
		$scope.typesIncidentValue = -1;
	});
	UtilService.getUtil('severitysIncident', function(data){
		names['severity'] = toObject(data);
		$scope.severitysIncident = toSelect(data);
		$scope.severitysIncidentValue = -1;
	});

	function refreshRealtimeMap(){
		showRefreshing(true);

		RealtimeService.getRealtimeMap(function(data){
			$scope.map = {};
			data.forEach(function(cell){
				$scope.map[cell.cellId] = cell;

				if($scope.mapMaxValue < cell.numIncidents){
					$scope.mapMaxValue = cell.numIncidents;
				}
				if(cell.numIncidents){
					var sum = 0;
					var max = 0;

					cell.eventsRealtime.forEach(function(incident){
						sum += incident.timeDelay;
						if(max < incident.timeDelay){
							max = incident.timeDelay;
						}
					});

					cell.delayAvg = Math.round(sum / cell.numIncidents);
					cell.delayMax = max;
				}
			});
			$scope.rawMap = $scope.map;

			refreshHeatMap();

			setTimeout(showRefreshing, 300);
		});
	}

	refreshRealtimeMap();
	setInterval(refreshRealtimeMap, 18 * 1000);

	$scope.$watch(function(){
		return $scope.cellSelected
	},function(newValue){
		if(!newValue){
			return;
		}

		$scope.cellSelected = newValue;

		generateCellTable();
	});

	$scope.$watch(function(){
		return angular.toJson([
			$scope.typesIncidentValue,
			$scope.severitysIncidentValue
		]);
	},function(newValue){
		if(!$scope.map){
			return;
		}

		var newValue = angular.fromJson(newValue);
		
		$scope.typesIncidentValue = newValue[0];
		$scope.severitysIncidentValue = newValue[1];
		
		if($scope.cellSelected){
			generateCellTable();
		}
		refreshRealtimeMap();
	});

	function generateCellTable(){
		console.log(2)
		var incidents = $scope.map[$scope.cellSelected].eventsRealtime;
		var titles = ['type','severity','timeDelay','lengthDelay','fromIncident','toIncident'];
		var rowName = 'incidentId';
		var filterName = {
			type: function(input){return names['typeEvent'][input]},
			severity: function(input){return names['severity'][input]}
		}

		if(incidents.length === 0){
			$scope.cellData = false;
			return;
		}

		$scope.cellData = {
			name: "Incidents",
			names: [],
			titles: titles,
			values: []
		}

		incidents
			.filter(function(incident){
				if($scope.typesIncidentValue !== -1 && incident.type !== $scope.typesIncidentValue){
					return false;
				}
				if($scope.severitysIncidentValue !== -1 && incident.severity !== $scope.severitysIncidentValue){
					return false;
				}
				return true;
			})
			.forEach(function(incident){
				var values = [];

				$scope.cellData.names.push(incident[rowName]);

				titles.forEach(function(title){
					var value = incident[title];
					if(filterName[title]){
						value = filterName[title](value);
					}
					values.push(value);
				});

				$scope.cellData.values.push(values);
			});
	}

	function showRefreshing(show){
		$('#refreshing').toggleClass('message-popup--hidden', show !== true);
	}


	function refreshHeatMap(){
		var canvas = heatMap.canvas.get(0);
		var ctx = canvas.getContext('2d');
		var map = generateMap();

		if(map === false){
			return false;
		}

		if(heatmap === false){
			heatmap = h337.create({
				width: heatMap.w,
				height: heatMap.h,
				container: heatMap.canvas.parent().get(0),
				radius: 12,
				minOpacity: 0.12,
				maxOpacity: 0.6
			});
			changeHeatmapColor();
		}

		if(mapPoints === false){
			mapPoints = new MapPoints(canvas, ctx);
		}
		heatmap.removeData();
		heatmap.setData({
			max: 2,
		  	data: map.heatMap
		});

		mapPoints.setData(map.points, map.pointsMax);
		mapPoints.updatePalette(ChartPalette[LSChartColours.get()]);

	}

	function generateMap(){
		var source = $scope.map;
		var map = [];
		var mapMax = 0;
		var points = [];
		var limits = heatMap;

		if(!source || !rectangleMap.xEnd){
			return false;
		}

		angular.forEach(source, function(cell){
			cell.filteredNumIncidents = 0;
			cell.eventsRealtime
				.filter(function(event){
					if($scope.severitysIncidentValue !== -1 && event.severity !== $scope.severitysIncidentValue){
						return false;
					}
					if($scope.typesIncidentValue !== -1 && event.type !== $scope.typesIncidentValue){
						return false;
					}
					return true;
				})
				.forEach(function(event){
					cell.filteredNumIncidents++;
					points.push({
						x: event.x,
						y: event.y
					})
				});
		});

		for(var x=0;x<limits.w;x++){
			map[x] = [];
			for(var y=0;y<limits.h;y++){
				map[x][y] = 0;
			}
		}

		points.forEach(function(position){
			var cell = getCellByPosition(position)
			map[cell.x][cell.y]++;
			if(mapMax < map[cell.x][cell.y]){
				mapMax = map[cell.x][cell.y];
			}
		});

		return {
			heatMap: points.map(getCellByPosition),
			points: map,
			pointsMax: mapMax
		}

		function getCellByPosition(position){
			var x = position.x;
			var y = position.y;
			var cell = {};

			cell.x = Math.floor((position.x - rectangleMap.xStart) / (rectangleMap.xEnd - rectangleMap.xStart) * limits.w);
			cell.y = Math.floor((1 - (position.y - rectangleMap.yStart) / (rectangleMap.yEnd - rectangleMap.yStart)) * limits.h);

			return cell;
		}
	}

	$scope.$watch(function(){
		return LSChartColours.get();
	}, function(newValue) {
		$scope.colorus = ChartPalette[newValue].heatmap;
		
		changeHeatmapColor(ChartPalette[newValue]);
		if(mapPoints !== false){
			mapPoints.updatePalette(ChartPalette[newValue]);
		}
	});

	function changeHeatmapColor(){
		if(heatmap){
			heatmap.configure({
				gradient: {
					'.5': $scope.colorus[2],
					'.99': $scope.colorus[1],
					'1': $scope.colorus[0]
				}
			});
		}
	}

	function MapPoints(canvas, ctx){
		var self = this;
		var size = 2;
		var type = "circle";
		var sizeCenterMinus = (size - 1) / 2;

		self.map = null;
		self.mapMax = null;
		self.color = "255,0,0";

		self.setData = function(map, mapMax){
			self.map = map;
			self.mapMax = mapMax;
				$scope.colorus[0]
			draw();
		}

		self.updatePalette = function(palette){
			self.color = getRgb(palette.points);

			draw();
		}

		function draw(){
			ctx.clearRect(0, 0, canvas.width, canvas.height);

			self.map.forEach(function(xCells, x){
				xCells.forEach(function(value, y){
					if(value > 0){
						ctx.fillStyle = 'rgba(' + self.color + ',' + (value / self.mapMax * 0.3 + 0.5) + ')';

						if(type == "circle"){
							ctx.beginPath(); 
							ctx.arc(x - sizeCenterMinus, y - sizeCenterMinus, size, 0, Math.PI*2, true);  
							ctx.closePath();
							ctx.fill();
						}else{
							ctx.fillRect(x - sizeCenterMinus, y - sizeCenterMinus, size, size);
						}
					}
				});
			});
		}

		function getRgb(hex) {
			var hex = hex.split('#')[1],
				bigint = parseInt(hex, 16),
				r = (bigint >> 16) & 255,
				g = (bigint >> 8) & 255,
				b = bigint & 255;

			return [r, g, b].join(',');
		}
	}

});

geodb.controller("historicCtrl", function($scope, UtilService, AggregationService, ChartService, LocalStorageChecker, ChartPalette, ChangeColours) {
	var chartData;
	var rawChartData;
	var names = {};
	var countGetCharts = 0;

	var LSChartDayVsEventType = new LocalStorageChecker('ChartDayVsEventType');
	var LSChartDayVsSeverity = new LocalStorageChecker('ChartDayVsSeverity');
	var LSChartColours = new LocalStorageChecker('ChartColours');

	// Change colors
	$scope.changeColours = ChangeColours($scope);

	$scope.cellSelected = 'CLUSTER';

	$scope.colours = ChartPalette[LSChartColours.get()] ? ChartPalette[LSChartColours.get()].chart || null : null;

	$scope.typeChartDayVsEventType = LSChartDayVsEventType.get() || 'Line';
	$scope.typeChartDayVsSeverity = LSChartDayVsSeverity.get() || 'Line';
	
	UtilService.getUtil('keysDescription', function(data){
		$scope.keysDescription = toSelect(data);
		$scope.keysDescriptionValue = 'TOTAL';
	});
	UtilService.getUtil('typesIncident', function(data){
		names['typeEvent'] = toObject(data);
		$scope.typesIncident = toSelect(data);
		$scope.typesIncidentValue = -1;
	});
	UtilService.getUtil('severitysIncident', function(data){
		names['severity'] = toObject(data);
		$scope.severitysIncident = toSelect(data);
		$scope.severitysIncidentValue = -1;
	});
	UtilService.getUtil('cells', function(data){
		$scope.cells = data;
	});
	UtilService.getUtil('days', function(data){
		$scope.daySelected = Math.max.apply(Math, data);
		$scope.daySelectedIni = Math.min.apply(Math, data.filter(function(x){return (x >= $scope.daySelected - 6 * 24 * 60 * 60 * 1000);}));
		$scope.days = data.map(function(day){
			var labelDate = new Date(day);
			var label = toDateId(labelDate);
			return {
				value: day,
				name: label
			};
		});
		$scope.daysIni = $scope.days;
	});

	UtilService.getUtil('cellX', function(data){
		$scope.cellX = data;
	});
	UtilService.getUtil('cellY', function(data){
		$scope.cellY = data;
	});

	$scope.updateCharts = function(){
		if($scope.page === 'historic'){
			ChartService.getChartByDay($scope.cellSelected, $scope.daySelected, chartCallback);
		}else if($scope.page === 'report'){
			ChartService.getChartByRange($scope.cellSelected, $scope.daySelectedIni, $scope.daySelected, chartCallback);
		}

		function chartCallback(data){
			countGetCharts ++;

			rawChartData = data;
			chartData = data;
		}
	}	

	$scope.updateMap = function(){
		AggregationService.getAggregation("selectAllCellsEveryDays", {
			"cellId": $scope.cellsValue,
			"dayFrom": $scope.page === 'report' ? $scope.daySelectedIni :  $scope.daySelected,
			"dayTo": $scope.daySelected,
			"keyDesc": $scope.keysDescriptionValue,
			"severity": $scope.severitysIncidentValue,
			"type": $scope.typesIncidentValue,
			"value": -1
		}, aggregationCallback);
		
		function aggregationCallback(data){
			$scope.map = {};
			$scope.mapMaxValue = 0;

			data.forEach(function(cell){
				$scope.map[cell.cellId] = cell;
				if($scope.mapMaxValue < cell.value){
					$scope.mapMaxValue = cell.value;
				}
			});

			$scope.cells = data;
		}
	}

	$scope.updateCharts();
	$scope.updateMap();

	setTimeout(generateChars, 10);

	$scope.$watch(function(){
		return $scope.daySelected;
	}, function(newValue){
		$scope.daySelected = newValue;

		$scope.updateCharts();
	});
	$scope.$watch(function(){
		return $scope.daySelectedIni;
	}, function(newValue){
		$scope.daySelectedIni = newValue;

		$scope.updateCharts();
	});

	$scope.$watch(function(){
		return $scope.cellSelected;
	}, function(newValue){
		$scope.cellSelected = newValue;

		$scope.updateCharts();
	});

	$scope.$watch(function(){
		return angular.toJson([
			$scope.typesIncidentValue,
			$scope.severitysIncidentValue,
			$scope.keysDescriptionValue,
			countGetCharts
		]);
	},function(newValue){
		if(!names['typeEvent'] || !names['severity']){
			return;
		}
		if(!rawChartData){
			return;
		}
		
		setTimeout($scope.updateMap, 0);

		var newValue = angular.fromJson(newValue);

		var copyRawChartData = JSON.parse(JSON.stringify(rawChartData));

		$scope.keysDescriptionValue = newValue[2];

		chartData = copyRawChartData.map(function(item){
			var pass =  true;

			if(newValue[0] !== -1 && item.typeEvent !== newValue[0]){
				pass = false;
			}
			if(newValue[1] !== -1 && item.severity !== newValue[1]){
				pass = false;
			}
			if(!pass){
				item.value = 0;
			}
			return item;
		});

		generateChars();
	});

	// Watching selects
	$scope.$watch(function(){return $scope.typeChartDayVsEventType;}, function(newValue){if(newValue) LSChartDayVsEventType.set(newValue)});
	$scope.$watch(function(){return $scope.typeChartDayVsSeverity;}, function(newValue){if(newValue) LSChartDayVsSeverity.set(newValue)});

	// Charts
	function generateChars(){
		if($scope.page === 'historic'){
			generateCTypeEvent();
			generateCSeverity();
			generateCTypeVsSeverity();
		}else if($scope.page === 'report'){
			generateCDayVsEventType();
			generateCDayVsSeverity();
		}
	}

	// Radar chart (Historic)
	function generateCTypeVsSeverity(){
		var origin = chartData;
		var data = {};
		var chart = {
			labels: [],
			values: [],
			series: []
		}

		angular.forEach(origin, function(item){
			if($scope.keysDescriptionValue != item.keyEnum){
				return;
			}
			if(item.severity === -1 || item.typeEvent === -1){
				return;
			}
			if(!(data[item.severity] instanceof Object)){
				data[item.severity] = {};
			}
			if(typeof data[item.severity][item.typeEvent] !== 'number'){
				data[item.severity][item.typeEvent] = 0;
			}
			data[item.severity][item.typeEvent] += item.value;
		});

		angular.forEach(data, function(series, label){
			var pLabel = chart.labels.length;
			chart.labels.push(names.severity[label]);

			angular.forEach(series, function(value, serie){
				var pSerie = chart.series.indexOf(names.typeEvent[serie]);
				if(pSerie === -1){
					pSerie = chart.series.length;
					chart.series.push(names.typeEvent[serie]);
				}
				if(!chart.values[pSerie]){
					chart.values[pSerie] = [];
				}
				chart.values[pSerie][pLabel] = Math.round(value * 100) / 100;
			});
		});

		$scope.chartTypeVsSeverity = chart;
	}

	// Radar chart (report)
	function generateCDayVsEventType(){
		generateChartByDayVs('typeEvent', 'chartDayVsEventType', 'tableDayVsEventType');
	}
	function generateCDayVsSeverity(){
		generateChartByDayVs('severity', 'chartDayVsSeverity', 'tableDayVsSeverity');
	}

	function generateChartByDayVs(property, scopeChartVar, scopeTableVar){
		var origin = chartData;
		var data = {};
		var chart = {
			labels: [],
			values: [],
			series: []
		}
		var table = {
			name: "Day",
			names: [],
			titles: [],
			values: []
		}

		angular.forEach(origin, function(item){
			if($scope.keysDescriptionValue != item.keyEnum){
				return;
			}
			if(item.severity === -1 || item.typeEvent === -1){
				return;
			}
			if(!(data[toDateId(item.day)] instanceof Object)){
				data[toDateId(item.day)] = {};
			}
			if(typeof data[toDateId(item.day)][item[property]] !== 'number'){
				data[toDateId(item.day)][item[property]] = 0;
			}
			data[toDateId(item.day)][item[property]] += item.value;
		});

		angular.forEach(data, function(series, label){
			var pLabel = chart.labels.length;
			var labelName = label;

			chart.labels.push(labelName);

			angular.forEach(series, function(value, serie){
				var pSerie = chart.series.indexOf(names[property][serie]);
				if(pSerie === -1){
					pSerie = chart.series.length;
					chart.series.push(names[property][serie]);
				}
				if(!chart.values[pSerie]){
					chart.values[pSerie] = [];
				}
				if(!table.values[pLabel]){
					table.values[pLabel] = [];
				}
				chart.values[pSerie][pLabel] = Math.round(value * 100) / 100;
				table.values[pLabel][pSerie] = Math.round(value * 100) / 100;
			});
		});

		table.names = chart.labels;
		table.titles = chart.series;

		$scope[scopeChartVar] = chart;
		$scope[scopeTableVar] = table;
	}


	// Donut chart
	function generateCSeverity(){
		var chart = getChartByAttribute(chartData, 'severity', $scope.severitysIncident);
		$scope.chartSeverity = chart;
	}
	function generateCTypeEvent(){
		var chart = getChartByAttribute(chartData, 'typeEvent', $scope.typesIncident);
		$scope.chartTypeEvent = chart;
	}


	function getChartByAttribute(input, filter, labelName){
		var data = {};
		var output = {};
		var chart = {
			labels: [],
			values: []
		};

		angular.forEach(input, function(item){
			if($scope.keysDescriptionValue != item.keyEnum){
				return;
			}
			if(!data[item[filter]]){
				data[item[filter]] = 0;
			}
			data[item[filter]] += item.value;
		});

		angular.forEach(data, function(value, key){
			if(!names[filter]){
				return;
			}
			var label = names[filter][key];

			output[label] = value;
		});

		angular.forEach(output, function(value, label){
			if(label === 'All data'){
				return;
			}
			chart.labels.push(label);
			chart.values.push(Math.round(value * 100) / 100);
		});

		return chart;
	}

	// print report
	$scope.print = function(){
		window.print();
	}

});


geodb.controller("previsionsCtrl", function($scope, UtilService, PrevisionsService, ChangeColours, ChartPalette, LocalStorageChecker) {

	var heatMap = {
		w: 1000,
		h: 538,
		r: 18,
		canvas: $('#heat-map')
	};
	var heatmap = false;
	var mapPoints = false;
	var LSChartColours = new LocalStorageChecker('ChartColours');
	var rectangleMap = {};

	$scope.colorus = ChartPalette[LSChartColours.get()] ? ChartPalette[LSChartColours.get()].heatmap || [] : [];

	$scope.changeColours = ChangeColours($scope);
	$scope.mapMaxValue = 0;
	$scope.day = 0;

	UtilService.getUtil('previsions', function(data){
		$scope.previsionsType = toSelect(data);
		$scope.previsionsTypeValue = data[0].id;
		generateMap();
	});

	UtilService.getUtilObject('rectangleMap', function(data){
		rectangleMap = data;

		generateMap();
	});

	UtilService.getUtil('cellX', function(data){
		$scope.cellX = data;
	});
	
	UtilService.getUtil('cellY', function(data){
		$scope.cellY = data;
	});

	function generateMap(){
		PrevisionsService.getMap($scope.previsionsTypeValue, function(data){
			$scope.day = data.day;
			$scope.type = data.typePrevision;
			$scope.map = {};
			$scope.mapMaxValue = 0;

			var heatmapData = [];

			data.previsions.forEach(function(cell){
				heatmapData.push(getCellById(cell));

				$scope.map[cell.cellId] = cell;

				if($scope.mapMaxValue < cell.value){
					$scope.mapMaxValue = cell.value;
				}
			});

			refreshHeatMap(heatmapData, $scope.mapMaxValue, $scope.previsionsTypeValue);
		});

		function getCellById(position){
			var x = ~~position.cellId.replace(/(?:.+)?X(\d+)(.+)?/, '$1');
			var y = ~~position.cellId.replace(/(?:.+)?Y(\d+)(.+)?/, '$1');
			var cell = {};

			var center = heatMap.w / $scope.cellX.length / 2;

			cell.x = Math.floor((x / $scope.cellX.length) * heatMap.w + center);
			cell.y = Math.floor((1 - (y / $scope.cellY.length)) * heatMap.h - center);
			cell.value = position.value;

			return cell;
		}
	}

	$scope.$watch(function(){
		return $scope.previsionsTypeValue;
	}, function(newValue){
		$scope.previsionsTypeValue = newValue;

		generateMap();
	});

	function refreshHeatMap(data, max, type){
		if(heatmap === false){
			heatmap = h337.create({
				width: heatMap.w,
				height: heatMap.h,
				container: heatMap.canvas.parent().get(0),
				radius: type === 'ACCIDENT' ? 24 : 30,
				minOpacity: 0,
				maxOpacity: 0.6
			});
			changeHeatmapColor();
		}
		heatmap.removeData();
		data.forEach(x=>x.value=x.value*2)
		heatmap.setData({
			max: Math.round(2 + (max * 0.2)),
		  	data: data//.filter(function(x){return x.value > 0})
		});
	}
	$scope.$watch(function(){
		return LSChartColours.get();
	}, function(newValue) {
		$scope.colorus = ChartPalette[newValue].heatmap;
		
		changeHeatmapColor(ChartPalette[newValue]);
		if(mapPoints !== false){
			mapPoints.updatePalette(ChartPalette[newValue]);
		}
	});

	function changeHeatmapColor(){
		if(heatmap){
			heatmap.configure({
				gradient: {
					'.3': $scope.colorus[2],
					'.6': $scope.colorus[1],
					'1': $scope.colorus[0]
				}
			});
		}
	}
});

geodb.directive('dynamicCss', function(){
    var directive = {
        restrict: 'E',
        scope: {
        	color: '@'
        },
        replace: true,
        templateUrl: 'geodb-temp-dynamic.html',
        controller: controller
    };

    return directive;

    function controller($scope, LocalStorageChecker, ChartPalette){
		var LSChartColours = new LocalStorageChecker('ChartColours');

		var colorList =  Object.keys(ChartPalette);
		var color = ChartPalette[LSChartColours.get()] ? LSChartColours.get() : colorList[0];
		var nextColorIndex = colorList[(colorList.indexOf(color) + 1) % colorList.length];

		$scope.color = ChartPalette[nextColorIndex].map;
		$scope.gradient = ChartPalette[nextColorIndex].heatmap;

		$scope.rgb = function(hex) {
			if(!hex) return;

			var hex = hex.split('#')[1],
				bigint = parseInt(hex, 16),
				r = (bigint >> 16) & 255,
				g = (bigint >> 8) & 255,
				b = bigint & 255;

			return [r, g, b].join(',');
		}

		$scope.$watch(function(){
			return LSChartColours.get()
		}, function(){
			$scope.color = ChartPalette[LSChartColours.get()] ? ChartPalette[LSChartColours.get()].map : null;
			$scope.gradient = ChartPalette[LSChartColours.get()].heatmap;
		});
	}
});

function toSelect(input){
	var output = [];

	input.forEach(function(item){
		output.push({
			value: item.id,
			name: item.description
		});
	});

	return output;
}
function toObject(input){
	var output = {};

	input.forEach(function(item){
		output[item.id] = item.description;
	});

	return output;
}

function toDateId(date){
	var date = new Date(date);
	return (date.getDate() + '/' + (date.getMonth() + 1) +'/' + date.getFullYear());
}

})();