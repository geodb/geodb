package com.paradigma.web.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cassandra.core.RowMapper;

import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.paradigma.utility.model.KeyEnum;
import com.paradigma.utility.model.SeverityEnum;
import com.paradigma.utility.model.TypeIncidentEnum;
import com.paradigma.utility.service.DateService;
import com.paradigma.web.controller.dto.AggregationDto;
import com.paradigma.web.controller.dto.AggregationRequestDto;
import com.paradigma.web.controller.dto.ChartResponseDto;
import com.paradigma.web.domain.Aggregation;

public class AggregationMapper implements RowMapper<Aggregation>{

	@Override
	public Aggregation mapRow(Row row, int rowNum)
			throws DriverException {
		Aggregation aggregation = new Aggregation();
		aggregation.setType(row.getInt("type"));
		aggregation.setSeverity(row.getInt("severity"));
		aggregation.setCellId(row.getString("cellid"));
		aggregation.setKeyDesc(row.getString("keydesc"));
		aggregation.setDay(row.getInt("day"));
		aggregation.setValue(row.getDouble("value"));
		//aggregation.setAggregationId(row.getString("aggregationid"));
		return aggregation;
	}
	
	public Aggregation parserDtotoCassandraRow(AggregationDto aggregationDto){
		Aggregation aggregation = new Aggregation();
		DateService dateService = new DateService();
		//aggregation.setAggregationId(aggregationDto.getAggregationId());
		aggregation.setCellId(aggregationDto.getCellId());
		aggregation.setDay(dateService.convertDayFrontToCassandra(aggregationDto.getDay()));
		aggregation.setKeyDesc(aggregationDto.getKeyDesc().getId());
		aggregation.setSeverity(aggregationDto.getSeverity());
		aggregation.setType(aggregationDto.getType());
		aggregation.setValue(aggregationDto.getValue());
		return aggregation;
	}
	
	public AggregationDto parserCassandraToDto(Aggregation aggregation){
		AggregationDto aggregationDto = new AggregationDto();
		DateService dateService = new DateService();
		//aggregationDto.setAggregationId(aggregation.getAggregationId());
		aggregationDto.setCellId(aggregation.getCellId());
		aggregationDto.setDay(dateService.convertDayCassandraToFront(aggregation.getDay()));
		aggregationDto.setKeyDesc(KeyEnum.getKeyFromId(aggregation.getKeyDesc()));
		aggregationDto.setSeverity(aggregation.getSeverity());
		aggregationDto.setType(aggregation.getType());
		aggregationDto.setValue(aggregation.getValue());
		return aggregationDto;
	}
	
	public ChartResponseDto parserCassandraToChartDto(Aggregation aggregation){
		ChartResponseDto chartResponseDto = new ChartResponseDto();
		DateService dateService = new DateService();
		//aggregationDto.setAggregationId(aggregation.getAggregationId());
		chartResponseDto.setCellId(aggregation.getCellId());
		chartResponseDto.setDay(dateService.convertDayCassandraToFront(aggregation.getDay()));
		chartResponseDto.setKeyEnum(KeyEnum.getKeyFromId(aggregation.getKeyDesc()));
		chartResponseDto.setSeverity(aggregation.getSeverity());
		chartResponseDto.setTypeEvent(aggregation.getType());
		chartResponseDto.setValue(aggregation.getValue());
		return chartResponseDto;
	}
	
	public List<AggregationDto> toListDto(List<Aggregation> aggregations){
		List<AggregationDto> aggregationDtos = new ArrayList<>();
		for(Aggregation a : aggregations){
			AggregationDto aDto = parserCassandraToDto(a);
			aggregationDtos.add(aDto);
		}
		return aggregationDtos;
	}
}
