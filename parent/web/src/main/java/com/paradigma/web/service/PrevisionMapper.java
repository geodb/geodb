package com.paradigma.web.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.cassandra.core.RowMapper;

import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.paradigma.utility.service.DateService;
import com.paradigma.web.controller.dto.PrevisionCellDto;
import com.paradigma.web.controller.dto.PrevisionDto;
import com.paradigma.web.domain.Prevision;

public class PrevisionMapper implements RowMapper<Prevision> {

	@Override
	public Prevision mapRow(Row row, int rowNum) throws DriverException {
		Prevision p = new Prevision();
		p.setCellId(row.getString("cellid"));
		p.setDay(row.getInt("day"));
		p.setPrevisionType(row.getString("previsiontype"));
		p.setValue(row.getDouble("value"));
		return p;
	}
	
	public PrevisionDto parserCassandraToDto(List<Prevision> previsions, String previsionId){
		
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR)*1000;
	    int tomorrow = year+cal.get(Calendar.DAY_OF_YEAR)+1;
		PrevisionDto previsionDto = new PrevisionDto();
		DateService dateService = new DateService();
		List<PrevisionCellDto> previsionCellsDto = new ArrayList<>();
		for(Prevision p : previsions){
			if(p.getPrevisionType().equals(previsionId)){
				previsionDto.setDay(dateService.convertDayCassandraToFront(tomorrow/*p.getDay()*/));
				previsionDto.setTypePrevision(p.getPrevisionType());
				PrevisionCellDto pcDto = new PrevisionCellDto();
				pcDto.setCellId(p.getCellId());
				pcDto.setValue(p.getValue());
				previsionCellsDto.add(pcDto);
			}
			
		}
		previsionDto.setPrevisions(previsionCellsDto);
		return previsionDto;
	}

}
