package com.paradigma.web.controller.dto;

import java.util.List;

public class ClusterMapEventRealtimeDto {
	
	private List<EventRealTimeDto> eventsRealtime;
	private int numIncidents;
	private String cellId;
	
	public ClusterMapEventRealtimeDto(){
		super();
	}
	
	public List<EventRealTimeDto> getEventsRealtime() {
		return eventsRealtime;
	}
	public void setEventsRealtime(List<EventRealTimeDto> eventsRealtime) {
		this.eventsRealtime = eventsRealtime;
	}
	public int getNumIncidents() {
		return numIncidents;
	}
	public void setNumIncidents(int numIncidents) {
		this.numIncidents = numIncidents;
	}
	public String getCellId() {
		return cellId;
	}
	public void setCellId(String cellId) {
		this.cellId = cellId;
	}
	
	

}
