package com.paradigma.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@EnableWebMvc
@Configuration
@Import({CassandraConfig.class, SwaggerConfiguration.class})
public class MVCConfiguration extends WebMvcConfigurerAdapter {
	

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/geodb").setViewName("/index.html");
        registry.addViewController("/swagger-web").setViewName("/swagger-ui.html");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }
	
	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("/resources/");
    }

}
