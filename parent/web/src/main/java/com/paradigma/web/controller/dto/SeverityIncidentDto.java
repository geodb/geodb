package com.paradigma.web.controller.dto;

public class SeverityIncidentDto {

	private int id;
    private String description;
    
    public SeverityIncidentDto(){
    	super();
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
    
    
}
