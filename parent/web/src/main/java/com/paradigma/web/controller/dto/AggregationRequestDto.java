package com.paradigma.web.controller.dto;

import com.paradigma.utility.model.KeyEnum;
import com.paradigma.utility.model.SeverityEnum;
import com.paradigma.utility.model.TypeIncidentEnum;


public class AggregationRequestDto {

	private int type;
	private int severity;
	private String cellId;
	private KeyEnum keyDesc;
	private long dayFrom;
	private long dayTo;
	private double value;

	public AggregationRequestDto() {
		super();
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getSeverity() {
		return severity;
	}

	public void setSeverity(int severity) {
		this.severity = severity;
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	public KeyEnum getKeyDesc() {
		return keyDesc;
	}

	public void setKeyDesc(KeyEnum keyDesc) {
		this.keyDesc = keyDesc;
	}

	public long getDayFrom() {
		return dayFrom;
	}

	public void setDayFrom(long dayFrom) {
		this.dayFrom = dayFrom;
	}
	
	public long getDayTo() {
		return dayTo;
	}

	public void setDayTo(long dayTo) {
		this.dayTo = dayTo;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "AggregationRequestDto [type=" + type + ", severity=" + severity
				+ ", cellId=" + cellId + ", keyDesc=" + keyDesc + ", dayFrom="
				+ dayFrom + ", dayTo=" + dayTo + ", value=" + value + "]";
	}

	

}
