package com.paradigma.web.controller.dto;

import com.paradigma.utility.model.SeverityEnum;
import com.paradigma.utility.model.TypeIncidentEnum;

public class EventRealTimeDto {
	
	    private String incidentId;
	    private String moment;
	    private double x;
	    private double y;
	    private String description;
	    private String roadName;
	    private String fromIncident;
	    private String toIncident;
	    private int type;
	    private int severity;
	    private int lengthDelay;
	    private int timeDelay;
	    private String cellId;

	    public EventRealTimeDto() {
	        super();
	    }

		public String getIncidentId() {
			return incidentId;
		}

		public void setIncidentId(String incidentId) {
			this.incidentId = incidentId;
		}

		public String getMoment() {
			return moment;
		}

		public void setMoment(String moment) {
			this.moment = moment;
		}

		public double getX() {
			return x;
		}

		public void setX(double x) {
			this.x = x;
		}

		public double getY() {
			return y;
		}

		public void setY(double y) {
			this.y = y;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public String getRoadName() {
			return roadName;
		}

		public void setRoadName(String roadName) {
			this.roadName = roadName;
		}

		public String getFromIncident() {
			return fromIncident;
		}

		public void setFromIncident(String fromIncident) {
			this.fromIncident = fromIncident;
		}

		public String getToIncident() {
			return toIncident;
		}

		public void setToIncident(String toIncident) {
			this.toIncident = toIncident;
		}

		public int getType() {
			return type;
		}

		public void setType(int type) {
			this.type = type;
		}

		public int getSeverity() {
			return severity;
		}

		public void setSeverity(int severity) {
			this.severity = severity;
		}

		public int getLengthDelay() {
			return lengthDelay;
		}

		public void setLengthDelay(int lengthDelay) {
			this.lengthDelay = lengthDelay;
		}

		public int getTimeDelay() {
			return timeDelay;
		}

		public void setTimeDelay(int timeDelay) {
			this.timeDelay = timeDelay;
		}

		public String getCellId() {
			return cellId;
		}

		public void setCellId(String cellId) {
			this.cellId = cellId;
		}
}
