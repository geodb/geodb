package com.paradigma.web.controller.dto;

public class PrevisionCellDto {

	private String cellId;
	private double value;
	
	public PrevisionCellDto(){
		super();
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	
	
}
