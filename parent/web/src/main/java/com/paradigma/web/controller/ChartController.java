package com.paradigma.web.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paradigma.web.config.CassandraConfig;
import com.paradigma.web.controller.dto.ChartRequestDto;
import com.paradigma.web.controller.dto.ChartResponseDto;
import com.paradigma.web.service.CassandraService;

@Controller
@RequestMapping("/api/chart")
@Import(CassandraConfig.class)
public class ChartController {

		
		@Resource(name="cassandraServiceAggregation")
		private CassandraService cassandraServiceAggregation;

		@RequestMapping(value = "/filterday", method = RequestMethod.POST)
		public @ResponseBody List<ChartResponseDto> getChartsFilterDay(@RequestBody ChartRequestDto chartRequestDto){
			return cassandraServiceAggregation.selectAggregationToChart(chartRequestDto);
		}
}
