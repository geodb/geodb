package com.paradigma.web.controller.dto;

public class ChartRequestDto {

	private long dayFrom;
	private long dayTo;
	private String cellId;
	
	public ChartRequestDto(){
		super();
	}

	public long getDayFrom() {
		return dayFrom;
	}

	public void setDayFrom(long dayFrom) {
		this.dayFrom = dayFrom;
	}

	public long getDayTo() {
		return dayTo;
	}

	public void setDayTo(long dayTo) {
		this.dayTo = dayTo;
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}
	
	
}
