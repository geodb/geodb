package com.paradigma.web.controller.dto;

import com.paradigma.utility.model.KeyEnum;
import com.paradigma.utility.model.SeverityEnum;
import com.paradigma.utility.model.TypeIncidentEnum;


public class AggregationDto {

	private int type;
	private int severity;
	private String cellId;
	private KeyEnum keyDesc;
	private long day;
	private double value;

	public AggregationDto() {
		super();
	}
	
	public AggregationDto(int type, int severity,
			String cellId, KeyEnum keyDesc, long day, double value) {
		super();
		this.type = type;
		this.severity = severity;
		this.cellId = cellId;
		this.keyDesc = keyDesc;
		this.day = day;
		this.value = value;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getSeverity() {
		return severity;
	}

	public void setSeverity(int severity) {
		this.severity = severity;
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	public KeyEnum getKeyDesc() {
		return keyDesc;
	}

	public void setKeyDesc(KeyEnum keyDesc) {
		this.keyDesc = keyDesc;
	}

	public long getDay() {
		return day;
	}

	public void setDay(long day) {
		this.day = day;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cellId == null) ? 0 : cellId.hashCode());
		result = prime * result + ((keyDesc == null) ? 0 : keyDesc.hashCode());
		result = prime * result + severity;
		result = prime * result + type;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AggregationDto other = (AggregationDto) obj;
		if (cellId == null) {
			if (other.cellId != null)
				return false;
		} else if (!cellId.equals(other.cellId))
			return false;
		if (keyDesc != other.keyDesc)
			return false;
		if (severity != other.severity)
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Aggregation [type=" + type
				+ ", severity=" + severity + ", cellId=" + cellId
				+ ", keyDesc=" + keyDesc + ", day=" + day + ", value=" + value
				+ "]";
	}

}
