package com.paradigma.web.domain;

public class Prevision {

	private String previsionType;
	private String cellId;
	private double value;
	private int day;
	
	public Prevision(){
		super();
	}

	public String getPrevisionType() {
		return previsionType;
	}

	public void setPrevisionType(String previsionType) {
		this.previsionType = previsionType;
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}
	
}
