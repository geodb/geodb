package com.paradigma.web.controller.dto;

import com.paradigma.utility.model.KeyEnum;

public class ChartResponseDto {

	private long day;
	private KeyEnum keyEnum;
	private int typeEvent;
	private int severity;
	private String cellId;
	private double value;
	
	public ChartResponseDto(){
		super();
	}

	public long getDay() {
		return day;
	}

	public void setDay(long day) {
		this.day = day;
	}

	public KeyEnum getKeyEnum() {
		return keyEnum;
	}

	public void setKeyEnum(KeyEnum keyEnum) {
		this.keyEnum = keyEnum;
	}

	public int getTypeEvent() {
		return typeEvent;
	}

	public void setTypeEvent(int typeEvent) {
		this.typeEvent = typeEvent;
	}

	public int getSeverity() {
		return severity;
	}

	public void setSeverity(int severity) {
		this.severity = severity;
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
}
