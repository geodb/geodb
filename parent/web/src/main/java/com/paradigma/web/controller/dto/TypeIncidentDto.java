package com.paradigma.web.controller.dto;

public class TypeIncidentDto {

	private int id;
    private String description;
    
    public TypeIncidentDto(){
    	super();
    }
    
    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
