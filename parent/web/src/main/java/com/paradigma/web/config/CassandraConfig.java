package com.paradigma.web.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cassandra.config.java.AbstractSessionConfiguration;
import org.springframework.cassandra.core.CqlOperations;
import org.springframework.cassandra.core.CqlTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.paradigma.web.service.AggregationMapper;
import com.paradigma.web.service.CassandraService;
import com.paradigma.web.service.EventRealTimeMapper;
import com.paradigma.web.service.PrevisionMapper;


@Configuration
public class CassandraConfig extends AbstractSessionConfiguration{

  private static final Logger LOG = LoggerFactory.getLogger(CassandraConfig.class);


	    @Override
	    public String getKeyspaceName() {
	        return "geodb";
	    }
	    
	    @Bean
	    public CassandraService cassandraServiceAggregation() throws Exception{
	    	return new CassandraService(new CqlTemplate(session().getObject()), new AggregationMapper());
	    }
	    
	    @Bean
	    public CassandraService cassandraServiceRealtime() throws Exception{
	    	return new CassandraService(new CqlTemplate(session().getObject()), new EventRealTimeMapper());
	    }
	    
	    @Bean
	    public CassandraService cassandraServicePrevision() throws Exception{
	    	return new CassandraService(new CqlTemplate(session().getObject()), new PrevisionMapper());
	    }
  
}
