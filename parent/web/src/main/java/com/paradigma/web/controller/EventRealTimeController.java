package com.paradigma.web.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paradigma.web.config.CassandraConfig;
import com.paradigma.web.controller.dto.ClusterMapEventRealtimeDto;
import com.paradigma.web.controller.dto.EventRealTimeDto;
import com.paradigma.web.domain.EventRealTime;
import com.paradigma.web.service.CassandraService;

@Controller
@RequestMapping("/api/event")
@Import(CassandraConfig.class)
public class EventRealTimeController {

	@Resource(name="cassandraServiceRealtime")
	private CassandraService cassandraServiceRealtime;
	
	@RequestMapping(value = "/realtime", method = RequestMethod.GET)
	public @ResponseBody List<ClusterMapEventRealtimeDto> count() {
		List<EventRealTime> events=cassandraServiceRealtime.getEventsRealtime();
		return cassandraServiceRealtime.getClusterMapEventRealtime(events);
	}
}
