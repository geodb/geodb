package com.paradigma.web.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cassandra.core.RowMapper;

import com.datastax.driver.core.Row;
import com.datastax.driver.core.exceptions.DriverException;
import com.paradigma.web.controller.dto.EventRealTimeDto;
import com.paradigma.web.domain.EventRealTime;

public class EventRealTimeMapper implements RowMapper<EventRealTime> {

	@Override
	public EventRealTime mapRow(Row row, int rowNum) throws DriverException {
		EventRealTime ert = new EventRealTime();
		ert.setType(row.getInt("type"));
		ert.setSeverity(row.getInt("severity"));
		ert.setCellId(row.getString("cellid"));
		ert.setDescription(row.getString("description"));
		ert.setEventId(row.getString("eventid"));
		ert.setFromIncident(row.getString("fromincident"));
		ert.setIncidentId(row.getString("incidentid"));
		ert.setLengthDelay(row.getInt("lengthdelay"));
		ert.setMoment(row.getString("moment"));
		ert.setRoadName(row.getString("roadname"));
		ert.setTimeDelay(row.getInt("timedelay"));
		ert.setToIncident(row.getString("toincident"));
		ert.setX(row.getDouble("x"));
		ert.setY(row.getDouble("y"));
		
		return ert;
	}

	public EventRealTimeDto parserCassandraToDto(EventRealTime eventRealTime){
		EventRealTimeDto eventRealTimeDto = new EventRealTimeDto();
		eventRealTimeDto.setType(eventRealTime.getType());
		eventRealTimeDto.setSeverity(eventRealTime.getSeverity());
		eventRealTimeDto.setCellId(eventRealTime.getCellId());
		eventRealTimeDto.setDescription(eventRealTime.getDescription());
		eventRealTimeDto.setFromIncident(eventRealTime.getFromIncident());
		eventRealTimeDto.setIncidentId(eventRealTime.getIncidentId());
		eventRealTimeDto.setLengthDelay(eventRealTime.getLengthDelay());
		eventRealTimeDto.setMoment(eventRealTime.getMoment());
		eventRealTimeDto.setRoadName(eventRealTime.getRoadName());
		eventRealTimeDto.setTimeDelay(eventRealTime.getTimeDelay());
		eventRealTimeDto.setToIncident(eventRealTime.getToIncident());
		eventRealTimeDto.setX(eventRealTime.getX());
		eventRealTimeDto.setY(eventRealTime.getY());
		
		return eventRealTimeDto;
	}
	
	public List<EventRealTimeDto> toListDto(List<EventRealTime> eventsRealTime){
		List<EventRealTimeDto> eventsRealTimeDto = new ArrayList<>();
		for(EventRealTime e : eventsRealTime){
			EventRealTimeDto eDto = parserCassandraToDto(e);
			eventsRealTimeDto.add(eDto);
		}
		return eventsRealTimeDto;
	}
}
