package com.paradigma.web.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paradigma.utility.model.Cell;
import com.paradigma.utility.model.KeyEnum;
import com.paradigma.utility.model.PrevisionsEnum;
import com.paradigma.utility.model.SeverityEnum;
import com.paradigma.utility.model.TypeIncidentEnum;
import com.paradigma.utility.service.CellService;
import com.paradigma.web.config.CassandraConfig;
import com.paradigma.web.controller.dto.*;
import com.paradigma.web.service.CassandraService;

@Controller
@RequestMapping("/api/utils")
@Import(CassandraConfig.class)
public class UtilController {

	@Resource(name = "cassandraServiceAggregation")
	private CassandraService cassandraServiceAggregation;

	@RequestMapping(value = "/cells", method = RequestMethod.GET)
	public @ResponseBody List<CellDto> getClusterMap() {

		List<CellDto> cells = new ArrayList<>();
		CellService cellService = new CellService();
		for (Cell cellX : cellService.getCellsX()) {
			for (Cell cellY : cellService.getCellsY()) {
				CellDto c = new CellDto();
				c.setCellId(cellX.getId() + "-" + cellY.getId());
				c.setX(cellX.getValue());
				c.setxDist(cellX.getDist());
				c.setY(cellY.getValue());
				c.setyDist(cellY.getDist());
				cells.add(c);
			}
		}
		return cells;
	}

	@RequestMapping(value = "/cellX", method = RequestMethod.GET)
	public @ResponseBody List<Integer> getX() {
		List<Integer> x = new ArrayList<>();
		CellService cellService = new CellService();
		for (int i = 0; i < cellService.getCellsX().size(); i++) {
			x.add(i);
		}
		return x;
	}

	@RequestMapping(value = "/cellY", method = RequestMethod.GET)
	public @ResponseBody List<Integer> getY() {
		List<Integer> y = new ArrayList<>();
		CellService cellService = new CellService();
		for (int i = 0; i < cellService.getCellsY().size(); i++) {
			y.add(i);
		}
		return y;
	}

	@RequestMapping(value = "/typesIncident", method = RequestMethod.GET)
	public @ResponseBody List<TypeIncidentDto> getTypesIncident() {

		List<TypeIncidentDto> typeIncidentsDto = new ArrayList<>();
		for (TypeIncidentEnum t : TypeIncidentEnum.values()) {
			TypeIncidentDto tDto = new TypeIncidentDto();
			tDto.setId(t.getId());
			tDto.setDescription(t.getDescription());
			typeIncidentsDto.add(tDto);
		}
		return typeIncidentsDto;
	}

	@RequestMapping(value = "/severitysIncident", method = RequestMethod.GET)
	public @ResponseBody List<SeverityIncidentDto> getSeveritysIncident() {

		List<SeverityIncidentDto> severityIncidentsDto = new ArrayList<>();
		for (SeverityEnum s : SeverityEnum.values()) {
			SeverityIncidentDto sDto = new SeverityIncidentDto();
			sDto.setId(s.getId());
			sDto.setDescription(s.getDescription());
			severityIncidentsDto.add(sDto);
		}
		return severityIncidentsDto;
	}

	@RequestMapping(value = "/keysDescription", method = RequestMethod.GET)
	public @ResponseBody List<KeyDescDto> getKeysDescription() {

		List<KeyDescDto> keysDescDto = new ArrayList<>();
		for (KeyEnum k : KeyEnum.values()) {
			KeyDescDto kDto = new KeyDescDto();
			kDto.setId(k.getId());
			kDto.setDescription(k.getDescription());
			keysDescDto.add(kDto);
		}
		return keysDescDto;

	}

	@RequestMapping(value = "/days", method = RequestMethod.GET)
	public @ResponseBody List<Long> getDays() {
		return cassandraServiceAggregation.getDaysWithDate();
	}
	
	@RequestMapping(value= "/previsions", method = RequestMethod.GET)
	public @ResponseBody List<PrevisionTypeDto> getTypePrevisions() {
		List<PrevisionTypeDto> previsions= new ArrayList<>();
		for(PrevisionsEnum pe:PrevisionsEnum.values()){
			PrevisionTypeDto pt = new PrevisionTypeDto();
			pt.setDescription(pe.getDescription());
			pt.setId(pe.getId());
			previsions.add(pt);
		}
		return previsions;
	}
	
	@RequestMapping(value= "/rectangleMap", method = RequestMethod.GET)
	public @ResponseBody RectangleMapDto getRectangleMap(){
		RectangleMapDto rMapDto = new RectangleMapDto();
		CellService cellService = new CellService();
		rMapDto.setxEnd(cellService.X_END);
		rMapDto.setxStart(cellService.X_ORIGIN);
		rMapDto.setyEnd(cellService.Y_END);
		rMapDto.setyStart(cellService.Y_ORIGIN);
		return rMapDto;
	}

}
