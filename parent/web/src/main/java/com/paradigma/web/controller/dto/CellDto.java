package com.paradigma.web.controller.dto;

public class CellDto {

	private String cellId;
	private double x;
	private double y;
	private double xDist;
	private double yDist;

	public CellDto() {
		super();
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getxDist() {
		return xDist;
	}

	public void setxDist(double xDist) {
		this.xDist = xDist;
	}

	public double getyDist() {
		return yDist;
	}

	public void setyDist(double yDist) {
		this.yDist = yDist;
	}

}
