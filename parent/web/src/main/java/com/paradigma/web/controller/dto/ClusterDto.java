package com.paradigma.web.controller.dto;

import java.util.List;

public class ClusterDto {

	List<CellDto> cells;
	
	public ClusterDto(){
		super();
	}

	public List<CellDto> getCells() {
		return cells;
	}

	public void setCells(List<CellDto> cells) {
		this.cells = cells;
	}
	
	
}
