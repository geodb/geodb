package com.paradigma.web.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paradigma.web.config.CassandraConfig;
import com.paradigma.web.controller.dto.AggregationDto;
import com.paradigma.web.controller.dto.AggregationRequestDto;
import com.paradigma.web.service.AggregationMapper;
import com.paradigma.web.service.CassandraService;

@Controller
@RequestMapping("/api/aggregation")
@Import(CassandraConfig.class)
public class AggregationController {

	@Resource(name="cassandraServiceAggregation")
	private CassandraService cassandraServiceAggregation;
	
	
	@RequestMapping(value = "/selectInfoAllCells", method = RequestMethod.POST)
	public @ResponseBody List<AggregationDto> selectInfoAllCells(@RequestBody AggregationDto a) {
		AggregationMapper mapper = new AggregationMapper();
		return  cassandraServiceAggregation.selectInfoAllCells(mapper.parserDtotoCassandraRow(a));
	}
	
	@RequestMapping(value = "/selectAllCellsEveryDays", method = RequestMethod.POST)
	public @ResponseBody List<AggregationDto> SelectInfoAllCellsSomeDays(@RequestBody AggregationRequestDto a) {
		return  cassandraServiceAggregation.selectInfoAllCellsSomeDays(a);
	}
}

