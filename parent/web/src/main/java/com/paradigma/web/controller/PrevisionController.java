package com.paradigma.web.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Import;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paradigma.utility.model.PrevisionsEnum;
import com.paradigma.utility.service.CellService;
import com.paradigma.utility.service.DateService;
import com.paradigma.web.config.CassandraConfig;
import com.paradigma.web.controller.dto.PrevisionCellDto;
import com.paradigma.web.controller.dto.PrevisionDto;
import com.paradigma.web.service.CassandraService;

@Controller
@RequestMapping("/api/prevision")
@Import(CassandraConfig.class)
public class PrevisionController {

		
		@Resource(name="cassandraServicePrevision")
		private CassandraService cassandraServicePrevision;

		@RequestMapping(value = "/{previsionId}", method = RequestMethod.GET)
		public @ResponseBody PrevisionDto getPrevisionById(@PathVariable String previsionId){
			DateService dateService = new DateService();
			Calendar cal = Calendar.getInstance();
			int year = cal.get(Calendar.YEAR)*1000;
		    int tomorrow = year+cal.get(Calendar.DAY_OF_YEAR)+1;
		    return cassandraServicePrevision.getPrevisionTomorrow(tomorrow, previsionId);
			
		    
		}
}
