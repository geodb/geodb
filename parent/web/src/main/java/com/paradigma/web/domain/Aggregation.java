package com.paradigma.web.domain;


public class Aggregation {

	private int type;
	private int severity;
	private String cellId;
	private String keyDesc;
	private int day;
	private double value;

	public Aggregation() {
		super();
	}
	
	public Aggregation(int type, int severity,
			String cellId, String keyDesc, int day, double value) {
		super();
		this.type = type;
		this.severity = severity;
		this.cellId = cellId;
		this.keyDesc = keyDesc;
		this.day = day;
		this.value = value;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getSeverity() {
		return severity;
	}

	public void setSeverity(int severity) {
		this.severity = severity;
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	public String getKeyDesc() {
		return keyDesc;
	}

	public void setKeyDesc(String keyDesc) {
		this.keyDesc = keyDesc;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Aggregation [type=" + type
				+ ", severity=" + severity + ", cellId=" + cellId
				+ ", keyDesc=" + keyDesc + ", day=" + day + ", value=" + value
				+ "]";
	}

}
