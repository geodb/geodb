package com.paradigma.web.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.cassandra.core.CqlOperations;
import org.springframework.dao.DataAccessException;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.paradigma.utility.model.Cell;
import com.paradigma.utility.model.KeyEnum;
import com.paradigma.utility.model.SeverityEnum;
import com.paradigma.utility.model.TypeIncidentEnum;
import com.paradigma.utility.service.CellService;
import com.paradigma.utility.service.DateService;
import com.paradigma.web.controller.dto.AggregationDto;
import com.paradigma.web.controller.dto.AggregationRequestDto;
import com.paradigma.web.controller.dto.ChartRequestDto;
import com.paradigma.web.controller.dto.ChartResponseDto;
import com.paradigma.web.controller.dto.ClusterDto;
import com.paradigma.web.controller.dto.ClusterMapEventRealtimeDto;
import com.paradigma.web.controller.dto.EventRealTimeDto;
import com.paradigma.web.controller.dto.PrevisionDto;
import com.paradigma.web.domain.Aggregation;
import com.paradigma.web.domain.EventRealTime;
import com.paradigma.web.domain.Prevision;

public class CassandraService {

	private CqlOperations template;
	
	private AggregationMapper aggregationMapper;
	private EventRealTimeMapper eventRealTimeMapper;
	private PrevisionMapper previsionMapper;

	private String SELECT_CQL_AGGREGATION = "select * from aggregation ";
	private String SELECT_CQL_EVENTREALTIME = "select * from event_realtime ";

	public CassandraService(CqlOperations template, AggregationMapper aggregationMapper) {
		this.template = template;
		this.aggregationMapper= aggregationMapper;
	}
	
	public CassandraService(CqlOperations template,EventRealTimeMapper eventRealTimeMapper){
		this.template=template;
		this.eventRealTimeMapper=eventRealTimeMapper;
	}
	
	public CassandraService(CqlOperations template, PrevisionMapper previsionMapper){
		this.template=template;
		this.previsionMapper=previsionMapper;
	}
	
	public List<AggregationDto> selectInfoAllCells(Aggregation aggregation){
		CellService cs = new CellService();
		String cells = cs.getAllCells();
		String cql = SELECT_CQL_AGGREGATION + "where day =" + aggregation.getDay()
				+ " and keydesc='" + aggregation.getKeyDesc()
				+ "' and cellid IN (" +cells +") and severity=" + aggregation.getSeverity() + " and type ="
				+ aggregation.getType();
		
		System.out.println(cql);
		List<Aggregation> select = template.query(cql, aggregationMapper);
		return aggregationMapper.toListDto(select);
	}
	
	public List<AggregationDto> selectInfoAllCellsSomeDays(AggregationRequestDto a){
		CellService cs = new CellService();
		DateService ds = new DateService();
		List<AggregationDto> finishAggList = new ArrayList<>();
		String cells = cs.getAllCells();
		for(int day=ds.convertDayFrontToCassandra(a.getDayFrom());day<=ds.convertDayFrontToCassandra(a.getDayTo());day++){
			String cql = SELECT_CQL_AGGREGATION + "where day =" + day
					+ " and keydesc='" + a.getKeyDesc()
					+ "' and cellid IN (" +cells +") and severity=" + a.getSeverity() + " and type ="
					+ a.getType();
			
			System.out.println(cql);
			List<Aggregation> select = template.query(cql, aggregationMapper);
			finishAggList=sumValuesOfSomeDays(aggregationMapper.toListDto(select),finishAggList);
		}
		
		return finishAggList;
	}
	
	public List<EventRealTime> getEventsRealtime(){
		String cql = SELECT_CQL_EVENTREALTIME;
		List<EventRealTime> eventsRealTimeDto= template.query(cql, eventRealTimeMapper);
		return eventsRealTimeDto;
	}
	
	public List<Long> getDaysWithDate(){
		String cql = "SELECT DAY from aggregation";
		DateService dateService = new DateService();
		ResultSet result= template.query(cql);
		List<Long> days= new ArrayList<>();
		List<Integer> aux= new ArrayList<>();
		for(Row row : result.all()){
			Integer i = row.getInt("day");
			if(!aux.contains(i) && !i.equals(new Integer(-1))){
				days.add(dateService.convertDayCassandraToFront(i));
				aux.add(i);
			}
		}
		return days;
	}
	
	public PrevisionDto getPrevisionTomorrow(int day, String previsionId){
		String cql= "select * from prevision where day = "+day+" ALLOW FILTERING";
		List<Prevision> previsions= template.query(cql, previsionMapper);
		return previsionMapper.parserCassandraToDto(previsions, previsionId);
	}
	
	public List<ChartResponseDto> selectAggregationToChart(ChartRequestDto chartRequest) {
		List<ChartResponseDto> response = new ArrayList<>();
		DateService ds = new DateService();
		int day= ds.convertDayFrontToCassandra(chartRequest.getDayFrom());
		int year=day/1000;
		int endYear=year*1000;endYear=endYear+366;
		boolean checkChangeyear=false;
		while(day <= ds.convertDayFrontToCassandra(chartRequest.getDayTo())){
			for(KeyEnum key : KeyEnum.values()){
				for(TypeIncidentEnum typeIncident : TypeIncidentEnum.values()){
					for(SeverityEnum severityIncident : SeverityEnum.values()){
						String cql = SELECT_CQL_AGGREGATION + "where day =" + day
								+ " and keydesc='" + key.getId()
								+ "' and cellid IN ('" + chartRequest.getCellId()
								+ "') and severity=" + severityIncident.getId() + " and type ="
								+ typeIncident.getId();
						List<Aggregation> select = template.query(cql, aggregationMapper);
						ChartResponseDto chartResponseDto = new ChartResponseDto();
						if(select.size()>0){
							chartResponseDto=aggregationMapper.parserCassandraToChartDto(select.get(0));
						}else{
							chartResponseDto.setCellId(chartRequest.getCellId());
							chartResponseDto.setDay(ds.convertDayCassandraToFront(day));
							chartResponseDto.setKeyEnum(KeyEnum.getKeyFromId(key.getId()));
							chartResponseDto.setSeverity(severityIncident.getId());
							chartResponseDto.setTypeEvent(typeIncident.getId());
							chartResponseDto.setValue(0.0);
						}
						response.add(chartResponseDto);
					}
				}
			}
			
			if(day>endYear && !checkChangeyear){
				day=year+1;day=day*1000;
				checkChangeyear=true;
			}
			day++;
		}
		return response;
		
	}
	
	
	public List<ClusterMapEventRealtimeDto> getClusterMapEventRealtime(List<EventRealTime> eventsRealtime){
		List<ClusterMapEventRealtimeDto> clusterMapEvents = new ArrayList<>();
		
		CellService cellService = new CellService();
		for (Cell cellX : cellService.getCellsX()) {
			for (Cell cellY : cellService.getCellsY()) {
				ClusterMapEventRealtimeDto clusterMapEventRealtimeDto = new ClusterMapEventRealtimeDto();
				clusterMapEventRealtimeDto.setCellId((cellX.getId()+"-"+cellY.getId()));
				clusterMapEventRealtimeDto.setEventsRealtime(getEventRealTimeInCell(eventsRealtime, clusterMapEventRealtimeDto.getCellId()));
				clusterMapEventRealtimeDto.setNumIncidents(clusterMapEventRealtimeDto.getEventsRealtime().size());
				clusterMapEvents.add(clusterMapEventRealtimeDto);
			}
		}
		
		
		return clusterMapEvents;
	}
	
	
	
	private List<EventRealTimeDto> getEventRealTimeInCell(List<EventRealTime> eventsRealtime, String cellId){
		List<EventRealTimeDto> eventsRealTimeDto = new ArrayList<>();
		for(EventRealTime e:eventsRealtime){
			if(e.getCellId().equals(cellId)){
				EventRealTimeMapper mapper = new EventRealTimeMapper();
				eventsRealTimeDto.add(mapper.parserCassandraToDto(e));
			}
		}
		return eventsRealTimeDto;
	}
	
	private List<AggregationDto> sumValuesOfSomeDays(List<AggregationDto> aggList, List<AggregationDto> finishAgg){
		for(AggregationDto a : aggList){
			if(containsAggCell(a.getCellId(), finishAgg)){
				AggregationDto aux = new AggregationDto();
				aux=obtainAggCell(a.getCellId(), finishAgg);
				finishAgg.remove(aux);
				aux.setValue(aux.getValue()+a.getValue());
				finishAgg.add(aux);
			}
			else{
				finishAgg.add(a);
			}
		}
		return finishAgg;
	}
	private boolean containsAggCell(String cellId, List<AggregationDto> finishAgg){
		boolean result = false;
		for(AggregationDto a : finishAgg){
			if(a.getCellId().equals(cellId)){
				result=true;
			}
		}
		return result;
	}
	private AggregationDto obtainAggCell(String cellId, List<AggregationDto> finishAgg){
		AggregationDto result = new AggregationDto();
		for(AggregationDto a : finishAgg){
			if(a.getCellId().equals(cellId)){
				result=a;
			}
		}
		return result;
	}
}
