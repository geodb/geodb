package com.paradigma.web.controller.dto;

public class PrevisionTypeDto {

	private String id;
	private String description;
	
	public PrevisionTypeDto(){
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
