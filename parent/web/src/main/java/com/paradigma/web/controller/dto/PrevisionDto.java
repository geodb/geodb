package com.paradigma.web.controller.dto;

import java.util.List;

public class PrevisionDto {

	private long day;
	private String typePrevision;
	private List<PrevisionCellDto> previsions;

	public PrevisionDto() {
		super();
	}

	public long getDay() {
		return day;
	}

	public void setDay(long day) {
		this.day = day;
	}

	public String getTypePrevision() {
		return typePrevision;
	}

	public void setTypePrevision(String typePrevision) {
		this.typePrevision = typePrevision;
	}

	public List<PrevisionCellDto> getPrevisions() {
		return previsions;
	}

	public void setPrevisions(List<PrevisionCellDto> previsions) {
		this.previsions = previsions;
	}

}
