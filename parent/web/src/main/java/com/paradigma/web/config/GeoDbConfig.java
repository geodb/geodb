package com.paradigma.web.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.paradigma.web.controller")
public class GeoDbConfig {
}
