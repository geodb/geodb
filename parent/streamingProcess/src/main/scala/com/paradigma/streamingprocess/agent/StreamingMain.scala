package com.paradigma.streamingprocess.agent

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming._
import org.apache.spark.streaming.flume._
import org.apache.spark.util.IntParam
import scala.util.parsing.json.JSON
import java.util.Calendar
import com.paradigma.streamingprocess.factory._
import com.paradigma.utility.service.ClusterMapService
import com.paradigma.utility.model.cassandra.EventVO
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import com.paradigma.utility.service.CellService
import scala.collection.JavaConversions.asScalaBuffer


object StreamingMain {
  def main(args: Array[String]) {
    val batchInterval = Milliseconds(50000)

    lazy val sc = new SparkContext(SparkConfFactory.sparkConf)

    val cal = Calendar.getInstance()
    val year = cal.get(Calendar.YEAR)*1000
    val dayOfTheYear = year+cal.get(Calendar.DAY_OF_YEAR)
    val dayOftheWeek = cal.get(Calendar.DAY_OF_WEEK)
    
    println("today: "+dayOfTheYear)
    println("dia de la semana: "+ dayOftheWeek);

    CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
      session.execute("CREATE KEYSPACE IF NOT EXISTS geodb WITH replication = {'class': 'SimpleStrategy','replication_factor': '1'};")
      session.execute("USE geodb;")
      session.execute("CREATE TABLE IF NOT EXISTS clustermap(cellId text PRIMARY KEY, longitud double, latitud double, distX double, distY double);")
      session.execute("CREATE TABLE IF NOT EXISTS event(eventid text, incidentid text, day int, moment text, x double, y double, description text, roadname text, fromincident text, toincident text, type int, severity int, lengthdelay int, timedelay int, cellid text, dayweek text, PRIMARY KEY (day, incidentid, eventid));")
      session.execute("CREATE TABLE IF NOT EXISTS event_realtime(eventid text, incidentid text, enabled int, moment text, x double, y double, description text, roadname text, fromincident text, toincident text, type int, severity int, lengthdelay int, timedelay int, cellid text, PRIMARY KEY (enabled, eventid));")
      session.execute("CREATE TABLE IF NOT EXISTS aggregation(type int, severity int, value double, cellid text, keydesc text, day int, dayweek text, PRIMARY KEY (keydesc, type, severity, day, cellid));")
      session.execute("CREATE TABLE IF NOT EXISTS weather(cellid text, day int, sunsetTime double, temperatureMinTime double, precipIntensityMaxTime double, visibility double, precipIntensityMax double, apparentTemperatureMaxTime double, temperatureMaxTime double, apparentTemperatureMinTime double,temperatureMin double, precipType_rain double, precipType_snow double, precipType_sleet double, precipType_hail double, sunriseTime double, apparentTemperatureMax double, apparentTemperatureMin double, temperatureMax double, avgPressure double, avgCloudCover double, avgApparentTemperature double, avgPrecipIntensity double, avgTemperature double, avgDewPoint double, avgOzone double, avgWindSpeed double, avgHumidity double, avgWindBearing double, avgPrecipProbability double, PRIMARY KEY (cellid, day));")
      session.execute("CREATE TABLE IF NOT EXISTS trainingdata(cellid text, day int, sunsetTime double, temperatureMinTime double, precipIntensityMaxTime double, visibility double, precipIntensityMax double, apparentTemperatureMaxTime double, temperatureMaxTime double, apparentTemperatureMinTime double,temperatureMin double, precipType_rain double, precipType_snow double, precipType_sleet double, precipType_hail double, sunriseTime double, apparentTemperatureMax double, apparentTemperatureMin double, temperatureMax double, avgPressure double, avgCloudCover double, avgApparentTemperature double, avgPrecipIntensity double, avgTemperature double, avgDewPoint double, avgOzone double, avgWindSpeed double, avgHumidity double, avgWindBearing double, avgPrecipProbability double, numAccidentT3 double, numTrafficJamsT3 double, delayT3 double, dayWorkable double, target_accident double, target_delay double, PRIMARY KEY (cellid, day));");
      session.execute("CREATE TABLE IF NOT EXISTS prevision(previsiontype text, cellid text, value double, day int, PRIMARY KEY(previsiontype, day, cellid));");
      session.execute("CREATE TABLE IF NOT EXISTS modeldata(previsiontype text, cellid text, day int, sunsetTime double, temperatureMinTime double, precipIntensityMaxTime double, visibility double, precipIntensityMax double, apparentTemperatureMaxTime double, temperatureMaxTime double, apparentTemperatureMinTime double,temperatureMin double, precipType_rain double, precipType_snow double, precipType_sleet double, precipType_hail double, sunriseTime double, apparentTemperatureMax double, apparentTemperatureMin double, temperatureMax double, avgPressure double, avgCloudCover double, avgApparentTemperature double, avgPrecipIntensity double, avgTemperature double, avgDewPoint double, avgOzone double, avgWindSpeed double, avgHumidity double, avgWindBearing double, avgPrecipProbability double, numAccidentT3 double, numTrafficJamsT3 double, delayT3 double, dayWorkable double, intercept double, performance double, PRIMARY KEY(day, performance, cellid, previsiontype));")
      session.execute("CREATE TABLE IF NOT EXISTS cellwithaccident(cellid text PRIMARY KEY);")
      val resultSet = session.execute("SELECT * FROM clustermap;")
      if (resultSet.all().size() == 0) {
        val cellService = new CellService()
        val listInserts = cellService.insertOfCellsInClusterMap()
        for (i <- 0 to listInserts.size()
            - 1) {
          session.execute(listInserts.get(i))
        }
      }
      session.close();

    }

    val ssc = new StreamingContext(SparkConfFactory.sparkConf, batchInterval)
    ssc.checkpoint("/tmp/checkpoint")

    // Create a flume stream that polls the Spark Sink running in a Flume agent
    val stream = FlumeUtils.createStream(ssc, "localhost", 3564)

    // Print out the count of events received from this server in each batch
    stream.count().map(cnt => "Received " + cnt + " flume events.").print()

    stream.foreachRDD { rdd =>
      rdd.foreach { value =>
        val avroEvent = value.event
        val json = new String(avroEvent.getBody.array())
        println("*")
        val clusterMapService = new ClusterMapService()
        val traffic = clusterMapService.getInfoTraffic(json)
        println("**")
        val eventList = clusterMapService.proccesClusterInfo(traffic)
        println("***")
        println("----------------- número de eventos de trafico -----------" + eventList.size())
        CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
          session.execute("USE geodb;")

          val deletelastEventRealtime = clusterMapService.deletedLastRealtimeEvents()
          session.execute(deletelastEventRealtime)

          for (i <- 0 to eventList.size() - 1) {

            val insert = clusterMapService.insertsEventTraffic(eventList.get(i))

            val insertNewEventRealtime = clusterMapService.insertsEventRealTime(eventList.get(i))
            session.execute(insertNewEventRealtime)

            println("EVENTO: " + eventList.get(i).getIncidentid)

            val rdd = sc.cassandraTable("geodb", "event").where("incidentid = '" + eventList.get(i).getIncidentid() + "' AND day=" + dayOfTheYear)

            println("count repeat: " + rdd.count())
            if (rdd.count() > 0) {
              println("*******************")
              val lengthDelay = rdd.map { row => row.getInt("lengthdelay") }.max()
              val timeDelay = rdd.map { row => row.getInt("timedelay") }.max()
              val eventId = rdd.map { row => row.getString("eventid") }.first()

              if (lengthDelay > eventList.get(i).getLengthdelay && timeDelay > eventList.get(i).getTimedelay) {
                val queryUpdate = "Update geodb.event SET lengthdelay =" + lengthDelay + ", timedelay=" + timeDelay + " WHERE incidentid='" + eventList.get(i).getIncidentid() + "' AND day=" + dayOfTheYear + " AND eventid='" + eventId + "'"
                println(queryUpdate)
                session.execute(queryUpdate)

              } else if (lengthDelay > eventList.get(i).getLengthdelay) {
                val queryUpdate = "Update geodb.event SET lengthdelay =" + lengthDelay + " WHERE incidentid='" + eventList.get(i).getIncidentid() + "' AND day=" + dayOfTheYear + " AND eventid='" + eventId + "'"
                println(queryUpdate)
                session.execute(queryUpdate)
              } else if (timeDelay > eventList.get(i).getTimedelay) {
                val queryUpdate = "Update geodb.event SET timedelay=" + timeDelay + " WHERE incidentid='" + eventList.get(i).getIncidentid() + "' AND day=" + dayOfTheYear + " AND eventid='" + eventId + "'"
                println(queryUpdate)
                session.execute(queryUpdate)
              }

            } else {
              println(insert)
              session.execute(insert)

            }
          }
          session.close()
        }
      }
    }

    ssc.start()
    ssc.awaitTermination()

  }
}