package com.paradigma.utility.service;

import com.paradigma.utility.model.cassandra.TrainingDataGlobalVO;
import com.paradigma.utility.model.cassandra.TrainingDataWithoutWeatherVO;
import com.paradigma.utility.model.cassandra.WeatherVO;

public class TrainingDataService {

	public TrainingDataService(){
		super();
	}
	
	public String generateInsertTrainingData(WeatherVO w, TrainingDataWithoutWeatherVO t){
		String insert="INSERT INTO trainingdata(cellid, day, sunsetTime, temperatureMinTime, precipIntensityMaxTime," 
				+"visibility, precipIntensityMax, apparentTemperatureMaxTime, temperatureMaxTime, apparentTemperatureMinTime,"
				+"temperatureMin, precipType_rain, precipType_snow, precipType_sleet, precipType_hail, sunriseTime, apparentTemperatureMax,"
				+ " apparentTemperatureMin, temperatureMax, avgPressure, avgCloudCover, avgApparentTemperature, avgPrecipIntensity, avgTemperature, avgDewPoint, avgOzone, avgWindSpeed, avgHumidity, avgWindBearing, avgPrecipProbability, numAccidentT3, numTrafficJamsT3, delayT3, dayWorkable)"
				+"VALUES('"+w.getCellId()+"',"+ w.getDay()+","+ w.getSunsetTime()+","+ w.getTemperatureMinTime()+","+ w.getPrecipIntensityMaxTime()+","+ 
				w.getVisibility()+","+ w.getPrecipIntensityMax()+","+ w.getApparentTemperatureMaxTime()+","+ w.getTemperatureMaxTime()+","+ w.getApparentTemperatureMinTime()+","+
				w.getTemperatureMin()+","+ w.getPrecipType_rain()+","+ w.getPrecipType_snow()+","+ w.getPrecipType_sleet()+","+ w.getPrecipType_hail()+","+ w.getSunriseTime()+","+ 
				w.getApparentTemperatureMax()+","+ w.getApparentTemperatureMin()+","+ w.getTemperatureMax()+","+ w.getAvgPressure()+","+ w.getAvgCloudCover()+","+ w.getAvgApparentTemperature()+","+
				w.getAvgPrecipIntensity()+","+ w.getAvgTemperature()+","+ w.getAvgDewPoint()+","+ w.getAvgOzone()+","+ w.getAvgWindSpeed()+","+ w.getAvgHumidity()+","+ w.getAvgWindBearing()+","+ w.getAvgPrecipProbability()+","+
				t.getNumAccidentT3()+","+t.getNumTrafficJamsT3()+","+ t.getDelayT3()+","+ t.getDayWorkable()+");";
		return insert;
	}
	
	public String generateInsertTrainingDataGlobal(TrainingDataGlobalVO t){
		String insert = "INSERT INTO trainingdata_global(cellid, percentageIncident, avgAccident, avgJam, avgDelay) VALUES('"+
				t.getCellId()+"',"+t.getPercentageIncident()+","+t.getAvgAccident()+","+t.getAvgJam()+","+t.getAvgDelay()+");";
		return insert;
	}
	
	public String generateDeleteOldTrainingDataGlobal(TrainingDataGlobalVO t){
		String delete = "DELETE from trainingdata_global where cellid='"+t.getCellId()+"'";
		return delete;
	}
	
	public String generateUpdateTargetsTrainingData(TrainingDataWithoutWeatherVO t){
		String update = "UPDATE geodb.trainingdata SET target_accident="+t.getTargetAccident()+", target_delay="+t.getTargetDelay() +" WHERE cellid='"+t.getCellId()+"' AND day="+t.getDay();
		return update;
	}
}
