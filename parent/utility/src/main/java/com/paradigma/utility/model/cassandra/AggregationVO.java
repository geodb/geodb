package com.paradigma.utility.model.cassandra;

import java.io.Serializable;

public class AggregationVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private int type;
	private int severity;
	private double value;
	private String cellId;
	private String key;
	private int day;

	public AggregationVO() {
		super();
	}

	public AggregationVO(String id, int type, int severity, double value,
			String cellId, String key, int day) {
		super();
		this.id = id;
		this.type = type;
		this.severity = severity;
		this.value = value;
		this.cellId = cellId;
		this.key = key;
		this.day = day;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getSeverity() {
		return severity;
	}

	public void setSeverity(int severity) {
		this.severity = severity;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

}
