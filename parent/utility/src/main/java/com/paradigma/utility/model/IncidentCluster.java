package com.paradigma.utility.model;

public class IncidentCluster {

    private String timestamp;
    private String idIncident;
    private double x;
    private double y;
    private String description;
    private String roadName;
    private String from;
    private String to;
    private TypeIncidentEnum type;
    private SeverityEnum severity;
    private Coordenate coordenates;
    private int lengthDelay; // Length of delay in meters
    private int timeDelay; // Duration-length of delay in seconds
    

    public IncidentCluster() {
        super();
    }

    public IncidentCluster(String timestamp, String idIncident, double x, double y, String description,
            String roadName, String from, String to, TypeIncidentEnum type, SeverityEnum severity,
            Coordenate coordenates, int lengthDelay, int timeDelay) {
        super();
        this.timestamp = timestamp;
        this.idIncident = idIncident;
        this.x = x;
        this.y = y;
        this.description = description;
        this.roadName = roadName;
        this.from = from;
        this.to = to;
        this.type = type;
        this.severity = severity;
        this.coordenates = coordenates;
        this.lengthDelay = lengthDelay;
        this.timeDelay = timeDelay;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getIdIncident() {
        return idIncident;
    }

    public void setIdIncident(String idIncident) {
        this.idIncident = idIncident;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRoadName() {
        return roadName;
    }

    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public TypeIncidentEnum getType() {
        return type;
    }

    public void setType(TypeIncidentEnum type) {
        this.type = type;
    }

    public SeverityEnum getSeverity() {
        return severity;
    }

    public void setSeverity(SeverityEnum severity) {
        this.severity = severity;
    }

    public Coordenate getCoordenates() {
        return coordenates;
    }

    public void setCoordenates(Coordenate coordenates) {
        this.coordenates = coordenates;
    }

    public int getLengthDelay() {
        return lengthDelay;
    }

    public void setLengthDelay(int lengthDelay) {
        this.lengthDelay = lengthDelay;
    }

    public int getTimeDelay() {
        return timeDelay;
    }

    public void setTimeDelay(int timeDelay) {
        this.timeDelay = timeDelay;
    }

    @Override
    public String toString() {
        return "IncidentTraffic [timestamp=" + timestamp + ", idIncident=" + idIncident + ", x=" + x + ", y=" + y
                + ", description=" + description + ", roadName=" + roadName + ", from=" + from + ", to=" + to
                + ", type=" + type + ", severity=" + severity + ", coordenates=" + coordenates + ", lengthDelay="
                + lengthDelay + ", timeDelay=" + timeDelay + "]";
    }
    
    

}
