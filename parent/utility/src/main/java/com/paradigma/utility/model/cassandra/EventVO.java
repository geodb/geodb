package com.paradigma.utility.model.cassandra;

import java.io.Serializable;

public class EventVO implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String eventid;
    private String incidentid;
    private int day;
    private String moment;
    private double x;
    private double y;
    private String description;
    private String roadname;
    private String fromincident;
    private String toincident;
    private int type;
    private int severity;
    private int lengthdelay;
    private int timedelay;
    private String cellid;

    public EventVO() {
        super();
    }

    public EventVO(String eventid, String incidentid, int day, String moment, double x, double y, String description,
            String roadname, String fromincident, String toincident, int type, int severity, int lengthdelay,
            int timedelay, String cellid) {
        super();
        this.eventid = eventid;
        this.incidentid = incidentid;
        this.day = day;
        this.moment = moment;
        this.x = x;
        this.y = y;
        this.description = description;
        this.roadname = roadname;
        this.fromincident = fromincident;
        this.toincident = toincident;
        this.type = type;
        this.severity = severity;
        this.lengthdelay = lengthdelay;
        this.timedelay = timedelay;
        this.cellid = cellid;
    }

    public String getEventid() {
        return eventid;
    }

    public void setEventid(String eventid) {
        this.eventid = eventid;
    }
    
    public String getIncidentid() {
		return incidentid;
	}

	public void setIncidentid(String incidentid) {
		this.incidentid = incidentid;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRoadname() {
        return roadname;
    }

    public void setRoadname(String roadname) {
        this.roadname = roadname;
    }

    public String getFromincident() {
        return fromincident;
    }

    public void setFromincident(String fromincident) {
        this.fromincident = fromincident;
    }

    public String getToincident() {
        return toincident;
    }

    public void setToincident(String toincident) {
        this.toincident = toincident;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    public int getLengthdelay() {
        return lengthdelay;
    }

    public void setLengthdelay(int lengthdelay) {
        this.lengthdelay = lengthdelay;
    }

    public int getTimedelay() {
        return timedelay;
    }

    public void setTimedelay(int timedelay) {
        this.timedelay = timedelay;
    }

    public String getCellid() {
        return cellid;
    }

    public void setCellid(String cellId) {
        this.cellid = cellId;
    }

    public String getMoment() {
        return moment;
    }

    public void setMoment(String moment) {
        this.moment = moment;
    }

}
