package com.paradigma.utility.service;

import java.util.Calendar;
import java.util.Date;

public class DateService {

	public DateService() {
		super();
	}

	public long convertDayCassandraToFront(int day){
		long result=0;
		String aux= new Integer(day).toString();
		String year=aux.substring(0, 4);
		String dayOfYear=aux.substring(4, 7);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_YEAR, Integer.parseInt(dayOfYear));
		cal.set(Calendar.YEAR, Integer.parseInt(year));
		Date d = new Date(cal.getTimeInMillis());
		System.out.println(d);
		result= d.getTime();
		return result;
	}
	
	public int convertDayFrontToCassandra(long day){
		int result = 0;
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(day);
		int year=cal.get(Calendar.YEAR)*1000;
		int dayOfYear=cal.get(Calendar.DAY_OF_YEAR);

		result=year+dayOfYear;
		System.out.println(result);
		return result;
	}
	
	
}
