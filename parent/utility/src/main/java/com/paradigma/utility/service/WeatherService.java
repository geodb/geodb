package com.paradigma.utility.service;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.dvdme.ForecastIOLib.FIODaily;
import com.github.dvdme.ForecastIOLib.FIOHourly;
import com.github.dvdme.ForecastIOLib.ForecastIO;
import com.paradigma.utility.model.cassandra.WeatherVO;

public class WeatherService implements Serializable {

	private static final long serialVersionUID = 3577791370059771008L;
	private static Logger LOG = LoggerFactory.getLogger(WeatherService.class
			.getName());
	private static String MY_API_KEY = "6831af7ead36634db735afc84993d27c";
	//private static String MY_API_KEY = "df3d343b3cbd42a1f66ca3bee9b66eea";
	
	//cuenta de stratio
	//private static String MY_API_KEY ="2ebc7dad9cc2a481f9964439e78e4c13";

	private static SimpleDateFormat FORMATTER = new SimpleDateFormat(
			"dd-MM-yyyy HH:mm:ss");

	public WeatherVO calculateWeatherInCell(String cellId, double x, double y, int dayTomorrow) {
		WeatherVO weatherVO = new WeatherVO();

		Date now = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(now);
		cal.add(Calendar.DAY_OF_YEAR, 1);
		Date tomorrow = cal.getTime();
		cal.setTime(tomorrow);
		
		String targetDay = FORMATTER.format(tomorrow).split(" ")[0];
		long dayStart = 0;
		long dayEnd = 0;

		try {
			dayStart = FORMATTER.parse(targetDay + " " + "00:00:01").getTime();
			dayEnd = FORMATTER.parse(targetDay + " " + "23:59:59").getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		ForecastIO fio = new ForecastIO(MY_API_KEY); // instantiate the class
														// with the API key.
		fio.setUnits(ForecastIO.UNITS_SI); // sets the units as SI - optional
		fio.getForecast(new Double(y).toString(), new Double(x).toString());
		// sets the latitude and longitude

		// Getting daily params

		FIODaily daily = new FIODaily(fio);

		try {

			weatherVO.setCellId(cellId);
			weatherVO.setDay(dayTomorrow);
			weatherVO.setSunsetTime(FORMATTER.parse(
					targetDay + " " + (daily.getDay(0).sunsetTime()!=null ? daily.getDay(0).sunsetTime(): "00:00:00"))
					.getTime());
			weatherVO.setTemperatureMinTime(FORMATTER.parse(
					targetDay + " "
							+ (daily.getDay(0).temperatureMinTime() !=null ? daily.getDay(0).temperatureMinTime(): "00:00:00"))
					.getTime());
			try{
				weatherVO.setPrecipIntensityMaxTime(FORMATTER.parse(
					targetDay
							+ " "
							+ (daily.getDay(0).precipIntensityMaxTime() != null ?daily.getDay(0).precipIntensityMaxTime() : "00:00:00"))
					.getTime());
			}catch(Exception exc){
				weatherVO.setPrecipIntensityMaxTime(0.0);
			}
			weatherVO.setVisibility(Double.parseDouble(daily.getDay(0).visibility() != null ? daily.getDay(0).visibility().toString() : "-99.0"));
			weatherVO.setPrecipIntensityMax(Double.parseDouble(daily.getDay(0)
					.getByKey("precipIntensityMax")!= null ? daily.getDay(0).getByKey("precipIntensityMax") :"-99.0"));
			weatherVO.setApparentTemperatureMaxTime(FORMATTER.parse(
					targetDay
							+ " "
							+ (daily.getDay(0).apparentTemperatureMaxTime()!=null ? daily.getDay(0).apparentTemperatureMaxTime() : "00:00:00")).getTime());
			weatherVO.setTemperatureMaxTime(FORMATTER.parse(
					targetDay + " "
							+ (daily.getDay(0).temperatureMaxTime()!=null ? daily.getDay(0).temperatureMaxTime():"00:00:00"))
					.getTime());
			weatherVO.setApparentTemperatureMinTime(FORMATTER.parse(
					targetDay
							+ " "
							+ (daily.getDay(0).apparentTemperatureMinTime()!=null ?daily.getDay(0).apparentTemperatureMinTime() : "00:00:00")).getTime() );
			weatherVO.setTemperatureMin(Double.parseDouble(daily.getDay(0)
					.getByKey("temperatureMin")!=null? daily.getDay(0)
							.getByKey("temperatureMin"):"-99.0"));

			Map<String, Double> precipType = codePrecipType(daily.getDay(0).precipType()!=null ? daily.getDay(0).precipType():"none");
			weatherVO.setPrecipType_rain(precipType.get("rain")!=null ? precipType.get("rain"):0.0);
			weatherVO.setPrecipType_snow(precipType.get("snow")!=null ? precipType.get("snow"):0.0);
			weatherVO.setPrecipType_sleet(precipType.get("sleet")!=null ? precipType.get("sleet"):0.0);
			weatherVO.setPrecipType_hail(precipType.get("hail")!=null ? precipType.get("hail"):0.0);
			weatherVO.setSunriseTime(FORMATTER.parse(
					targetDay + " " + (daily.getDay(0).sunriseTime()!=null ? daily.getDay(0).sunriseTime():"00:00:00"))
					.getTime());
			weatherVO.setApparentTemperatureMax(Double.parseDouble(daily
					.getDay(0).apparentTemperatureMax()!=null?daily
							.getDay(0).apparentTemperatureMax().toString():"-99.0"));
			weatherVO.setApparentTemperatureMin(Double.parseDouble(daily
					.getDay(0).apparentTemperatureMin()!=null?daily
							.getDay(0).apparentTemperatureMin().toString():"00:00:00"));
			weatherVO.setTemperatureMax(Double.parseDouble(daily.getDay(0)
					.getByKey("temperatureMax")!=null?daily.getDay(0)
							.getByKey("temperatureMax"):"-99.0"));

		} catch (ParseException e) {
			LOG.error("ERROR PARSER WEATHER: " + e.getMessage(), e);
		}

		// Getting hourly params

		FIOHourly hourly = new FIOHourly(fio);

		List<Double> pressureList = new ArrayList<Double>();
		List<Double> cloudCoverList = new ArrayList<Double>();
		List<Double> apparentTemperatureList = new ArrayList<Double>();
		List<Double> precipIntensityList = new ArrayList<Double>();
		List<Double> temperatureList = new ArrayList<Double>();
		List<Double> dewPointList = new ArrayList<Double>();
		List<Double> ozoneList = new ArrayList<Double>();
		List<Double> windSpeedList = new ArrayList<Double>();
		List<Double> humidityList = new ArrayList<Double>();
		List<Double> windBearingList = new ArrayList<Double>();
		List<Double> precipProbabilityList = new ArrayList<Double>();

		String t = "";

		Date date = null;

		long unixDate;

		// Print daily data
		for (int i = 0; i < hourly.hours(); i++) {

			t = hourly.getHour(i).getByKey("time");

			try {
				date = FORMATTER.parse(t);
			} catch (ParseException e) {
				LOG.error("ERROR PARSER WEATHER: " + e.getMessage(), e);
			}

			unixDate = date.getTime();

			if ((unixDate > dayStart) && (unixDate < dayEnd)) {

				pressureList.add(Double.parseDouble(hourly.getHour(i).getByKey(
						"pressure")!=null ? hourly.getHour(i).getByKey(
								"pressure"):"0.0"));
				cloudCoverList.add(Double.parseDouble(hourly.getHour(i)
						.getByKey("cloudCover")!=null ? hourly.getHour(i).getByKey(
								"cloudCover"):"0.0"));
				apparentTemperatureList.add(Double.parseDouble(hourly
						.getHour(i).getByKey("apparentTemperature")!=null ? hourly.getHour(i).getByKey(
								"apparentTemperature"):"0.0"));
				precipIntensityList.add(Double.parseDouble(hourly.getHour(i)
						.getByKey("precipIntensity")!=null ? hourly.getHour(i).getByKey(
								"precipIntensity"):"0.0"));
				temperatureList.add(Double.parseDouble(hourly.getHour(i)
						.getByKey("temperature")!=null ? hourly.getHour(i).getByKey(
								"temperature"):"0.0"));
				dewPointList.add(Double.parseDouble(hourly.getHour(i).getByKey(
						"dewPoint")!=null ? hourly.getHour(i).getByKey(
								"dewPoint"):"0.0"));
				ozoneList.add(Double.parseDouble(hourly.getHour(i).getByKey(
						"ozone")!=null ? hourly.getHour(i).getByKey(
								"ozone"):"0.0"));
				windSpeedList.add(Double.parseDouble(hourly.getHour(i)
						.getByKey("windSpeed")!=null ? hourly.getHour(i).getByKey(
								"windSpeed"):"0.0"));
				humidityList.add(Double.parseDouble(hourly.getHour(i).getByKey(
						"humidity")!=null ? hourly.getHour(i).getByKey(
								"humidity"):"0.0"));
				windBearingList.add(Double.parseDouble(hourly.getHour(i)
						.getByKey("windBearing")!=null ? hourly.getHour(i).getByKey(
								"windBearing"):"0.0"));
				precipProbabilityList.add(Double.parseDouble(hourly.getHour(i)
						.getByKey("precipProbability")!=null ? hourly.getHour(i).getByKey(
								"precipProbability"):"0.0"));

			}

		}

		weatherVO.setAvgPressure(calculateAvgFromList(pressureList));
		weatherVO.setAvgCloudCover(calculateAvgFromList(cloudCoverList));
		weatherVO
				.setAvgApparentTemperature(calculateAvgFromList(apparentTemperatureList));
		weatherVO.setAvgPrecipIntensity(calculateAvgFromList(precipIntensityList));
		weatherVO.setAvgTemperature(calculateAvgFromList(temperatureList));
		weatherVO.setAvgDewPoint(calculateAvgFromList(dewPointList));
		weatherVO.setAvgOzone(calculateAvgFromList(ozoneList));
		weatherVO.setAvgWindSpeed(calculateAvgFromList(windSpeedList));
		weatherVO.setAvgHumidity(calculateAvgFromList(humidityList));
		weatherVO.setAvgWindBearing(calculateAvgFromList(windBearingList));
		weatherVO
				.setAvgPrecipProbability(calculateAvgFromList(precipProbabilityList));

		return weatherVO;
	}
	
	public String generateInsertWeather(WeatherVO w){
		String insert="INSERT INTO weather(cellid, day, sunsetTime, temperatureMinTime, precipIntensityMaxTime," 
				+"visibility, precipIntensityMax, apparentTemperatureMaxTime, temperatureMaxTime, apparentTemperatureMinTime,"
				+"temperatureMin, precipType_rain, precipType_snow, precipType_sleet, precipType_hail, sunriseTime, apparentTemperatureMax,"
				+ " apparentTemperatureMin, temperatureMax, avgPressure, avgCloudCover, avgApparentTemperature, avgPrecipIntensity, avgTemperature, avgDewPoint, avgOzone, avgWindSpeed, avgHumidity, avgWindBearing, avgPrecipProbability)"
				+"VALUES('"+w.getCellId()+"',"+ w.getDay()+","+ w.getSunsetTime()+","+ w.getTemperatureMinTime()+","+ w.getPrecipIntensityMaxTime()+","+ 
				w.getVisibility()+","+ w.getPrecipIntensityMax()+","+ w.getApparentTemperatureMaxTime()+","+ w.getTemperatureMaxTime()+","+ w.getApparentTemperatureMinTime()+","+
						w.getTemperatureMin()+","+ w.getPrecipType_rain()+","+ w.getPrecipType_snow()+","+ w.getPrecipType_sleet()+","+ w.getPrecipType_hail()+","+ w.getSunriseTime()+","+ w.getApparentTemperatureMax()+","+ w.getApparentTemperatureMin()+","+ w.getTemperatureMax()+","+ w.getAvgPressure()+","+ w.getAvgCloudCover()+","+ w.getAvgApparentTemperature()+","+ w.getAvgPrecipIntensity()+","+ w.getAvgTemperature()+","+ w.getAvgDewPoint()+","+ w.getAvgOzone()+","+ w.getAvgWindSpeed()+","+ w.getAvgHumidity()+","+ w.getAvgWindBearing()+","+ w.getAvgPrecipProbability()+");";
		return insert;
	}

	private double calculateAvgFromList(List<Double> inputList) {

		// System.out.println("In the method!!! The size of the list is " +
		// inputList.size());

		int numElems = inputList.size();

		Double sum = 0.0;

		for (int i = 0; i < inputList.size(); i++) {
			sum = sum + inputList.get(i);
		}

		return sum / numElems;
	}

	private Map<String, Double> codePrecipType(String input) {
		// rain, snow, sleet, hail
		Map<String, Double> codedType = new HashMap<>();

		if (input.toLowerCase().compareTo("rain") == 0) {
			codedType.put("rain", 1.0);
		} else if (input.toLowerCase().compareTo("snow") == 0) {
			codedType.put("snow", 1.0);
		} else if (input.toLowerCase().compareTo("sleet") == 0) {
			codedType.put("sleet", 1.0);
		} else if (input.toLowerCase().compareTo("hail") == 0) {
			codedType.put("hail", 1.0);
		}

		return codedType;
	}
}
