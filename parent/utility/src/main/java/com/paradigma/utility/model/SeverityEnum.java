package com.paradigma.utility.model;

public enum SeverityEnum {
    
    NO_DELAY(0, "No delay"),
    SLOW_TRAFFIC(1, "Slow traffic"),
    QUEUING_TRAFFIC(2, "Queuing traffic"),
    STATIONARY_TRAFFIC(3, "Stationary traffic"),
    CLOSED(4, "Closed"),
    OTHERS(13, "Unknown"),
    ALL_DATA(-1, "All data");
    
    private int id;
    private String description;
    
    private SeverityEnum(int id, String description){
        this.id=id;
        this.description=description;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }
    
    public static SeverityEnum getSeverityFromId(int id){
        SeverityEnum value=null;
        for(SeverityEnum se : values()){
            if(se.getId()==id){
                value=se;
            }
        }
        return value;
    }

}
