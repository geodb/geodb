package com.paradigma.utility.model.cassandra;

import java.io.Serializable;

public class TrainingDataWithoutWeatherVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cellId;
	private int day;
    private double numAccidentT3;
    private double numTrafficJamsT3;
    private double delayT3;
    private double dayWorkable;
    private double targetAccident;
    private double targetDelay;
    
    public TrainingDataWithoutWeatherVO(){
    	super();
    }

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public double getNumAccidentT3() {
		return numAccidentT3;
	}

	public void setNumAccidentT3(double numAccidentT3) {
		this.numAccidentT3 = numAccidentT3;
	}

	public double getNumTrafficJamsT3() {
		return numTrafficJamsT3;
	}

	public void setNumTrafficJamsT3(double numTrafficJamsT3) {
		this.numTrafficJamsT3 = numTrafficJamsT3;
	}

	public double getDelayT3() {
		return delayT3;
	}

	public void setDelayT3(double delayT3) {
		this.delayT3 = delayT3;
	}

	public double getDayWorkable() {
		return dayWorkable;
	}

	public void setDayWorkable(double dayWorkable) {
		this.dayWorkable = dayWorkable;
	}

	public double getTargetAccident() {
		return targetAccident;
	}

	public void setTargetAccident(double targetAccident) {
		this.targetAccident = targetAccident;
	}

	public double getTargetDelay() {
		return targetDelay;
	}

	public void setTargetDelay(double targetDelay) {
		this.targetDelay = targetDelay;
	}
	
	
}
