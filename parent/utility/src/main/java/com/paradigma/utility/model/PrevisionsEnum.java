package com.paradigma.utility.model;

public enum PrevisionsEnum {

	ACCIDENT("ACCIDENT", "Accident"), 
	METRESDELAY("METRESDELAY",
			"Metres delay");

	private String id;
	private String description;

	private PrevisionsEnum(String id, String description) {
		this.id = id;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

}
