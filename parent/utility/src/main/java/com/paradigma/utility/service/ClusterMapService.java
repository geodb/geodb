package com.paradigma.utility.service;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.InvalidPathException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.paradigma.utility.exception.UtilityException;
import com.paradigma.utility.model.*;
import com.paradigma.utility.model.cassandra.*;
import com.paradigma.utility.model.json.*;

public class ClusterMapService implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -872609065061007074L;
	private static Logger LOG = LoggerFactory.getLogger(ClusterMapService.class
			.getName());

	private static String ORIGINAL = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ'¿?`¡!";
	private static String ASCII = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC          ";

	public Traffic getInfoTraffic(String json) {

		Traffic traffic = jsonToObject(json, Traffic.class);
		return traffic;
	}

	public List<EventVO> proccesClusterInfo(Traffic traffic)
			throws UtilityException {

		List<EventVO> events = new ArrayList<>();
		CellService cellService = new CellService();
		List<Cell> coordenatesX = cellService.getCellsX();
		List<Cell> coordenatesY = cellService.getCellsY();
		for (Pous pous : traffic.getTm().getPoi()) {
			if (!pous.getCpoi().isEmpty()) {
				for (Cpous cpous : pous.getCpoi()) {
					IncidentCluster incidentTraffic = obtainIncidentTrafficInfo(
							cpous, traffic.getTm().getId(), coordenatesX,
							coordenatesY);
					EventVO event = parserEventFromIncidentTraffic(incidentTraffic);
					if(!event.getCellid().contains("null")){
						events.add(event);
					}
					
				}
			} else {
				IncidentCluster incidentTraffic = obtainIncidentTrafficInfo(
						pous, traffic.getTm().getId(), coordenatesX,
						coordenatesY);
				EventVO event = parserEventFromIncidentTraffic(incidentTraffic);
				if(!event.getCellid().contains("null")){
					events.add(event);
				}
			}

		}
		return events;

	}

	public String insertsEventTraffic(EventVO event) {
		String query = "";
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR)*1000;
		int dayOfYear = year+cal.get(Calendar.DAY_OF_YEAR);
		DayWeekEnum dayWeek = DayWeekEnum.getDayWeekFromNumDay(cal.get(Calendar.DAY_OF_WEEK));
		query = "INSERT INTO event(eventid, incidentid ,day, moment, x, y, description, roadname, fromincident, toincident, type, severity, lengthdelay, timedelay, cellid, dayweek)"
				+ "VALUES('"
				+ event.getEventid()
				+ "', '"
				+ event.getIncidentid()
				+ "', "
				+ dayOfYear
				+ ", '"
				+ event.getMoment()
				+ "', "
				+ event.getX()
				+ ","
				+ event.getY()
				+ ", '"
				+ removeBadCharacters(event.getDescription())
				+ "', '"
				+ removeBadCharacters(event.getRoadname())
				+ "', '"
				+ removeBadCharacters(event.getFromincident())
				+ "', '"
				+ removeBadCharacters(event.getToincident())
				+ "', "
				+ event.getType()
				+ ", "
				+ event.getSeverity()
				+ ", "
				+ event.getLengthdelay()
				+ ", "
				+ event.getTimedelay()
				+ ", '"
				+ event.getCellid()
				+ "', '"
				+dayWeek.getDay()+"');";

		LOG.info(query);
		return query;
	}

	public String insertsEventRealTime(EventVO event) {
		String query = "INSERT INTO event_realtime(eventid, incidentid ,enabled, moment, x, y, description, roadname, fromincident, toincident, type, severity, lengthdelay, timedelay, cellid)"
				+ "VALUES('"
				+ event.getEventid()
				+ "', '"
				+ event.getIncidentid()
				+ "', "
				+ 1
				+ ", '"
				+ event.getMoment()
				+ "', "
				+ event.getX()
				+ ","
				+ event.getY()
				+ ", '"
				+ removeBadCharacters(event.getDescription())
				+ "', '"
				+ removeBadCharacters(event.getRoadname())
				+ "', '"
				+ removeBadCharacters(event.getFromincident())
				+ "', '"
				+ removeBadCharacters(event.getToincident())
				+ "', "
				+ event.getType()
				+ ", "
				+ event.getSeverity()
				+ ", "
				+ event.getLengthdelay()
				+ ", "
				+ event.getTimedelay()
				+ ", '"
				+ event.getCellid() + "');";
		LOG.info(query);
		return query;
	}

	public String deletedLastRealtimeEvents() {
		String query = "DELETE from event_realtime where enabled=1;";
		LOG.info(query);
		return query;
	}

	private String removeBadCharacters(String input) {
		String output = "";
		if (input != null) {

			output = input;
			for (int i = 0; i < ORIGINAL.length(); i++) {
				output = output.replace(ORIGINAL.charAt(i), ASCII.charAt(i));
			}
		}
		return output;
	}

	private Coordenate obtainCoordenate(double x, double y,
			List<Cell> coordenatesX, List<Cell> coordenatesY)
			throws UtilityException {
		Coordenate coordenate = new Coordenate();

		boolean bucleOut = false;

		for (int i = 0; i < coordenatesX.size() && !bucleOut; i++) {
			if (x < coordenatesX.get(i).getValue()
					+ coordenatesX.get(i).getDist()) {
				coordenate.setX(coordenatesX.get(i).getId());
				bucleOut = true;
			}

		}
		bucleOut = false;

		for (int j = 0; j < coordenatesY.size() && !bucleOut; j++) {
			if (y < coordenatesY.get(j).getValue()
					+ coordenatesY.get(j).getDist()) {
				coordenate.setY(coordenatesY.get(j).getId());
				bucleOut = true;
			}

		}

		LOG.info("Coordenate : " + coordenate.toString());
		LOG.info("X: " + x + ", Y: " + y);

		return coordenate;
	}

	private IncidentCluster obtainIncidentTrafficInfo(Pous pous,
			String timestamp, List<Cell> coordenatesX, List<Cell> coordenatesY)
			throws UtilityException {
		IncidentCluster incidentTraffic = new IncidentCluster();
		Coordenate coordenate = obtainCoordenate(pous.getP().getX(), pous
				.getP().getY(), coordenatesX, coordenatesY);
		incidentTraffic.setCoordenates(coordenate);
		incidentTraffic.setDescription(pous.getD() != null ? pous.getD() : "");
		incidentTraffic.setFrom(pous.getF() != null ? pous.getF() : "");
		incidentTraffic.setIdIncident(pous.getId());
		incidentTraffic.setLengthDelay(pous.getL());
		incidentTraffic.setRoadName(pous.getR() != null ? pous.getR() : "");
		incidentTraffic
				.setSeverity(SeverityEnum.getSeverityFromId(pous.getTy() != null ? pous.getTy() : 13));
		incidentTraffic.setTimeDelay(pous.getDl() != null ? pous.getDl() : 0);
		incidentTraffic.setTimestamp(timestamp);
		incidentTraffic.setTo(pous.getT() != null ? pous.getT() : "");
		System.out.println("TYPE: "+ pous.getIc() != null ? pous.getIc() : 13);
		incidentTraffic.setType(TypeIncidentEnum.getTypeIncidentFromId(pous.getIc() != null ? pous.getIc() : 13));
		incidentTraffic.setX(pous.getP().getX());
		incidentTraffic.setY(pous.getP().getY());
		return incidentTraffic;
	}

	private IncidentCluster obtainIncidentTrafficInfo(Cpous cpous,
			String timestamp, List<Cell> coordenatesX, List<Cell> coordenatesY)
			throws UtilityException {
		IncidentCluster incidentTraffic = new IncidentCluster();
		Coordenate coordenate = obtainCoordenate(cpous.getP().getX(), cpous
				.getP().getY(), coordenatesX, coordenatesY);
		incidentTraffic.setCoordenates(coordenate != null ? coordenate
				: new Coordenate());
		incidentTraffic
				.setDescription(cpous.getD() != null ? cpous.getD() : "");
		incidentTraffic.setFrom(cpous.getF() != null ? cpous.getF() : "");
		incidentTraffic.setIdIncident(cpous.getId());
		incidentTraffic.setLengthDelay(cpous.getL() != null ? cpous.getL() : 0);
		incidentTraffic.setSeverity(SeverityEnum.getSeverityFromId(cpous.getTy() != null ? cpous.getTy() : 13));
		incidentTraffic.setTimeDelay(cpous.getDl() != null ? cpous.getDl() : 0);
		incidentTraffic.setTimestamp(timestamp);
		incidentTraffic.setTo(cpous.getT() != null ? cpous.getT() : "");
		System.out.println("TYPE: "+ cpous.getIc() != null ? cpous.getIc() : 13);
		incidentTraffic.setType(TypeIncidentEnum.getTypeIncidentFromId(cpous.getIc() != null ? cpous.getIc() : 13));
		incidentTraffic.setX(cpous.getP().getX());
		incidentTraffic.setY(cpous.getP().getY());
		return incidentTraffic;
	}

	private EventVO parserEventFromIncidentTraffic(
			IncidentCluster incidentTraffic) {
		EventVO event = new EventVO();

		event.setCellid(incidentTraffic.getCoordenates().getX() + "-"
				+ incidentTraffic.getCoordenates().getY());
		event.setDescription(incidentTraffic.getDescription());
		event.setEventid(UUID.randomUUID().toString());
		event.setIncidentid(incidentTraffic.getIdIncident());
		event.setFromincident(incidentTraffic.getFrom());
		event.setLengthdelay(incidentTraffic.getLengthDelay());
		event.setRoadname(incidentTraffic.getRoadName());
		event.setSeverity(incidentTraffic.getSeverity() != null ? incidentTraffic.getSeverity().getId() : 13);
		event.setTimedelay(incidentTraffic.getTimeDelay());
		event.setMoment(incidentTraffic.getTimestamp());
		event.setToincident(incidentTraffic.getTo());
		event.setType(incidentTraffic.getType() !=null ? incidentTraffic.getType().getId() : 13);
		event.setX(incidentTraffic.getX());
		event.setY(incidentTraffic.getY());

		return event;
	}

	private <T> T jsonToObject(String json, Class<T> clazz) {

		T object = null;

		try {

			// mapper, module
			ObjectMapper mapper = new ObjectMapper();

			// Convert JSON to Java object
			object = mapper.readValue(json, clazz);

		} catch (InvalidPathException e) {
			LOG.error(e.getMessage());

		} catch (IOException e) {
			LOG.error(e.getMessage());
		}

		return object;
	}
}
