package com.paradigma.utility.model.cassandra;

import java.io.Serializable;

public class TrafficVO implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cellId;
    private double longitud;
    private double latitud;

    public TrafficVO() {
        super();
    }

    public TrafficVO(String cellId, double longitud, double latitud) {
        super();
        this.cellId = cellId;
        this.longitud = longitud;
        this.latitud = latitud;
    }

    public String getCellId() {
        return cellId;
    }

    public void setCellId(String cellId) {
        this.cellId = cellId;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

}
