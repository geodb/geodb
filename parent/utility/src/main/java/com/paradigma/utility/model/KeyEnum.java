package com.paradigma.utility.model;

public enum KeyEnum {

	TOTAL("TOTAL","total events"),
	PERCENTAGE("PERCENTAGE","percentage of the events"),
	TOTAL_SEC("TOTAL_SEC","total delay in seconds"),
	TOTAL_M("TOTAL_M","total delay in metres"),
	MAX_SEC("MAX_SEC","maximum delay in seconds"),
	MAX_M("MAX_M","maximum delay in metres");
	
	private String id;
	private String description;
	
	private KeyEnum(String id, String description){
		this.id=id;
		this.description = description;
	}

	public String getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}
	
	public static KeyEnum getKeyFromId(String id){
		KeyEnum value=null;
        for(KeyEnum key : values()){
            if(key.getId().equals(id)){
                value=key;
            }
        }
        return value;
    }
	
}
