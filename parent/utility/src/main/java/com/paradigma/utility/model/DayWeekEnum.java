package com.paradigma.utility.model;

public enum DayWeekEnum {

	SUNDAY(1, "Sunday"),
	MONDAY(2, "Monday"),
	TUESDAY(3, "Tuesday"),
	WEDNESDAY(4, "Wednesday"),
	THURSDAY(5, "Thursday"),
	FRIDAY(6, "Friday"),
	SATURDAY(7, "Saturday"),
	NODAY(-1, "noday");
	
	private int numDay;
	private String day;
	
	private DayWeekEnum(int numDay, String day){
		this.numDay=numDay;
		this.day=day;
		
	}

	public int getNumDay() {
		return numDay;
	}

	public String getDay() {
		return day;
	}
	
	public static DayWeekEnum getDayWeekFromNumDay(int numDay){
		DayWeekEnum value=null;
        for(DayWeekEnum dw : values()){
            if(dw.getNumDay()==numDay){
                value=dw;
            }
        }
        return value;
    }
}
