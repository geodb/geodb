package com.paradigma.utility.model.cassandra;

import java.io.Serializable;

public class ModelDataVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int day;
	private String previsionType;
	private String cellId;
	private double sunsetTime;
	private double temperatureMinTime;
	private double precipIntensityMaxTime;
	private double visibility;
	private double precipIntensityMax;
	private double apparentTemperatureMaxTime;
	private double temperatureMaxTime;
	private double apparentTemperatureMinTime;
	private double temperatureMin;
	private double precipType_rain;
	private double precipType_snow;
	private double precipType_sleet;
	private double precipType_hail;
	private double sunriseTime;
	private double apparentTemperatureMax;
	private double apparentTemperatureMin;
	private double temperatureMax;
	private double avgPressure;
	private double avgCloudCover;
	private double avgApparentTemperature;
	private double avgPrecipIntensity;
	private double avgTemperature;
	private double avgDewPoint;
	private double avgOzone;
	private double avgWindSpeed;
	private double avgHumidity;
	private double avgWindBearing;
	private double avgPrecipProbability;
	private double numAccidentT3;
	private double numTrafficJamsT3;
	private double delayT3;
	private double dayWorkable;
	private double targetAccident;
	private double targetDelay;
	private double intercept;
	private double performance;

	public ModelDataVO() {
		super();
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public String getPrevisionType() {
		return previsionType;
	}

	public void setPrevisionType(String previsionType) {
		this.previsionType = previsionType;
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	public double getSunsetTime() {
		return sunsetTime;
	}

	public void setSunsetTime(double sunsetTime) {
		this.sunsetTime = sunsetTime;
	}

	public double getTemperatureMinTime() {
		return temperatureMinTime;
	}

	public void setTemperatureMinTime(double temperatureMinTime) {
		this.temperatureMinTime = temperatureMinTime;
	}

	public double getPrecipIntensityMaxTime() {
		return precipIntensityMaxTime;
	}

	public void setPrecipIntensityMaxTime(double precipIntensityMaxTime) {
		this.precipIntensityMaxTime = precipIntensityMaxTime;
	}

	public double getVisibility() {
		return visibility;
	}

	public void setVisibility(double visibility) {
		this.visibility = visibility;
	}

	public double getPrecipIntensityMax() {
		return precipIntensityMax;
	}

	public void setPrecipIntensityMax(double precipIntensityMax) {
		this.precipIntensityMax = precipIntensityMax;
	}

	public double getApparentTemperatureMaxTime() {
		return apparentTemperatureMaxTime;
	}

	public void setApparentTemperatureMaxTime(double apparentTemperatureMaxTime) {
		this.apparentTemperatureMaxTime = apparentTemperatureMaxTime;
	}

	public double getTemperatureMaxTime() {
		return temperatureMaxTime;
	}

	public void setTemperatureMaxTime(double temperatureMaxTime) {
		this.temperatureMaxTime = temperatureMaxTime;
	}

	public double getApparentTemperatureMinTime() {
		return apparentTemperatureMinTime;
	}

	public void setApparentTemperatureMinTime(double apparentTemperatureMinTime) {
		this.apparentTemperatureMinTime = apparentTemperatureMinTime;
	}

	public double getTemperatureMin() {
		return temperatureMin;
	}

	public void setTemperatureMin(double temperatureMin) {
		this.temperatureMin = temperatureMin;
	}

	public double getPrecipType_rain() {
		return precipType_rain;
	}

	public void setPrecipType_rain(double precipType_rain) {
		this.precipType_rain = precipType_rain;
	}

	public double getPrecipType_snow() {
		return precipType_snow;
	}

	public void setPrecipType_snow(double precipType_snow) {
		this.precipType_snow = precipType_snow;
	}

	public double getPrecipType_sleet() {
		return precipType_sleet;
	}

	public void setPrecipType_sleet(double precipType_sleet) {
		this.precipType_sleet = precipType_sleet;
	}

	public double getPrecipType_hail() {
		return precipType_hail;
	}

	public void setPrecipType_hail(double precipType_hail) {
		this.precipType_hail = precipType_hail;
	}

	public double getSunriseTime() {
		return sunriseTime;
	}

	public void setSunriseTime(double sunriseTime) {
		this.sunriseTime = sunriseTime;
	}

	public double getApparentTemperatureMax() {
		return apparentTemperatureMax;
	}

	public void setApparentTemperatureMax(double apparentTemperatureMax) {
		this.apparentTemperatureMax = apparentTemperatureMax;
	}

	public double getApparentTemperatureMin() {
		return apparentTemperatureMin;
	}

	public void setApparentTemperatureMin(double apparentTemperatureMin) {
		this.apparentTemperatureMin = apparentTemperatureMin;
	}

	public double getTemperatureMax() {
		return temperatureMax;
	}

	public void setTemperatureMax(double temperatureMax) {
		this.temperatureMax = temperatureMax;
	}

	public double getAvgPressure() {
		return avgPressure;
	}

	public void setAvgPressure(double avgPressure) {
		this.avgPressure = avgPressure;
	}

	public double getAvgCloudCover() {
		return avgCloudCover;
	}

	public void setAvgCloudCover(double avgCloudCover) {
		this.avgCloudCover = avgCloudCover;
	}

	public double getAvgApparentTemperature() {
		return avgApparentTemperature;
	}

	public void setAvgApparentTemperature(double avgApparentTemperature) {
		this.avgApparentTemperature = avgApparentTemperature;
	}

	public double getAvgPrecipIntensity() {
		return avgPrecipIntensity;
	}

	public void setAvgPrecipIntensity(double avgPrecipIntensity) {
		this.avgPrecipIntensity = avgPrecipIntensity;
	}

	public double getAvgTemperature() {
		return avgTemperature;
	}

	public void setAvgTemperature(double avgTemperature) {
		this.avgTemperature = avgTemperature;
	}

	public double getAvgDewPoint() {
		return avgDewPoint;
	}

	public void setAvgDewPoint(double avgDewPoint) {
		this.avgDewPoint = avgDewPoint;
	}

	public double getAvgOzone() {
		return avgOzone;
	}

	public void setAvgOzone(double avgOzone) {
		this.avgOzone = avgOzone;
	}

	public double getAvgWindSpeed() {
		return avgWindSpeed;
	}

	public void setAvgWindSpeed(double avgWindSpeed) {
		this.avgWindSpeed = avgWindSpeed;
	}

	public double getAvgHumidity() {
		return avgHumidity;
	}

	public void setAvgHumidity(double avgHumidity) {
		this.avgHumidity = avgHumidity;
	}

	public double getAvgWindBearing() {
		return avgWindBearing;
	}

	public void setAvgWindBearing(double avgWindBearing) {
		this.avgWindBearing = avgWindBearing;
	}

	public double getAvgPrecipProbability() {
		return avgPrecipProbability;
	}

	public void setAvgPrecipProbability(double avgPrecipProbability) {
		this.avgPrecipProbability = avgPrecipProbability;
	}

	public double getNumAccidentT3() {
		return numAccidentT3;
	}

	public void setNumAccidentT3(double numAccidentT3) {
		this.numAccidentT3 = numAccidentT3;
	}

	public double getNumTrafficJamsT3() {
		return numTrafficJamsT3;
	}

	public void setNumTrafficJamsT3(double numTrafficJamsT3) {
		this.numTrafficJamsT3 = numTrafficJamsT3;
	}

	public double getDelayT3() {
		return delayT3;
	}

	public void setDelayT3(double delayT3) {
		this.delayT3 = delayT3;
	}

	public double getDayWorkable() {
		return dayWorkable;
	}

	public void setDayWorkable(double dayWorkable) {
		this.dayWorkable = dayWorkable;
	}

	public double getTargetAccident() {
		return targetAccident;
	}

	public void setTargetAccident(double targetAccident) {
		this.targetAccident = targetAccident;
	}

	public double getTargetDelay() {
		return targetDelay;
	}

	public void setTargetDelay(double targetDelay) {
		this.targetDelay = targetDelay;
	}

	public double getIntercept() {
		return intercept;
	}

	public void setIntercept(double intercept) {
		this.intercept = intercept;
	}

	public double getPerformance() {
		return performance;
	}

	public void setPerformance(double performance) {
		this.performance = performance;
	}

}
