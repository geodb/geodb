
package com.paradigma.utility.model.json;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "id",
    "p",
    "ic",
    "ty",
    "cs",
    "d",
    "f",
    "t",
    "l",
    "dl"
})
public class Cpous {

    @JsonProperty("id")
    private String id;
    @JsonProperty("p")
    private P_ p;
    @JsonProperty("ic")
    private Integer ic;
    @JsonProperty("ty")
    private Integer ty;
    @JsonProperty("cs")
    private Integer cs;
    @JsonProperty("d")
    private String d;
    @JsonProperty("f")
    private String f;
    @JsonProperty("t")
    private String t;
    @JsonProperty("l")
    private Integer l;
    @JsonProperty("dl")
    private Integer dl;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The id
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The p
     */
    @JsonProperty("p")
    public P_ getP() {
        return p;
    }

    /**
     * 
     * @param p
     *     The p
     */
    @JsonProperty("p")
    public void setP(P_ p) {
        this.p = p;
    }

    /**
     * 
     * @return
     *     The ic
     */
    @JsonProperty("ic")
    public Integer getIc() {
        return ic;
    }

    /**
     * 
     * @param ic
     *     The ic
     */
    @JsonProperty("ic")
    public void setIc(Integer ic) {
        this.ic = ic;
    }

    /**
     * 
     * @return
     *     The ty
     */
    @JsonProperty("ty")
    public Integer getTy() {
        return ty;
    }

    /**
     * 
     * @param ty
     *     The ty
     */
    @JsonProperty("ty")
    public void setTy(Integer ty) {
        this.ty = ty;
    }

    /**
     * 
     * @return
     *     The cs
     */
    @JsonProperty("cs")
    public Integer getCs() {
        return cs;
    }

    /**
     * 
     * @param cs
     *     The cs
     */
    @JsonProperty("cs")
    public void setCs(Integer cs) {
        this.cs = cs;
    }

    /**
     * 
     * @return
     *     The d
     */
    @JsonProperty("d")
    public String getD() {
        return d;
    }

    /**
     * 
     * @param d
     *     The d
     */
    @JsonProperty("d")
    public void setD(String d) {
        this.d = d;
    }

    /**
     * 
     * @return
     *     The f
     */
    @JsonProperty("f")
    public String getF() {
        return f;
    }

    /**
     * 
     * @param f
     *     The f
     */
    @JsonProperty("f")
    public void setF(String f) {
        this.f = f;
    }

    /**
     * 
     * @return
     *     The t
     */
    @JsonProperty("t")
    public String getT() {
        return t;
    }

    /**
     * 
     * @param t
     *     The t
     */
    @JsonProperty("t")
    public void setT(String t) {
        this.t = t;
    }

    /**
     * 
     * @return
     *     The l
     */
    @JsonProperty("l")
    public Integer getL() {
        return l;
    }

    /**
     * 
     * @param l
     *     The l
     */
    @JsonProperty("l")
    public void setL(Integer l) {
        this.l = l;
    }

    /**
     * 
     * @return
     *     The dl
     */
    @JsonProperty("dl")
    public Integer getDl() {
        return dl;
    }

    /**
     * 
     * @param dl
     *     The dl
     */
    @JsonProperty("dl")
    public void setDl(Integer dl) {
        this.dl = dl;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
