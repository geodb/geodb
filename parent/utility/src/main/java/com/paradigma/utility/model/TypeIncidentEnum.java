package com.paradigma.utility.model;

public enum TypeIncidentEnum {
    
    UNKNOW(1, "Unknown"),
    ACCIDENT_CLEARED(3, "Accident cleared"),
    CLOSED(4, "Road closed"),
    TRAFFIC_JAM(6, "Traffic jam"),
    ROADWORK(7, "Roadwork"),
    ACCIDENT(8, "Accident"),
    LONGTREM_ROADWORK(9, "Long-term roadwork"),
    WIND(10, "strong winds"),
    OTHERS(13, "Others"),
    ALL_DATA(-1, "All data");
    
    
    private int id;
    private String description;
    
    private TypeIncidentEnum(int id, String description){
        this.id=id;
        this.description=description;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }
   
    public static TypeIncidentEnum getTypeIncidentFromId(int id){
        TypeIncidentEnum value=null;
        for(TypeIncidentEnum type : values()){
            if(type.getId()==id){
                value=type;
            }
        }
        return value;
    }
    
}
