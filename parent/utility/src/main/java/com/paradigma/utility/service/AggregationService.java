package com.paradigma.utility.service;


public class AggregationService {
	
	public AggregationService(){
		super();
	}
	
	public String generateInsertAggregation(int type, int severity, double value, String cellId, String keydesc, int day, String dayWeek){
		//String query= "INSERT INTO aggregation(aggregationid, type, severity, value, cellid, keydesc, day) VALUES ('"+UUID.randomUUID().toString()+"', "+type+","+severity+", "+value+", '"+cellId+"', '"+keydesc+"', "+day+");";
		String query= "INSERT INTO aggregation(type, severity, value, cellid, keydesc, day, dayweek) VALUES ("+type+","+severity+", "+value+", '"+cellId+"', '"+keydesc+"', "+day+", '"+dayWeek+"');";
		return query;
	}
	
	public String generateWhereAggregation(int type, int severity, String cellId,String keydesc){
		String query="type = " + type +" AND severity= "+severity+" AND cellid IN ('"+cellId+"') AND keydesc= '"+keydesc+"' AND day ="+ -1;
		return query;
	}
	
	public String generateUpdateAggregation(double value, int type, int severity, String cellId, String keydesc){
		//String query = "UPDATE geodb.aggregation SET value="+ value+ " WHERE type = " + type +" AND severity= "+severity+" AND cellid IN('"+cellId+"') AND aggregationid= '"+aggregationId+"' AND keydesc= '"+keydesc+"' AND day ="+ -1;
		String query = "UPDATE geodb.aggregation SET value="+ value+ " WHERE type = " + type +" AND severity= "+severity+" AND cellid ='"+cellId+"' AND keydesc= '"+keydesc+"' AND day ="+ -1;
		return query;
	}

}
