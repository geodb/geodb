package com.paradigma.utility.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.paradigma.utility.model.Cell;;

public class CellService implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1235559738548513709L;

	private static Logger LOG = LoggerFactory.getLogger(CellService.class.getName());

    public final double X_ORIGIN = -1256268.783645;
    public final double X_END = 589226.827272;
    public final double Y_ORIGIN = 4400071.008582;
    public final double Y_END = 5382133.94799;

    private final int X_CELLS = 50;
    private final int Y_CELLS = 25;
    
    private final int X_FACTOR = 111320;
    private final int Y_FACTOR = 121900;

    private double yDist;
    private double xDist;

    public CellService() {
        super();
    }
    
    public List<Cell> getCellsX(){
        List<Cell> cellsX = new ArrayList<>();
        xDist=  Math.abs((X_ORIGIN-X_END)/X_CELLS);
        double value=0.0;
        for(int i=0; i<X_CELLS; i++){
            Cell c = new Cell();
            if(i==0){
                value = X_ORIGIN;
            }
            else{
                value = value+xDist;
            }

            c.setDist(xDist);
            c.setId("X"+i);
            c.setValue(value);
            
            cellsX.add(c);

        }
        return cellsX;
    }
    
    public List<Cell> getCellsY(){
        List<Cell> cellsY = new ArrayList<>();
        yDist= Math.abs((Y_ORIGIN-Y_END)/Y_CELLS);
        double value=0.0;
        for(int i=0; i<Y_CELLS; i++){
            Cell c = new Cell();
            if(i==0){
                value = Y_ORIGIN;
            }
            else{
                value = value+yDist;
            }
            c.setDist(yDist);
            c.setId("Y"+i);
            c.setValue(value);
            
            cellsY.add(c);

        }
        return cellsY;
    }
    
    public List<String> insertOfCellsInClusterMap(){
         List<Cell> coordenatesX = getCellsX();
         List<Cell> coordenatesY = getCellsY();
         
         List<String> inserts = new ArrayList<>();
 		
         for(Cell cx : coordenatesX){
             for(Cell cy : coordenatesY){
            	String query="INSERT INTO clustermap (cellId, longitud, latitud, distX, distY) VALUES ('"+cx.getId()+"-"+cy.getId()+"',"+cx.getValue()+","+cy.getValue()+","+cx.getDist()+","+cy.getDist()+");";
                inserts.add(query);
                LOG.info(query);
             }
         }
         return inserts;
    }

    public double getyDist() {
        return yDist;
    }

    public void setyDist(double yDist) {
        this.yDist = yDist;
    }

    public double getxDist() {
        return xDist;
    }

    public void setxDist(double xDist) {
        this.xDist = xDist;
    }
    
    public String getAllCells(){
    	String result="";
    	for(Cell x : getCellsX()){
    		for(Cell y : getCellsY()){
    			result=result+"'"+x.getId()+"-"+y.getId()+"',";
    		}
    	}
    	return result.substring(0, result.length()-1);
    }
    
    public List<String> obtainCellsInMap(){
    	String y0 = "X0,X1,X2,X3,X4,X5,X6,X7,X8,X9,X10,X11,X12,X13,X28,X29,X30,X31,X32,X33,X34,X35,X36,X37,X38,X39,X40";
		String y1 = "X0,X1,X2,X3,X4,X5,X6,X29,X30,X31,X32,X33,X34,X35,X36,X37,X38,X39,X40,X41,X42,X43,X44,X45,X46,X47,X48,X49";
		String y2 = "X0,X1,X2,X3,X4,X5,X6,X30,X31,X32,X33,X34,X35,X36,X37,X38,X39,X40,X41,X42,X43,X44,X45,X46,X47,X48,X49";
		String y3 = "X0,X1,X2,X3,X4,X5,X6,X32,X33,X34,X35,X36,X37,X38,X39,X40,X41,X42,X43,X44,X45,X46,X47,X48,X49";
		String y4 = "X0,X1,X2,X3,X4,X5,X6,X32,X33,X34,X35,X36,X37,X38,X39,X40,X41,X42,X43,X44,X45,X46,X47,X48,X49";
		String y5 = "X0,X1,X2,X3,X4,X5,X6,X33,X34,X35,X36,X37,X38,X39,X40,X41,X42,X43,X44,X45,X46,X47,X48,X49";
		String y6 = "X0,X1,X2,X3,X4,X5,X35,X36,X37,X39,X40,X41,X42,X43,X44,X45,X46,X47,X48,X49";
		String y7 = "X0,X1,X2,X3,X35,X36,X39,X40,X41,X42,X43,X44,X45,X46,X47,X48,X49";
		String y8 = "X0,X1,X2,X3,X4,X34,X35,X36,X39,X40,X41,X42,X43,X44,X45,X46,X47,X48,X49";
		String y9 = "X0,X1,X2,X3,X4,X5,X34,X35,X36,X37,X38,X39,X40,X41,X44,X45,X46,X47,X48,X49";
		String y10 = "X0,X1,X2,X3,X4,X5,X34,X35,X36,X37,X38,X39,X40,X45,X46,X47,X48,X49";
		String y11 = "X0,X1,X2,X3,X4,X5,X6,X35,X36,X37,X38,X39,X40,X41,X44,X47,X48,X49";
		String y12 = "X0,X1,X2,X3,X4,X5,X6,X35,X36,X37,X38,X39,X40,X41,X42,X43,X44,X45,X46,X47,X48,X49";
		String y13 = "X0,X1,X2,X3,X4,X5,X6,X36,X37,X38,X39,X40,X41,X42,X43,X44,X45,X46,X47,X48,X49";
		String y14 = "X0,X1,X2,X3,X4,X5,X6,X37,X38,X39,X40,X41,X42,X43,X44,X45,X46,X47,X48,X49";
		String y15 = "X0,X1,X2,X3,X4,X5,X6,X7,X38,X39,X40,X41,X42,X43,X44,X45,X46,X47,X48,X49";
		String y16 = "X0,X1,X2,X3,X4,X5,X6,X41,X42,X43,X44,X45,X46,X47,X48,X49";
		String y17 = "X0,X1,X2,X3,X4,X5,X6,X43,X44,X45,X46,X47,X48,X49";
		String y18 = "X0,X1,X2,X3,X4,X5,X6,X44,X45,X46,X47,X48,X49";
		String y19 = "X0,X1,X2,X3,X4,X5,X6,X44,X45,X46,X47,X48,X49";
		String y20 = "X0,X1,X2,X3,X4,X5,X6,X44,X45,X46,X47,X48,X49";
		String y21 = "X0,X1,X2,X3,X4,X5,X44,X45,X46,X47,X48,X49";
		String y22 = "X0,X1,X2,X3,X4,X5,X43,X44,X45,X46,X47,X48,X49";
		String y23 = "X0,X1,X2,X3,X4,X5,X44,X45,X46,X47,X48,X49";
		String y24 = "X0,X1,X2,X3,X4,X5,X6";

		CellService cellService = new CellService();

		List<String> cellsIn = new ArrayList<>();

		for (Cell cellY : cellService.getCellsY()) {
			for (Cell cellX : cellService.getCellsX()) {
				if (cellY.getId().equals("Y0")) {
					if (!y0.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y1")) {
					if (!y1.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y2")) {
					if (!y2.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y3")) {
					if (!y3.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y4")) {
					if (!y4.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y5")) {
					if (!y5.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y6")) {
					if (!y6.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y7")) {
					if (!y7.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y8")) {
					if (!y8.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y9")) {
					if (!y9.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y10")) {
					if (!y10.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y11")) {
					if (!y11.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y12")) {
					if (!y12.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y13")) {
					if (!y13.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y14")) {
					if (!y14.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y15")) {
					if (!y15.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y16")) {
					if (!y16.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y17")) {
					if (!y17.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y18")) {
					if (!y18.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y19")) {
					if (!y19.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y20")) {
					if (!y20.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y21")) {
					if (!y21.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y22")) {
					if (!y22.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y23")) {
					if (!y23.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}
				if (cellY.getId().equals("Y24")) {
					if (!y24.contains(cellX.getId())) {
						cellsIn.add(cellX.getId() + "-" + cellY.getId());
					}
				}

			}

		}
		return cellsIn;
    }
    
    public double getXwithFactor(double x){
    	return x/X_FACTOR;
    }

    public double getYwithFactor(double y){
    	return y/Y_FACTOR;
    }
    
    public Map<String, Double> obtainCenter(double x, double y){
    	Map<String, Double> center = new HashMap<>();
    	double centerX=(x+xDist)/2;
    	double centerY=(y+yDist)/2;
    	center.put("x", centerX);
    	center.put("y", centerY);
    	
    	return center;
    }
    
    public double convertCellInDouble(String cellId){
    	double value=0.0;
    	String x=cellId.split("X")[1].split("-")[0];
    	String y=cellId.split("Y")[1];
    	value=new Double(x.concat(".").concat(y));
    	return value;
    }
    
    public String convertDoubleInCell(double value){
    	String cellId="X0-Y0";
    	String aux= String.valueOf(value);
    	String x=aux.split("\\.")[0];
    	String y=aux.split("\\.")[1];
    	
    	cellId="X".concat(x).concat("-Y").concat(y);
    	
    	return cellId;
    }
}
