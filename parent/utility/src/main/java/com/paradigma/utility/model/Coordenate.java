package com.paradigma.utility.model;

public class Coordenate {

    private String x;
    private String y;
    
    public Coordenate(){
        
    }
    
    public Coordenate(String x, String y){
        this.x=x;
        this.y=y;
    }
    public String getX() {
        return x;
    }
    public void setX(String x) {
        this.x = x;
    }
    public String getY() {
        return y;
    }
    public void setY(String y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Coordenate [x=" + x + ", y=" + y + "]";
    }
    
    
    
}
