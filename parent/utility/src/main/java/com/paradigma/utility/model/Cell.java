package com.paradigma.utility.model;

import java.io.Serializable;

public class Cell implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8379639141828109522L;
	private String id;
    private double value;
    private double dist;

    public Cell() {
        super();
    }

    public Cell(String id, double value, double dist) {
        super();
        this.id = id;
        this.value = value;
        this.dist = dist;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getDist() {
        return dist;
    }

    public void setDist(double dist) {
        this.dist = dist;
    }

    @Override
    public String toString() {
        return "Cell [id=" + id + ", value=" + value + ", dist=" + dist + "]";
    }

    
    
}
