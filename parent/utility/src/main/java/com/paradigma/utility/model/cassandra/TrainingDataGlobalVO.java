package com.paradigma.utility.model.cassandra;

import java.io.Serializable;

public class TrainingDataGlobalVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String cellId;
	private double percentageIncident;
	private double avgAccident;
	private double avgJam;
	private double avgDelay;

	public TrainingDataGlobalVO() {
		super();
	}

	public String getCellId() {
		return cellId;
	}

	public void setCellId(String cellId) {
		this.cellId = cellId;
	}

	public double getPercentageIncident() {
		return percentageIncident;
	}

	public void setPercentageIncident(double percentageIncident) {
		this.percentageIncident = percentageIncident;
	}

	public double getAvgAccident() {
		return avgAccident;
	}

	public void setAvgAccident(double avgAccident) {
		this.avgAccident = avgAccident;
	}

	public double getAvgJam() {
		return avgJam;
	}

	public void setAvgJam(double avgJam) {
		this.avgJam = avgJam;
	}

	public double getAvgDelay() {
		return avgDelay;
	}

	public void setAvgDelay(double avgDelay) {
		this.avgDelay = avgDelay;
	}

}
