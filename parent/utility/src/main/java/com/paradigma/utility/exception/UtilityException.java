package com.paradigma.utility.exception;

public class UtilityException extends Exception{

    /**
     * 
     */
    private static final long serialVersionUID = -8943797528255349231L;

    public UtilityException() {
        super();
    }

    public UtilityException(String message) {
        super(message);
    }

    public UtilityException(String message, Throwable cause) {
        super(message, cause);
    }

    public UtilityException(Throwable cause) {
        super(cause);
    }
}
