package com.paradigma.model.auxiliar

import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import com.paradigma.model.factory.SparkConfFactory
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import com.paradigma.utility.model.cassandra.WeatherVO
import java.util.Arrays.ArrayList
import com.paradigma.utility.model.cassandra.TrainingDataGlobalVO
import com.datastax.spark.connector.rdd.CassandraTableScanRDD
import com.paradigma.utility.model.cassandra.TrainingDataWithoutWeatherVO
import com.paradigma.utility.model.PrevisionsEnum
import org.apache.spark.mllib.linalg._


class ModelAuxiliar {
  def createInsertModelData(day: Int, cellId: String ,typeprevision: String, modelVector: Vector, intercept: Double, performance: Double): String ={
    val query = "INSERT INTO modeldata(previsiontype, cellid, day, sunsetTime, temperatureMinTime, precipIntensityMaxTime, visibility, precipIntensityMax , apparentTemperatureMaxTime , temperatureMaxTime , apparentTemperatureMinTime, temperatureMin, precipType_rain, precipType_snow, precipType_sleet, precipType_hail, sunriseTime, apparentTemperatureMax, apparentTemperatureMin, temperatureMax, avgPressure, avgCloudCover, avgApparentTemperature, avgPrecipIntensity, avgTemperature, avgDewPoint, avgOzone, avgWindSpeed, avgHumidity, avgWindBearing, avgPrecipProbability, numAccidentT3, numTrafficJamsT3, delayT3, dayWorkable, intercept, performance) VALUES('"+typeprevision+"','"+cellId+"',"+ day+","+ modelVector.apply(0)+","+ modelVector.apply(1)+","+ modelVector.apply(2)+","+ modelVector.apply(3)+","+ modelVector.apply(4) +","+ modelVector.apply(5) +","+ modelVector.apply(6) +","+ modelVector.apply(7)+","+ modelVector.apply(8)+","+ modelVector.apply(9)+","+ modelVector.apply(10)+","+ modelVector.apply(11)+","+ modelVector.apply(12)+","+ modelVector.apply(13)+","+ modelVector.apply(14)+","+ modelVector.apply(15)+","+ modelVector.apply(16)+","+ modelVector.apply(17)+","+ modelVector.apply(18)+","+ modelVector.apply(19)+","+ modelVector.apply(20)+","+ modelVector.apply(21)+","+ modelVector.apply(22)+","+ modelVector.apply(23)+","+ modelVector.apply(24)+","+ modelVector.apply(25)+","+ modelVector.apply(26)+","+ modelVector.apply(27)+","+ modelVector.apply(28)+","+ modelVector.apply(29)+","+ modelVector.apply(30)+","+ modelVector.apply(31)+","+ intercept+","+ performance+")"
     query
  }

  def obtainWeatherByDay(rdd: RDD[CassandraRow]): RDD[WeatherVO] = {
    var rddWeather = rdd.map { row =>
      val weather = new WeatherVO()
      weather.setCellId(row.getString("cellid"))
      weather.setApparentTemperatureMax(row.getDouble("apparenttemperaturemax"))
      weather.setApparentTemperatureMaxTime(row.getDouble("apparenttemperaturemaxtime"))
      weather.setApparentTemperatureMin(row.getDouble("apparenttemperaturemin"))
      weather.setApparentTemperatureMinTime(row.getDouble("apparenttemperaturemintime"))
      weather.setAvgApparentTemperature(row.getDouble("avgapparenttemperature"))
      weather.setAvgCloudCover(row.getDouble("avgcloudcover"))
      weather.setAvgDewPoint(row.getDouble("avgdewpoint"))
      weather.setAvgHumidity(row.getDouble("avghumidity"))
      weather.setAvgOzone(row.getDouble("avgozone"))
      weather.setAvgPrecipIntensity(row.getDouble("avgprecipintensity"))
      weather.setAvgPrecipProbability(row.getDouble("avgprecipprobability"))
      weather.setAvgPressure(row.getDouble("avgpressure"))
      weather.setAvgTemperature(row.getDouble("avgtemperature"))
      weather.setAvgWindBearing(row.getDouble("avgwindbearing"))
      weather.setAvgWindSpeed(row.getDouble("avgwindspeed"))
      weather.setPrecipIntensityMax(row.getDouble("precipintensitymax"))
      weather.setPrecipIntensityMaxTime(row.getDouble("precipintensitymaxtime"))
      weather.setPrecipType_hail(row.getDouble("preciptype_hail"))
      weather.setPrecipType_rain(row.getDouble("preciptype_rain"))
      weather.setPrecipType_sleet(row.getDouble("preciptype_sleet"))
      weather.setPrecipType_snow(row.getDouble("preciptype_snow"))
      weather.setSunriseTime(row.getDouble("sunrisetime"))
      weather.setSunsetTime(row.getDouble("sunsettime"))
      weather.setTemperatureMax(row.getDouble("temperaturemax"))
      weather.setTemperatureMaxTime(row.getDouble("temperaturemaxtime"))
      weather.setTemperatureMin(row.getDouble("temperaturemin"))
      weather.setTemperatureMinTime(row.getDouble("temperaturemintime"))
      weather.setVisibility(row.getDouble("visibility"))
      weather

    }

    rddWeather;

  }

  def obtainTrainingDataByDay(rdd: RDD[CassandraRow]): RDD[TrainingDataWithoutWeatherVO] = {

    var rddTrainingData = rdd.map { row =>
      val trainingData = new TrainingDataWithoutWeatherVO()
      trainingData.setCellId(row.getString("cellid"))
      trainingData.setDayWorkable(row.getDouble("dayworkable"))
      trainingData.setDelayT3(row.getDouble("delayt3"))
      trainingData.setNumAccidentT3(row.getDouble("numaccidentt3"))
      trainingData.setNumTrafficJamsT3(row.getDouble("numtrafficjamst3"))

      trainingData.setTargetAccident(row.getDoubleOption("target_accident").getOrElse(0.0))
      trainingData.setTargetDelay(row.getDoubleOption("target_delay").getOrElse(0.0))
      trainingData
    }

    rddTrainingData
  }

}