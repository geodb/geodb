package com.paradigma.model.factory

import org.apache.spark.SparkConf


object SparkConfFactory extends Serializable{
  val sparkConf = new SparkConf().setAppName("BatchEvent").setMaster("local[2]").set("spark.ui.port", "7077").set("spark.mesos.coarse", "true")
      .set("spark.cassandra.connection.host", "localhost").set("spark.driver.allowMultipleContexts", "true")
}