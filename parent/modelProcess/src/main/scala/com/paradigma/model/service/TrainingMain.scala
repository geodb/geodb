package com.paradigma.model.service

import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import com.paradigma.model.factory.SparkConfFactory
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import com.paradigma.utility.model.cassandra.WeatherVO
import java.util.Arrays.ArrayList
import com.paradigma.utility.model.cassandra.TrainingDataGlobalVO
import com.datastax.spark.connector.rdd.CassandraTableScanRDD
import com.paradigma.utility.model.cassandra.TrainingDataWithoutWeatherVO
import java.util.Calendar
import org.apache.spark.mllib.linalg._
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.classification.LogisticRegressionWithSGD
import org.apache.spark.mllib.regression.LinearRegressionWithSGD
import org.apache.spark.ml.classification.LogisticRegression
import com.paradigma.utility.model.PrevisionsEnum
import scala.collection.mutable.Set
import com.paradigma.model.auxiliar.ModelAuxiliar
import com.paradigma.utility.service.CellService
import org.apache.spark.mllib.feature.StandardScaler
import java.io._



object TrainingMain {
  def main(args: Array[String]) {

    lazy val sc = new SparkContext(SparkConfFactory.sparkConf)
    
    /*val TextFile = new File("/home/geodb/datosEntrenamiento.txt");
    val TextOut = new FileWriter(TextFile, true);
    TextOut.write("(cellService.convertCellInDouble(arrayCells, weather.getSunsetTime, weather.getTemperatureMinTime, weather.getPrecipIntensityMaxTime, weather.getVisibility, weather.getPrecipIntensityMax, weather.getApparentTemperatureMaxTime, weather.getTemperatureMaxTime, weather.getApparentTemperatureMinTime, weather.getTemperatureMin, weather.getPrecipType_rain, weather.getPrecipType_snow, weather.getPrecipType_sleet, weather.getPrecipType_hail, weather.getSunriseTime, weather.getApparentTemperatureMax, weather.getApparentTemperatureMin, weather.getTemperatureMax, weather.getAvgPressure, weather.getAvgCloudCover, weather.getAvgApparentTemperature, weather.getAvgPrecipIntensity, weather.getAvgTemperature, weather.getAvgDewPoint, weather.getAvgOzone, weather.getAvgWindSpeed,weather.getAvgHumidity, weather.getAvgWindBearing, weather.getAvgPrecipProbability, trainingData.getNumAccidentT3, trainingData.getNumTrafficJamsT3,trainingData.getDelayT3,trainingData.getDayWorkable)\r\n");
		*/
    val cellService = new CellService()
    val modelAuxiliar = new ModelAuxiliar()
    val cal = Calendar.getInstance()
    val year = cal.get(Calendar.YEAR) * 1000
    val today = year + cal.get(Calendar.DAY_OF_YEAR)

    val rddWeatherCassandra = sc.cassandraTable("geodb", "weather")
    val rddGlobalDataCassandra = sc.cassandraTable("geodb", "trainingdata_global")
    val rddTrainingDataCassandra = sc.cassandraTable("geodb", "trainingdata")
    val rddCellWithAccident = sc.cassandraTable("geodb", "cellwithaccident")

    var arrayVector = Array[Vector]()
    var arrayVectorLinearTarget = Array[Vector]()
    var arrayVectorLogisticTarget = Array[Vector]()
    var arrayCellsWithAccident = rddCellWithAccident.map { row => row.getString("cellid") }.collect()

    val arrayDays = rddWeatherCassandra.map { row => row.getInt("day") }.distinct().collect()

    for (i <- 0 to arrayDays.length - 1) {
      if (arrayDays.apply(i) < today) {
        println("day " + arrayDays.apply(i))
        val rddWeatherByDay = rddWeatherCassandra.filter { row => row.getInt("day") == arrayDays.apply(i) }

        val rddTrainingDataByDayCassandra = rddTrainingDataCassandra.filter { row => row.getInt("day") == arrayDays.apply(i) }
        val rddTraingData = modelAuxiliar.obtainTrainingDataByDay(rddTrainingDataByDayCassandra)
        val rddWeather = modelAuxiliar.obtainWeatherByDay(rddWeatherCassandra)

        // modeldata(previsiontype, cellid, day , sunsetTime , temperatureMinTime , precipIntensityMaxTime , visibility , 
        //   precipIntensityMax , apparentTemperatureMaxTime , temperatureMaxTime , apparentTemperatureMinTime ,
        //   temperatureMin , precipType_rain , precipType_snow , precipType_sleet , precipType_hail , sunriseTime , 
        //   apparentTemperatureMax , apparentTemperatureMin , temperatureMax , avgPressure , avgCloudCover ,
        //   avgApparentTemperature , avgPrecipIntensity , avgTemperature , avgDewPoint , avgOzone , avgWindSpeed , 
        //   avgHumidity , avgWindBearing , avgPrecipProbability , numAccidentT3 , numTrafficJamsT3 , delayT3 ,
        //   dayWorkable , percentageIncident , avgAccident , avgJam , avgDelay , intercept , performance , 

        if (!rddTraingData.isEmpty()) {

          val arrayCells = cellService.obtainCellsInMap()
          for (j <- 0 to arrayCells.size() - 1) {
            println("cell ->" + arrayCells.get(j))

            val rddTrainingDataCheck = rddTraingData.filter { t => t.getCellId.equals(arrayCells.get(j)) }

            if (!rddTrainingDataCheck.isEmpty()) {

              val weather = rddWeather.filter { w => w.getCellId.equals(arrayCells.get(j)) }.first()
              val trainingData = rddTrainingDataCheck.first()

              if (trainingData.getTargetAccident == 1.0 || arrayCellsWithAccident.contains(arrayCells.get(j))) {

                val arrayModel = Array(cellService.convertCellInDouble(arrayCells.get(j)), weather.getSunsetTime, weather.getTemperatureMinTime, weather.getPrecipIntensityMaxTime, weather.getVisibility,
                  weather.getPrecipIntensityMax, weather.getApparentTemperatureMaxTime, weather.getTemperatureMaxTime, weather.getApparentTemperatureMinTime,
                  weather.getTemperatureMin, weather.getPrecipType_rain, weather.getPrecipType_snow, weather.getPrecipType_sleet, weather.getPrecipType_hail, weather.getSunriseTime,
                  weather.getApparentTemperatureMax, weather.getApparentTemperatureMin, weather.getTemperatureMax, weather.getAvgPressure, weather.getAvgCloudCover,
                  weather.getAvgApparentTemperature, weather.getAvgPrecipIntensity, weather.getAvgTemperature, weather.getAvgDewPoint, weather.getAvgOzone, weather.getAvgWindSpeed,
                  weather.getAvgHumidity, weather.getAvgWindBearing, weather.getAvgPrecipProbability, trainingData.getNumAccidentT3, trainingData.getNumTrafficJamsT3, trainingData.getDelayT3,
                  trainingData.getDayWorkable)
                  
                  /*TextOut.write((cellService.convertCellInDouble(arrayCells.get(j))+","+ weather.getSunsetTime+","+ weather.getTemperatureMinTime+","+ weather.getPrecipIntensityMaxTime+","+ weather.getVisibility+","+
                  weather.getPrecipIntensityMax+","+ weather.getApparentTemperatureMaxTime+","+ weather.getTemperatureMaxTime+","+ weather.getApparentTemperatureMinTime+","+
                  weather.getTemperatureMin+","+ weather.getPrecipType_rain+","+ weather.getPrecipType_snow+","+ weather.getPrecipType_sleet+","+ weather.getPrecipType_hail+","+ weather.getSunriseTime+","+
                  weather.getApparentTemperatureMax+","+ weather.getApparentTemperatureMin+","+ weather.getTemperatureMax+","+ weather.getAvgPressure+","+ weather.getAvgCloudCover+","+
                  weather.getAvgApparentTemperature+","+ weather.getAvgPrecipIntensity+","+ weather.getAvgTemperature+","+ weather.getAvgDewPoint+","+ weather.getAvgOzone+","+ weather.getAvgWindSpeed+","+
                  weather.getAvgHumidity+","+ weather.getAvgWindBearing+","+ weather.getAvgPrecipProbability+","+ trainingData.getNumAccidentT3+","+ trainingData.getNumTrafficJamsT3+","+ trainingData.getDelayT3+","+
                  trainingData.getDayWorkable)+"\r\n");*/

                println("ARRAY MODEL +++++++++" + arrayModel.apply(1)+","+arrayModel.apply(2)+","+arrayModel.apply(10))
                val arrayModelLogisticTarget = Array(trainingData.getTargetAccident, cellService.convertCellInDouble(arrayCells.get(j)))

                val arrayModelLinearTarget = Array(trainingData.getTargetDelay, cellService.convertCellInDouble(arrayCells.get(j)))

                val vectorModel = Vectors.dense(arrayModel)
                val vectorModelLinearTarget = Vectors.dense(arrayModelLinearTarget)
                val vectorModelLogisticTarget = Vectors.dense(arrayModelLogisticTarget)

                arrayVector = arrayVector ++ Array(vectorModel)
                arrayVectorLinearTarget = arrayVectorLinearTarget ++ Array(vectorModelLinearTarget)
                arrayVectorLogisticTarget = arrayVectorLogisticTarget ++ Array(vectorModelLogisticTarget)

                if (!arrayCellsWithAccident.contains(arrayCells.get(j))) {
                  arrayCellsWithAccident = arrayCellsWithAccident ++ Array(arrayCells.get(j))
                }

                println("+++++++++++++++TARGETS+++++++++++++")
                println("accident: " + trainingData.getTargetAccident)
                println("delay: " + trainingData.getTargetDelay)
              }
            } else {
              println("********NO DATA IN THE CELL " + arrayCells.get(j) + "******")
            }
          }
        } else {
          println("********NO DATA IN THE DAY " + arrayDays.apply(i) + "******")
        }
      }
    }

    println("-------------------NORMALIZAR----------")
    //TextOut.write("-------------------NORMALIZAR----------\r\n");
    for (z <- 0 to arrayCellsWithAccident.length - 1) {

      println("--------cell------------ " + arrayCellsWithAccident.apply(z))
      val rddCellArrayVector = sc.parallelize(arrayVector).filter { row => cellService.convertDoubleInCell(row.apply(0)).equals(arrayCellsWithAccident.apply(z)) }
      if (!rddCellArrayVector.isEmpty()) {
        val scalerCell = new StandardScaler()
        val scalerModelCell = scalerCell.fit(rddCellArrayVector)
        val rddTransformCell = scalerModelCell.transform(rddCellArrayVector)
        

        println("***finish with the days***")
        var arrayLabeledLogisticPointCell = Array[LabeledPoint]()
        var arrayLabeledLinearPointCell = Array[LabeledPoint]()
        val arrayTransformCell = rddTransformCell.collect()
        for (i <- 0 to arrayTransformCell.length - 1) {
          //TextOut.write(arrayTransformCell.apply(i).toString()+"\r\n");
          println("-*-*-*-*-*-*-*-*-*-" + arrayTransformCell.apply(i).toString())

          val labeledPointLogisticModelCell = new LabeledPoint(arrayVectorLogisticTarget.filter { row => cellService.convertDoubleInCell(row.apply(1)).equals(arrayCellsWithAccident.apply(z)) }.apply(i).apply(0), arrayTransformCell.apply(i))
          arrayLabeledLogisticPointCell = arrayLabeledLogisticPointCell ++ Array(labeledPointLogisticModelCell)
          val laberedPointLinearModel = new LabeledPoint(arrayVectorLinearTarget.filter { row => cellService.convertDoubleInCell(row.apply(1)).equals(arrayCellsWithAccident.apply(z)) }.apply(i).apply(0), arrayTransformCell.apply(i))
          arrayLabeledLinearPointCell = arrayLabeledLinearPointCell ++ Array(laberedPointLinearModel)
        }

        println("size logistic: " + arrayLabeledLogisticPointCell.size + " and linear: " + arrayLabeledLinearPointCell.size)

        val rddLabeledLogisticPointCell = sc.parallelize(arrayLabeledLogisticPointCell)
        val rddLabeledLinearPointCell = sc.parallelize(arrayLabeledLinearPointCell)

        val logisticRegressionModelCell = LogisticRegressionWithSGD.train(rddLabeledLogisticPointCell, 3)
        val linearRegressionModelCell = LinearRegressionWithSGD.train(rddLabeledLinearPointCell, 3)
        

        val queryModelAccidentCell = modelAuxiliar.createInsertModelData(today, arrayCellsWithAccident.apply(z), PrevisionsEnum.ACCIDENT.getId, logisticRegressionModelCell.weights, logisticRegressionModelCell.intercept, 0.0)
        val queryModelDelayCell = modelAuxiliar.createInsertModelData(today, arrayCellsWithAccident.apply(z), PrevisionsEnum.METRESDELAY.getId, linearRegressionModelCell.weights, linearRegressionModelCell.intercept, 0.0)

         println(queryModelAccidentCell)
         println(queryModelDelayCell)
        CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
          session.execute("USE geodb;")

          session.execute(queryModelAccidentCell)
          session.execute(queryModelDelayCell)

          session.close()
        }
      }
    } //end for cells with data

    CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
      session.execute("USE geodb;")
      for (z <- 0 to arrayCellsWithAccident.length - 1) {
        val query = "INSERT INTO cellwithaccident(cellid) VALUES('" + arrayCellsWithAccident.apply(z) + "') IF NOT EXISTS"
        session.execute(query)
      }
      session.close()
    }
    //TextOut.close();
  }
}