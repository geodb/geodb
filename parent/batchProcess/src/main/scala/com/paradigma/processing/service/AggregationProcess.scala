package com.paradigma.processing.service

import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import java.util.Calendar
import com.paradigma.processing.factory.SparkConfFactory
import org.apache.spark.SparkContext
import com.paradigma.utility.model.cassandra.EventVO
import com.paradigma.utility.service.CellService
import com.paradigma.utility.model._
import java.util.ArrayList
import java.util.UUID
import java.text._
import java.util.Date
import com.paradigma.utility.service.AggregationService
import org.glassfish.grizzly.http.server.Session
import com.paradigma.processing.auxiliar.AggregationAuxiliar


class AggregationProcess {
  def aggregationProcess(sc: SparkContext) {

    val aggregationService = new AggregationService()
    val aggregationAuxiliar = new AggregationAuxiliar()
    val cellService = new CellService()

    val cal = Calendar.getInstance()
    val year = cal.get(Calendar.YEAR)*1000
    val yesterday = year+cal.get(Calendar.DAY_OF_YEAR)
    var numDayOfWeek = cal.get(Calendar.DAY_OF_WEEK)
    /*if(numDayOfWeek==1){
      numDayOfWeek=7;
    }
    else{
      numDayOfWeek=numDayOfWeek-1;
    }*/

    val dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    val dayWeek = DayWeekEnum.getDayWeekFromNumDay(numDayOfWeek)

    println("day of year " + yesterday)

    val rdd = sc.cassandraTable("geodb", "event").where("day = " + yesterday)

    CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
      session.execute("USE geodb;")

      val listCell = rdd.map { row => row.getString("cellid") }.distinct().collect()
      //val listCell = cellService.obtainCellsInMap()
      println("---------------------------------")
      println("CELLS")
      for (i <- 0 to listCell.length - 1) {
        val cellRdd = rdd.filter { row => row.getString("cellid").equals(listCell.apply(i)) }
        val eventsCell = cellRdd.count()
        val queryTotalEvents = aggregationService.generateInsertAggregation(TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, eventsCell.toInt, listCell.apply(i), KeyEnum.TOTAL.getId, yesterday, dayWeek.getDay)
        println(queryTotalEvents)
        session.execute(queryTotalEvents)
        println("cell: " + listCell.apply(i) + "->" + eventsCell)

        println("-------------------------------")

        val typeIncidentArray = TypeIncidentEnum.values()
        val severityArray = SeverityEnum.values()

        for (z <- 0 to typeIncidentArray.length - 1) {
          for (k <- 0 to severityArray.length - 1) {
            val totalIncidentType = cellRdd.filter { row => row.getInt("type") == typeIncidentArray.apply(z).getId }.count()
            val totalSeverity = cellRdd.filter { row => row.getInt("severity") == severityArray.apply(k).getId }.count()
            val rddIncidentPerSeverity = cellRdd.filter { row => (row.getInt("type") == typeIncidentArray.apply(z).getId && row.getInt("severity") == severityArray.apply(k).getId) }
            println("----------------------------------")
            if (severityArray.apply(k).equals(SeverityEnum.ALL_DATA) && typeIncidentArray.apply(z).equals(TypeIncidentEnum.ALL_DATA)) {
              val querysSumMax = aggregationAuxiliar.sumAndMaxAggregationInDayTotal(cellRdd, TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, listCell.apply(i), yesterday, dayWeek.getDay)
              for (u <- 0 to querysSumMax.size() - 1) {
                session.execute(querysSumMax.get(u))
              }
            } else {
              if (typeIncidentArray.apply(z).equals(TypeIncidentEnum.ALL_DATA)) {
                if (!severityArray.apply(k).equals(SeverityEnum.ALL_DATA)) {
                  val querysSeverity = aggregationAuxiliar.totalAndPercentageAggregationInDay(totalSeverity.toInt, eventsCell.toInt, TypeIncidentEnum.ALL_DATA.getId, severityArray.apply(k).getId, listCell.apply(i), yesterday, dayWeek.getDay)
                  for (q <- 0 to querysSeverity.size() - 1) {
                    println(querysSeverity.get(q))
                    session.execute(querysSeverity.get(q))
                  }
                }
              } else if (severityArray.apply(k).equals(SeverityEnum.ALL_DATA)) {
                if (!typeIncidentArray.apply(z).equals(TypeIncidentEnum.ALL_DATA)) {
                  val querysType = aggregationAuxiliar.totalAndPercentageAggregationInDay(totalIncidentType.toInt, eventsCell.toInt, typeIncidentArray.apply(z).getId, SeverityEnum.ALL_DATA.getId, listCell.apply(i), yesterday, dayWeek.getDay)
                  for (q <- 0 to querysType.size() - 1) {
                    println(querysType.get(q))
                    session.execute(querysType.get(q))
                  }
                }

              } else {
                println("rdd is empty?? --->" + rddIncidentPerSeverity.isEmpty())
                if (!rddIncidentPerSeverity.isEmpty()) {
                  val totalIncidentPerSeverity = cellRdd.filter { row => (row.getInt("type") == typeIncidentArray.apply(z).getId && row.getInt("severity") == severityArray.apply(k).getId) }.count()
                  val querysTypePerSeverity = aggregationAuxiliar.totalAndPercentageAggregationInDay(totalIncidentPerSeverity.toInt, eventsCell.toInt, typeIncidentArray.apply(z).getId, severityArray.apply(k).getId, listCell.apply(i), yesterday, dayWeek.getDay)
                  for (q <- 0 to querysTypePerSeverity.size() - 1) {
                    println(querysTypePerSeverity.get(q))
                    session.execute(querysTypePerSeverity.get(q))
                  }
                  println("--------------MAX--------------------")
                  val querysSumMax = aggregationAuxiliar.sumAndMaxAggregationInDayTotal(rddIncidentPerSeverity, typeIncidentArray.apply(z).getId, severityArray.apply(k).getId, listCell.apply(i), yesterday, dayWeek.getDay)
                  for (u <- 0 to querysSumMax.size() - 1) {
                    session.execute(querysSumMax.get(u))
                  }
                }
              }

            }
          }
        }
      } //end cells

      println("----------------TOTAL ----------------")
      val totalEvents = rdd.cassandraCount()
      println("totalEvents: " + totalEvents)
      println("CLUSTER")

      val queryTotalEvents = aggregationService.generateInsertAggregation(TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, totalEvents.toInt, "CLUSTER", KeyEnum.TOTAL.getId, yesterday, dayWeek.getDay)
      println(queryTotalEvents)
      session.execute(queryTotalEvents)

      println("----------------TYPE PER SEVERITY -----------------")
      val typeIncidentArray = TypeIncidentEnum.values()
      val severityArray = SeverityEnum.values()

      for (i <- 0 to typeIncidentArray.length - 1) {
        for (j <- 0 to severityArray.length - 1) {
          val totalIncidentType = rdd.filter { row => row.getInt("type") == typeIncidentArray.apply(i).getId }.count()
          val totalSeverity = rdd.filter { row => row.getInt("severity") == severityArray.apply(j).getId }.count()
          val rddIncidentPerSeverity = rdd.filter { row => (row.getInt("type") == typeIncidentArray.apply(i).getId && row.getInt("severity") == severityArray.apply(j).getId) }

          if (severityArray.apply(j).equals(SeverityEnum.ALL_DATA) && typeIncidentArray.apply(i).equals(TypeIncidentEnum.ALL_DATA)) {
            println("-----------------SUM AND MAX LENGTH, TIME------------------")
            val querysSumMax = aggregationAuxiliar.sumAndMaxAggregationInDayTotal(rdd, TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, "CLUSTER", yesterday, dayWeek.getDay)
            for (j <- 0 to querysSumMax.size() - 1) {
              session.execute(querysSumMax.get(j))
            }

          } else {
            println("----------------TOTAL TYPE PER SEVERITY -----------------")
            if (typeIncidentArray.apply(i).equals(TypeIncidentEnum.ALL_DATA)) {
              if (!severityArray.apply(j).equals(SeverityEnum.ALL_DATA)) {
                val querysSeverity = aggregationAuxiliar.totalAndPercentageAggregationInDay(totalSeverity.toInt, totalEvents.toInt, TypeIncidentEnum.ALL_DATA.getId, severityArray.apply(j).getId, "CLUSTER", yesterday, dayWeek.getDay)
                for (q <- 0 to querysSeverity.size() - 1) {
                  println(querysSeverity.get(q))
                  session.execute(querysSeverity.get(q))
                }
              }
            } else if (severityArray.apply(j).equals(SeverityEnum.ALL_DATA)) {
              if (!typeIncidentArray.apply(i).equals(TypeIncidentEnum.ALL_DATA)) {
                val querysType = aggregationAuxiliar.totalAndPercentageAggregationInDay(totalIncidentType.toInt, totalEvents.toInt, typeIncidentArray.apply(i).getId, SeverityEnum.ALL_DATA.getId, "CLUSTER", yesterday, dayWeek.getDay)
                for (q <- 0 to querysType.size() - 1) {
                  println(querysType.get(q))
                  session.execute(querysType.get(q))
                }
              }

            } else {
              println("rdd is empty?? --->" + rddIncidentPerSeverity.isEmpty())
              if (!rddIncidentPerSeverity.isEmpty()) {
                val totalIncidentPerSeverity = rdd.filter { row => (row.getInt("type") == typeIncidentArray.apply(i).getId && row.getInt("severity") == severityArray.apply(j).getId) }.count()
                val querysType = aggregationAuxiliar.totalAndPercentageAggregationInDay(totalIncidentPerSeverity.toInt, totalEvents.toInt, typeIncidentArray.apply(i).getId, severityArray.apply(j).getId, "CLUSTER", yesterday, dayWeek.getDay)
                for (q <- 0 to querysType.size() - 1) {
                  println(querysType.get(q))
                  session.execute(querysType.get(q))
                }
                println("-----------------SUM AND MAX LENGTH, TIME------------------")
                val querysSumMax = aggregationAuxiliar.sumAndMaxAggregationInDayTotal(rddIncidentPerSeverity, typeIncidentArray.apply(i).getId, severityArray.apply(j).getId, "CLUSTER", yesterday, dayWeek.getDay)
                for (j <- 0 to querysSumMax.size() - 1) {
                  session.execute(querysSumMax.get(j))
                }

              }

            }

          }

        }

      }

      println("-----------------------NO DAY------------------------------")

      val valuesMaxAndSum = aggregationAuxiliar.getMaxAndTotalDelaysTotal(rdd)
      val maxLengthDelay = valuesMaxAndSum.get(0)
      val totalLengthDelay = valuesMaxAndSum.get(1)
      val maxTimeDelay = valuesMaxAndSum.get(2)
      val totalTimeDealy = valuesMaxAndSum.get(3)

      println("-------------------- TOTAL JAMS NO DAY--------------------")
      val rddTotalJamsNoDay = sc.cassandraTable("geodb", "aggregation").where(aggregationService.generateWhereAggregation(TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, "CLUSTER", KeyEnum.TOTAL.getId))
      val queryTotalJamsNoDay = aggregationAuxiliar.totalsAggregationWithoutDay(rddTotalJamsNoDay, totalEvents.toInt, TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, "CLUSTER", KeyEnum.TOTAL.getId)
      println(queryTotalJamsNoDay)
      session.execute(queryTotalJamsNoDay)

      println("-------------------- TOTAL METRES DELAY NO DAY-------------")
      val rddTotalMetresDelayNoDay = sc.cassandraTable("geodb", "aggregation").where(aggregationService.generateWhereAggregation(TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, "CLUSTER", KeyEnum.TOTAL_M.getId))
      val queryTotalMetresDelayNoDay = aggregationAuxiliar.totalsAggregationWithoutDay(rddTotalMetresDelayNoDay, totalLengthDelay.toInt, TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, "CLUSTER", KeyEnum.TOTAL_M.getId)
      println(queryTotalMetresDelayNoDay)
      session.execute(queryTotalMetresDelayNoDay)

      println("-------------------- TOTAL TIME DELAY NO DAY-------------")
      val rddTotalTimeDelayNoDay = sc.cassandraTable("geodb", "aggregation").where(aggregationService.generateWhereAggregation(TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, "CLUSTER", KeyEnum.TOTAL_SEC.getId))
      val queryTotalTimeDelayNoDay = aggregationAuxiliar.totalsAggregationWithoutDay(rddTotalTimeDelayNoDay, totalTimeDealy.toInt, TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, "CLUSTER", KeyEnum.TOTAL_SEC.getId)
      println(queryTotalTimeDelayNoDay)
      session.execute(queryTotalTimeDelayNoDay)

      println("-------------------- MAX METRES DELAY NO DAY------------")
      val rddMaxMetresDelayNoDay = sc.cassandraTable("geodb", "aggregation").where(aggregationService.generateWhereAggregation(TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, "CLUSTER", KeyEnum.MAX_M.getId))
      val queryMaxMetresDelayNoDay = aggregationAuxiliar.maxAggregationWithoutDay(rddMaxMetresDelayNoDay, maxLengthDelay.toInt, TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, "CLUSTER", KeyEnum.MAX_M.getId)
      println(queryMaxMetresDelayNoDay)
      session.execute(queryMaxMetresDelayNoDay)

      println("-------------------- MAX TIME DELAY NO DAY------------")
      val rddMaxTimeDelayNoDay = sc.cassandraTable("geodb", "aggregation").where(aggregationService.generateWhereAggregation(TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, "CLUSTER", KeyEnum.MAX_SEC.getId))
      val queryMaxTimeDelayNoDay = aggregationAuxiliar.maxAggregationWithoutDay(rddMaxTimeDelayNoDay, maxTimeDelay.toInt, TypeIncidentEnum.ALL_DATA.getId, SeverityEnum.ALL_DATA.getId, "CLUSTER", KeyEnum.MAX_SEC.getId)
      println(queryMaxTimeDelayNoDay)
      session.execute(queryMaxTimeDelayNoDay)

      session.close()

    }

  }
}