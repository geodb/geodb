package com.paradigma.processing.auxiliar

import com.datastax.spark.connector.rdd.CassandraTableScanRDD
import com.datastax.spark.connector.CassandraRow
import org.apache.spark.rdd.RDD
import com.paradigma.utility.model._
import com.paradigma.utility.service._
import java.util.ArrayList
import org.apache.spark.rdd.RDD.numericRDDToDoubleRDDFunctions


class AggregationAuxiliar {

  def totalsAggregationWithoutDay(rdd: CassandraTableScanRDD[CassandraRow], value: Int, typeIncident: Int, severity: Int, cellId: String, key: String): String = {
    val aggregationService = new AggregationService()
    val countRdd = rdd.count().toInt
    if (countRdd == 0) {
      val query = aggregationService.generateInsertAggregation(typeIncident, severity, value.toInt, cellId, key, -1, "noday")
      query.toString()
    } else {
      val totalSelect = rdd.map { row => row.getInt("value") }.first()
      println("totalSelect: " + totalSelect + ", totalValue: " + value.toInt)
      val valueUpdate = value.toInt + totalSelect.toInt
      val queryUpdate = aggregationService.generateUpdateAggregation(valueUpdate, typeIncident, severity, cellId, key)
      queryUpdate.toString()
    }
  }

  def maxAggregationWithoutDay(rdd: CassandraTableScanRDD[CassandraRow], value: Int, typeIncident: Int, severity: Int, cellId: String, key: String): String = {
    val aggregationService = new AggregationService()
    val countRdd = rdd.count().toInt
    if (countRdd == 0) {
      val query = aggregationService.generateInsertAggregation(typeIncident, severity, value.toInt, cellId, key, -1, "noday")
      query.toString()
    } else {
      val maxSelect = rdd.map { row => row.getInt("value") }.first()
      println("maxSelect: " + maxSelect + ", totalValue: " + value.toInt)
      var valueUpdate = 0
      if (value > maxSelect) {
        valueUpdate = value.toInt
      } else {
        valueUpdate = maxSelect.toInt
      }

      val queryUpdate = aggregationService.generateUpdateAggregation(valueUpdate, typeIncident, severity, cellId, key)
      queryUpdate.toString()
    }
  }

  def totalAndPercentageAggregationInDay(count: Int, totalEvents: Int, typeIncident: Int, severity: Int, cellId: String, day: Int, dayWeek: String): ArrayList[String] = {
    val aggregationService = new AggregationService()
    val result = new ArrayList[String]();
    var percentage = 0.0
    if (totalEvents != 0) {
      percentage = (count.floatValue() / totalEvents.floatValue()) * 100
    }
    println("Severity : " + severity + "type: " + typeIncident + ", total severity: " + count + ", percentage: " + percentage)

    val queryTotal = aggregationService.generateInsertAggregation(typeIncident, severity, count.toDouble, cellId, KeyEnum.TOTAL.getId, day, dayWeek)
    val queryPercentage = aggregationService.generateInsertAggregation(typeIncident, severity, percentage.toDouble, cellId, KeyEnum.PERCENTAGE.getId, day, dayWeek)
    result.add(queryTotal)
    result.add(queryPercentage)
    result
  }

  /*def totalAggregationInDay(count: Int, typeIncident: Int, severity: Int, cellId: String, day: Int, dayWeek: String): String = {
    val aggregationService = new AggregationService()
    println("Severity : " + severity + "type: " + typeIncident + ", total: " + count)

    val queryTotal = aggregationService.generateInsertAggregation(typeIncident, severity, count.toDouble, cellId, KeyEnum.TOTAL.getId, day, dayWeek)
    queryTotal
  }*/

  def sumAndMaxAggregationInDayTotal(rdd: RDD[CassandraRow], typeIncident: Int, severity: Int, cellId: String, day: Int, dayWeek: String): ArrayList[String] = {
    val aggregationService = new AggregationService()
    val result = new ArrayList[String]();
    val valuesMaxAndSum = getMaxAndTotalDelaysTotal(rdd)

    val maxLengthDelay = valuesMaxAndSum.get(0)
    val totalLengthDelay = valuesMaxAndSum.get(1)
    val queryMaxLengthDelay = aggregationService.generateInsertAggregation(typeIncident, severity, maxLengthDelay.toDouble, cellId, KeyEnum.MAX_M.getId, day, dayWeek)
    val queryTotalLengthDelay = aggregationService.generateInsertAggregation(typeIncident, severity, totalLengthDelay.toDouble, cellId, KeyEnum.TOTAL_M.getId, day, dayWeek)
    println("max delay " + maxLengthDelay + ", total delay :" + totalLengthDelay)

    val maxTimeDelay = valuesMaxAndSum.get(2)
    val totalTimeDealy = valuesMaxAndSum.get(3)
    val queryMaxTimeDealy = aggregationService.generateInsertAggregation(typeIncident, severity, maxTimeDelay.toDouble, cellId, KeyEnum.MAX_SEC.getId, day, dayWeek)
    val queryTotalTimeDealy = aggregationService.generateInsertAggregation(typeIncident, severity, totalTimeDealy.toDouble, cellId, KeyEnum.TOTAL_SEC.getId, day, dayWeek)

    println("max time delay " + maxTimeDelay + ", total time delay: " + totalTimeDealy)
    result.add(queryMaxLengthDelay)
    result.add(queryTotalLengthDelay)
    result.add(queryMaxTimeDealy)
    result.add(queryTotalTimeDealy)
    result
  }

  def getMaxAndTotalDelaysTotal(rdd: RDD[CassandraRow]): ArrayList[Int] = {
    val result = new ArrayList[Int]();

    val lengthDelayRdd = rdd.map { row => row.getInt("lengthdelay") }
    val timeDelayRdd = rdd.map { row => row.getInt("timedelay") }

    if (!timeDelayRdd.isEmpty()) {
      result.add(timeDelayRdd.max())
      result.add(timeDelayRdd.sum().toInt)
    } else {
      result.add(0)
      result.add(0)
    }
    if (!lengthDelayRdd.isEmpty()) {
      result.add(lengthDelayRdd.max())
      result.add(lengthDelayRdd.sum().toInt)
    } else {
      result.add(0)
      result.add(0)
    }

    result
  }

}