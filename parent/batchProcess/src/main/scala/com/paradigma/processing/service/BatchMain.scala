package com.paradigma.processing.service

import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import com.paradigma.processing.factory.SparkConfFactory
import org.apache.spark.SparkContext
import com.paradigma.processing.service._


object BatchMain {
   def main(args: Array[String]) {

    lazy val sc = new SparkContext(SparkConfFactory.sparkConf)
    
    val aggregationProcess = new AggregationProcess()
    val weatherProcess = new WeatherProcess()
    val predictionProcess = new PredictionProcess()
    
    /*val prueba = new TrainingDataProcess()
    prueba.trainingDataProcess22()*/
    
    //aggregationProcess.aggregationProcess(sc)
    //weatherProcess.weatherProcess(sc)
    predictionProcess.predictionProcess(sc)
    
   }
}