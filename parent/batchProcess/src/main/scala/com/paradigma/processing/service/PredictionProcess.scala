package com.paradigma.processing.service

import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import java.util.Calendar
import com.paradigma.processing.factory.SparkConfFactory
import org.apache.spark.SparkContext
import com.paradigma.utility.model.cassandra.EventVO
import com.paradigma.utility.service.CellService
import com.paradigma.utility.model._
import com.datastax.spark.connector._
import com.datastax.spark.connector.rdd._
import org.apache.spark.api.java._
import org.apache.spark.rdd.RDD
import java.util.ArrayList
import java.util.UUID
import java.text._
import java.util.Date
import com.paradigma.utility.service.AggregationService
import org.glassfish.grizzly.http.server.Session
import com.paradigma.processing.auxiliar.AggregationAuxiliar
import com.paradigma.utility.model._
import com.paradigma.utility.model.cassandra.ModelDataVO
import com.paradigma.utility.model.cassandra.TrainingDataGlobalVO
import com.paradigma.utility.model.cassandra.WeatherVO
import com.paradigma.utility.model.cassandra.TrainingDataWithWeatherVO
import org.apache.spark.mllib.linalg._
import org.apache.spark.mllib.classification.LogisticRegressionModel
import org.apache.spark.mllib.regression.LinearRegressionModel


class PredictionProcess {
  def predictionProcess(sc: SparkContext) {

    val cal = Calendar.getInstance()
    val year = cal.get(Calendar.YEAR) * 1000
    val today = year + cal.get(Calendar.DAY_OF_YEAR)
    val tomorrow = today+1

    println("day of year " + today)

    val rddTrainingDataCassandra = sc.cassandraTable("geodb", "trainingdata")
    val rddModelDataCassandra = sc.cassandraTable("geodb", "modeldata")
    val rddCellWithAccident = sc.cassandraTable("geodb", "cellwithaccident")

    val rddModelData = obtainModelData(rddModelDataCassandra)
    val rddLinear = rddModelData.filter { x => x.getPrevisionType.equals(PrevisionsEnum.METRESDELAY.getId) }
    val rddLogistic = rddModelData.filter { x => x.getPrevisionType.equals(PrevisionsEnum.ACCIDENT.getId) }

    CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
      session.execute("USE geodb;")
      val listCell = rddCellWithAccident.map { row => row.getString("cellid") }.distinct().collect()
      for (i <- 0 to listCell.size - 1) {
        println("CELL ->" + listCell.apply(i))

        val rddLinearCell = rddLinear.filter { x => x.getCellId.equals(listCell.apply(i)) }
        val rddLogisticCell = rddLogistic.filter { x => x.getCellId.equals(listCell.apply(i)) }

        var logisticPredict = 0.0
        var linearPredict = 0.0

        val rddCellDataTrainingTodayToPrecit = obtainDataTrainingWithWeather(rddTrainingDataCassandra.filter { x => x.getString("cellid").equals(listCell.apply(i)) && x.getInt("day") == today })

        val vectorTestData = rddTrainingdataToVector(rddCellDataTrainingTodayToPrecit)

        if (!rddLinearCell.isEmpty()) {
          val vectorWeightsLinearModelCell = rddModeldataToWeight(rddLinearCell)
          val linearModelCell = new LinearRegressionModel(vectorWeightsLinearModelCell, rddLinearCell.first().getIntercept)
          linearPredict = linearModelCell.predict(vectorTestData).toDouble

        }
        if (!rddLogisticCell.isEmpty()) {
          val vectorWeightsLogisticModelCell = rddModeldataToWeight(rddLogisticCell)
          val logisticModelCell = new LogisticRegressionModel(vectorWeightsLogisticModelCell, rddLogisticCell.first().getIntercept).clearThreshold()
          logisticPredict = logisticModelCell.predict(vectorTestData).toDouble
        }

        println("celda: " + listCell.apply(i) + ", accident? " + logisticPredict + ", metres delay: " + linearPredict)

        session.execute("INSERT INTO prevision(previsiontype, cellid, value, day) VALUES ('" +PrevisionsEnum.ACCIDENT.getId+ "', '" +listCell.apply(i)+ "', " +logisticPredict+ "," +tomorrow+ ")")
        session.execute("INSERT INTO prevision(previsiontype, cellid, value, day) VALUES ('" +PrevisionsEnum.METRESDELAY.getId+ "', '" +listCell.apply(i)+ "', " +linearPredict+ "," +tomorrow+ ")")
      }

      
      session.close()

    }

  }

  def obtainModelData(rdd: CassandraTableScanRDD[CassandraRow]): RDD[ModelDataVO] = {
    var rddModelData = rdd.map { row =>
      val modelData = new ModelDataVO()
      modelData.setDay(row.getInt("day"))
      modelData.setCellId(row.getString("cellid"))
      modelData.setPerformance(row.getDouble("performance"))
      modelData.setPrevisionType(row.getString("previsiontype"))
      modelData.setApparentTemperatureMax(row.getDouble("apparenttemperaturemax"))
      modelData.setApparentTemperatureMaxTime(row.getDouble("apparenttemperaturemaxtime"))
      modelData.setApparentTemperatureMin(row.getDouble("apparenttemperaturemintime"))
      modelData.setApparentTemperatureMinTime(row.getDouble("apparenttemperaturemintime"))
      modelData.setAvgApparentTemperature(row.getDouble("avgapparenttemperature"))
      modelData.setAvgCloudCover(row.getDouble("avgcloudcover"))
      modelData.setAvgDewPoint(row.getDouble("avgdewpoint"))
      modelData.setAvgHumidity(row.getDouble("avghumidity"))
      modelData.setAvgOzone(row.getDouble("avgozone"))
      modelData.setAvgPrecipIntensity(row.getDouble("avgprecipintensity"))
      modelData.setAvgPrecipProbability(row.getDouble("avgprecipprobability"))
      modelData.setAvgPressure(row.getDouble("avgpressure"))
      modelData.setAvgTemperature(row.getDouble("avgtemperature"))
      modelData.setAvgWindBearing(row.getDouble("avgwindbearing"))
      modelData.setAvgWindSpeed(row.getDouble("avgwindspeed"))
      modelData.setDayWorkable(row.getDouble("dayworkable"))
      modelData.setDelayT3(row.getDouble("delayt3"))
      modelData.setIntercept(row.getDouble("intercept"))
      modelData.setNumAccidentT3(row.getDouble("numaccidentt3"))
      modelData.setNumTrafficJamsT3(row.getDouble("numtrafficjamst3"))
      modelData.setPrecipIntensityMax(row.getDouble("precipintensitymax"))
      modelData.setPrecipIntensityMaxTime(row.getDouble("precipintensitymaxtime"))
      modelData.setPrecipType_hail(row.getDouble("preciptype_hail"))
      modelData.setPrecipType_rain(row.getDouble("preciptype_rain"))
      modelData.setPrecipType_sleet(row.getDouble("preciptype_sleet"))
      modelData.setPrecipType_snow(row.getDouble("preciptype_snow"))
      modelData.setSunriseTime(row.getDouble("sunrisetime"))
      modelData.setSunsetTime(row.getDouble("sunsettime"))
      modelData.setTemperatureMax(row.getDouble("temperaturemax"))
      modelData.setTemperatureMaxTime(row.getDouble("temperaturemaxtime"))
      modelData.setTemperatureMin(row.getDouble("temperaturemin"))
      modelData.setTemperatureMinTime(row.getDouble("temperaturemintime"))
      modelData.setVisibility(row.getDouble("visibility"))
      modelData
    }
    rddModelData

  }

  def obtainDataTrainingWithWeather(rdd: RDD[CassandraRow]): RDD[TrainingDataWithWeatherVO] = {
    var rddDataTraining = rdd.map { row =>
      val trainingData = new TrainingDataWithWeatherVO()
      trainingData.setCellId(row.getString("cellid"))
      trainingData.setApparentTemperatureMax(row.getDouble("apparenttemperaturemax"))
      trainingData.setApparentTemperatureMaxTime(row.getDouble("apparenttemperaturemaxtime"))
      trainingData.setApparentTemperatureMin(row.getDouble("apparenttemperaturemin"))
      trainingData.setApparentTemperatureMinTime(row.getDouble("apparenttemperaturemintime"))
      trainingData.setAvgApparentTemperature(row.getDouble("avgapparenttemperature"))
      trainingData.setAvgCloudCover(row.getDouble("avgcloudcover"))
      trainingData.setAvgDewPoint(row.getDouble("avgdewpoint"))
      trainingData.setAvgHumidity(row.getDouble("avghumidity"))
      trainingData.setAvgOzone(row.getDouble("avgozone"))
      trainingData.setAvgPrecipIntensity(row.getDouble("avgprecipintensity"))
      trainingData.setAvgPrecipProbability(row.getDouble("avgprecipprobability"))
      trainingData.setAvgPressure(row.getDouble("avgpressure"))
      trainingData.setAvgTemperature(row.getDouble("avgtemperature"))
      trainingData.setAvgWindBearing(row.getDouble("avgwindbearing"))
      trainingData.setAvgWindSpeed(row.getDouble("avgwindspeed"))
      trainingData.setPrecipIntensityMax(row.getDouble("precipintensitymax"))
      trainingData.setPrecipIntensityMaxTime(row.getDouble("precipintensitymaxtime"))
      trainingData.setPrecipType_hail(row.getDouble("preciptype_hail"))
      trainingData.setPrecipType_rain(row.getDouble("preciptype_rain"))
      trainingData.setPrecipType_sleet(row.getDouble("preciptype_sleet"))
      trainingData.setPrecipType_snow(row.getDouble("preciptype_snow"))
      trainingData.setSunriseTime(row.getDouble("sunrisetime"))
      trainingData.setSunsetTime(row.getDouble("sunsettime"))
      trainingData.setTemperatureMax(row.getDouble("temperaturemax"))
      trainingData.setTemperatureMaxTime(row.getDouble("temperaturemaxtime"))
      trainingData.setTemperatureMin(row.getDouble("temperaturemin"))
      trainingData.setTemperatureMinTime(row.getDouble("temperaturemintime"))
      trainingData.setVisibility(row.getDouble("visibility"))
      trainingData.setNumAccidentT3(row.getDouble("numaccidentt3"))
      trainingData.setNumTrafficJamsT3(row.getDouble("numtrafficjamst3"))
      trainingData.setDelayT3(row.getDouble("delayt3"))
      trainingData.setDayWorkable(row.getDouble("dayworkable"))
      //trainingData.setTargetAccident(row.getDouble("targetaccident"))
      //trainingData.setTargetDelay(row.getDouble("targetdelay"))
      trainingData

    }

    rddDataTraining;
  }

  def obtainGlobalDataTraining(rdd: RDD[CassandraRow]): RDD[TrainingDataGlobalVO] = {

    var rddGlobalData = rdd.map { row =>
      val trainingGlobalData = new TrainingDataGlobalVO()
      trainingGlobalData.setCellId(row.getString("cellid"))
      trainingGlobalData.setAvgAccident(row.getDouble("avgaccident"))
      trainingGlobalData.setAvgDelay(row.getDouble("avgdelay"))
      trainingGlobalData.setAvgJam(row.getDouble("avgjam"))
      trainingGlobalData.setPercentageIncident(row.getDouble("percentageincident"))
      trainingGlobalData

    }

    rddGlobalData
  }

  def rddModeldataToWeight(rdd: RDD[ModelDataVO]): Vector = {
    val modelData = rdd.first()
    val arrayWeight = Array(modelData.getSunsetTime, modelData.getTemperatureMinTime, modelData.getPrecipIntensityMaxTime, modelData.getVisibility,
      modelData.getPrecipIntensityMax, modelData.getApparentTemperatureMaxTime, modelData.getTemperatureMaxTime, modelData.getApparentTemperatureMinTime,
      modelData.getTemperatureMin, modelData.getPrecipType_rain, modelData.getPrecipType_snow, modelData.getPrecipType_sleet, modelData.getPrecipType_hail, modelData.getSunriseTime,
      modelData.getApparentTemperatureMax, modelData.getApparentTemperatureMin, modelData.getTemperatureMax, modelData.getAvgPressure, modelData.getAvgCloudCover,
      modelData.getAvgApparentTemperature, modelData.getAvgPrecipIntensity, modelData.getAvgTemperature, modelData.getAvgDewPoint, modelData.getAvgOzone, modelData.getAvgWindSpeed,
      modelData.getAvgHumidity, modelData.getAvgWindBearing, modelData.getAvgPrecipProbability, modelData.getNumAccidentT3, modelData.getNumTrafficJamsT3, modelData.getDelayT3,
      modelData.getDayWorkable)
    val vectorWeight = Vectors.dense(arrayWeight)
    vectorWeight
  }
  def rddTrainingdataToVector(rddTrainingToday: RDD[TrainingDataWithWeatherVO]): Vector = {
    val trainingToday = rddTrainingToday.first()
    val arrayTestData = Array(trainingToday.getSunsetTime, trainingToday.getTemperatureMinTime, trainingToday.getPrecipIntensityMaxTime, trainingToday.getVisibility,
      trainingToday.getPrecipIntensityMax, trainingToday.getApparentTemperatureMaxTime, trainingToday.getTemperatureMaxTime, trainingToday.getApparentTemperatureMinTime,
      trainingToday.getTemperatureMin, trainingToday.getPrecipType_rain, trainingToday.getPrecipType_snow, trainingToday.getPrecipType_sleet, trainingToday.getPrecipType_hail, trainingToday.getSunriseTime,
      trainingToday.getApparentTemperatureMax, trainingToday.getApparentTemperatureMin, trainingToday.getTemperatureMax, trainingToday.getAvgPressure, trainingToday.getAvgCloudCover,
      trainingToday.getAvgApparentTemperature, trainingToday.getAvgPrecipIntensity, trainingToday.getAvgTemperature, trainingToday.getAvgDewPoint, trainingToday.getAvgOzone, trainingToday.getAvgWindSpeed,
      trainingToday.getAvgHumidity, trainingToday.getAvgWindBearing, trainingToday.getAvgPrecipProbability, trainingToday.getNumAccidentT3, trainingToday.getNumTrafficJamsT3, trainingToday.getDelayT3,
      trainingToday.getDayWorkable)
    val vectorTestData = Vectors.dense(arrayTestData)
    vectorTestData
  }
}