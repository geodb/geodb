package com.paradigma.processing.service

import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import com.datastax.spark.connector._
import com.datastax.spark.connector.cql.CassandraConnector
import java.util.Calendar
import com.paradigma.processing.factory.SparkConfFactory
import org.apache.spark.SparkContext
import com.paradigma.utility.model.cassandra._
import com.paradigma.utility.service._
import com.paradigma.utility.model._
import com.paradigma.processing.service.TrainingDataProcess
import java.util.ArrayList
import java.util.UUID
import java.text._
import java.util.Date
import org.glassfish.grizzly.http.server.Session


class WeatherProcess {
  def weatherProcess(sc: SparkContext) {

    val trainingDataProcess= new TrainingDataProcess()
    val weatherService = new WeatherService()
    val cellService = new CellService()

    val cal = Calendar.getInstance()
    val year = cal.get(Calendar.YEAR)*1000
    val tomorrow = year+cal.get(Calendar.DAY_OF_YEAR)+1

    println("day of year " + tomorrow)

    val cellsIdInMap = cellService.obtainCellsInMap()
    CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
        session.execute("USE geodb;")
    for (i <- 0 to cellsIdInMap.size()- 1) {
        val cql = "SELECT * FROM clustermap where cellid='"+cellsIdInMap.get(i)+"';"
        println(cql)
        val resultSet = session.execute("SELECT * FROM clustermap where cellid='"+cellsIdInMap.get(i)+"';")
        
        val row = resultSet.one()
        val x = row.getDouble("longitud")
        val y = row.getDouble("latitud")
        val centerInCell= cellService.obtainCenter(x, y)
        val weatherVO = weatherService.calculateWeatherInCell(cellsIdInMap.get(i), cellService.getXwithFactor(centerInCell.get("x")), cellService.getYwithFactor(centerInCell.get("y")), tomorrow)

        val insertWeather=weatherService.generateInsertWeather(weatherVO)
        println(insertWeather)
        println("---------------")
        session.execute(insertWeather)
        session.close()
        trainingDataProcess.trainingDataProcess(sc, weatherVO)

      }
    }

  }
}