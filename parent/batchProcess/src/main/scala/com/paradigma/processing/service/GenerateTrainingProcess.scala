package com.paradigma.processing.service

import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import com.datastax.spark.connector._
import org.apache.spark.rdd.RDD
import com.datastax.spark.connector.cql.CassandraConnector
import com.paradigma.processing.factory.SparkConfFactory
import org.apache.spark.SparkContext
import com.paradigma.processing.service._
import com.paradigma.utility.model.cassandra._
import java.util.Calendar
import com.paradigma.processing.auxiliar.TrainingDataAuxiliar
import com.paradigma.utility.service.TrainingDataService
import com.paradigma.utility.service.CellService


object GenerateTrainingProcess {
  def main(args: Array[String]) {
    lazy val sc = new SparkContext(SparkConfFactory.sparkConf)

    val trainingDataService = new TrainingDataService()
    val cal = Calendar.getInstance()
    val year = cal.get(Calendar.YEAR) * 1000
    val today = year+cal.get(Calendar.DAY_OF_YEAR)-2
    var numDayOfWeek = cal.get(Calendar.DAY_OF_WEEK)
    if (numDayOfWeek == 7) {
      numDayOfWeek = 1;
    } else {
      numDayOfWeek = numDayOfWeek + 1;
    }

    val trainingDataAux = new TrainingDataAuxiliar()

    val rddWeatherCassandra = sc.cassandraTable("geodb", "weather")

    val rddAggregation = sc.cassandraTable("geodb", "aggregation")

    val daysWiew = rddAggregation.map { row => row.getInt("day") }.distinct().count()

    //val arrayDays = rddAggregation.map { row => row.getInt("day") }.distinct().collect()
    val arrayCells = rddWeatherCassandra.map { row => row.getString("cellid") }.distinct().collect()
    //for (i <- 0 to arrayDays.length - 1) {
      //println("day : " + arrayDays.apply(i))
      for (j <- 0 to arrayCells.length - 1) {
        println("cell : " + arrayCells.apply(j))
        /* val rddWeatherByDay = rddWeatherCassandra.filter { row => row.getInt("day") == arrayDays.apply(i) }
        val rddWeather = obtainWeatherByDay(rddWeatherCassandra)
        var weather = rddWeather.filter { row => row.getCellId.equals(arrayCells.apply(j)) }.first()
        weather.setDay(arrayDays.apply(i))
        val rddGlobaldata = sc.cassandraTable("geodb", "trainingdata_global").where("cellid = '" + weather.getCellId + "'")
       */ val cellRdd = rddAggregation.filter { row => row.getString("cellid").equals(arrayCells.apply(j)) && row.getInt("day") == today }
        println("tamaño cellRDD: " + cellRdd.count())
        if (cellRdd.count() > 0) {
          // val trainingDataGlobal = trainingDataAux.obtainTraininDataGlobal(cellRdd, weather.getCellId, daysWiew.toInt)

          /*val t3Rdd = cellRdd.filter { row => row.getInt("day") >= arrayDays.apply(i).toInt - 3 && row.getInt("day") <= arrayDays.apply(i).toInt - 1 }
        val trainingDataWithoutWeather = trainingDataAux.obtainTrainingDataT3(t3Rdd, arrayDays.apply(i), numDayOfWeek, weather.getCellId)
*/
          //val yesterdayRdd = cellRdd.filter { row => row.getInt("day") == arrayDays.apply(i).toInt - 1 }
          //println("tamaño yesterdayRDD: " + yesterdayRdd.count())
          val trainingDataTargets = trainingDataAux.obtainTrainingDataTargets(cellRdd, today, arrayCells.apply(j))

          /* val insertTrainingData = trainingDataService.generateInsertTrainingData(weather, trainingDataWithoutWeather)
        val deleteOldTrainingDataGlobal = trainingDataService.generateDeleteOldTrainingDataGlobal(trainingDataGlobal)
        val insertTrainingDataGlobal = trainingDataService.generateInsertTrainingDataGlobal(trainingDataGlobal)*/
          val updateTrainingDataTargets = trainingDataService.generateUpdateTargetsTrainingData(trainingDataTargets)

          CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
            session.execute("USE geodb;")
            /*println(insertTrainingData)
          session.execute(insertTrainingData)
          println(deleteOldTrainingDataGlobal)
          session.execute(deleteOldTrainingDataGlobal)
          println(insertTrainingDataGlobal)
          session.execute(insertTrainingDataGlobal)*/

            val rddTrainingDataCassandra = sc.cassandraTable("geodb", "trainingdata")
            val checkRdd = rddTrainingDataCassandra.filter { row => row.getString("cellid").equals(arrayCells.apply(j)) && row.getInt("day") == today }
            if (!checkRdd.isEmpty()) {
              println(updateTrainingDataTargets)
              session.execute(updateTrainingDataTargets)
            }
            println("***********************")
            session.close()
          }
        }
      }
    //}
  }

  def obtainWeatherByDay(rdd: RDD[CassandraRow]): RDD[WeatherVO] = {
    var rddWeather = rdd.map { row =>
      val weather = new WeatherVO()
      weather.setCellId(row.getString("cellid"))
      weather.setApparentTemperatureMax(row.getDouble("apparenttemperaturemax"))
      weather.setApparentTemperatureMaxTime(row.getDouble("apparenttemperaturemaxtime"))
      weather.setApparentTemperatureMin(row.getDouble("apparenttemperaturemin"))
      weather.setApparentTemperatureMinTime(row.getDouble("apparenttemperaturemintime"))
      weather.setAvgApparentTemperature(row.getDouble("avgapparenttemperature"))
      weather.setAvgCloudCover(row.getDouble("avgcloudcover"))
      weather.setAvgDewPoint(row.getDouble("avgdewpoint"))
      weather.setAvgHumidity(row.getDouble("avghumidity"))
      weather.setAvgOzone(row.getDouble("avgozone"))
      weather.setAvgPrecipIntensity(row.getDouble("avgprecipintensity"))
      weather.setAvgPrecipProbability(row.getDouble("avgprecipprobability"))
      weather.setAvgPressure(row.getDouble("avgpressure"))
      weather.setAvgTemperature(row.getDouble("avgtemperature"))
      weather.setAvgWindBearing(row.getDouble("avgwindbearing"))
      weather.setAvgWindSpeed(row.getDouble("avgwindspeed"))
      weather.setPrecipIntensityMax(row.getDouble("precipintensitymax"))
      weather.setPrecipIntensityMaxTime(row.getDouble("precipintensitymaxtime"))
      weather.setPrecipType_hail(row.getDouble("preciptype_hail"))
      weather.setPrecipType_rain(row.getDouble("preciptype_rain"))
      weather.setPrecipType_sleet(row.getDouble("preciptype_sleet"))
      weather.setPrecipType_snow(row.getDouble("preciptype_snow"))
      weather.setSunriseTime(row.getDouble("sunrisetime"))
      weather.setSunsetTime(row.getDouble("sunsettime"))
      weather.setTemperatureMax(row.getDouble("temperaturemax"))
      weather.setTemperatureMaxTime(row.getDouble("temperaturemaxtime"))
      weather.setTemperatureMin(row.getDouble("temperaturemin"))
      weather.setTemperatureMinTime(row.getDouble("temperaturemintime"))
      weather.setVisibility(row.getDouble("visibility"))
      weather

    }

    rddWeather;

  }

}