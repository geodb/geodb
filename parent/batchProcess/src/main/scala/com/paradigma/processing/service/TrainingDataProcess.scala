package com.paradigma.processing.service

import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import com.datastax.spark.connector._
import org.apache.spark.rdd.RDD
import com.datastax.spark.connector.cql.CassandraConnector
import com.paradigma.processing.factory.SparkConfFactory
import org.apache.spark.SparkContext
import com.paradigma.processing.service._
import com.paradigma.utility.model.cassandra._
import java.util.Calendar
import com.paradigma.processing.auxiliar.TrainingDataAuxiliar
import com.paradigma.utility.service.TrainingDataService
import com.paradigma.utility.service.CellService


class TrainingDataProcess {
  def trainingDataProcess(sc: SparkContext, weather: WeatherVO) {

    val trainingDataService = new TrainingDataService()
    val cal = Calendar.getInstance()
    val year = cal.get(Calendar.YEAR)*1000
    val today = year+cal.get(Calendar.DAY_OF_YEAR)
    var numDayOfWeek = cal.get(Calendar.DAY_OF_WEEK)
    if (numDayOfWeek == 7) {
      numDayOfWeek = 1;
    } else {
      numDayOfWeek = numDayOfWeek + 1;
    }

    val trainingDataAux = new TrainingDataAuxiliar()

    val rddAggregation = sc.cassandraTable("geodb", "aggregation")

    val daysWiew = rddAggregation.map { row => row.getInt("day") }.distinct().count()

    val cellRdd = rddAggregation.filter { row => row.getString("cellid").equals(weather.getCellId) }
    println("tamaño cellRDD: "+cellRdd.count())

    val t3Rdd = cellRdd.filter { row => row.getInt("day") >= today.toInt - 3 && row.getInt("day") <= today.toInt - 1 }
    val trainingDataWithoutWeather = trainingDataAux.obtainTrainingDataT3(t3Rdd, today, numDayOfWeek, weather.getCellId)

    val yesterdayRdd = cellRdd.filter { row => row.getInt("day") == today.toInt - 1 }
    println("tamaño yesterdayRDD: "+yesterdayRdd.count())
    val trainingDataTargets = trainingDataAux.obtainTrainingDataTargets(yesterdayRdd, today - 1, weather.getCellId)

    val insertTrainingData = trainingDataService.generateInsertTrainingData(weather, trainingDataWithoutWeather)
    val updateTrainingDataTargets = trainingDataService.generateUpdateTargetsTrainingData(trainingDataTargets)

   CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
      session.execute("USE geodb;")
      println(insertTrainingData)
      session.execute(insertTrainingData)
      //println(updateTrainingDataTargets)
      //session.execute(updateTrainingDataTargets)
      println("***********************")
      session.close()
    }

  }
  def trainingDataProcess22() {
    val cellService = new CellService()
    
    CassandraConnector(SparkConfFactory.sparkConf).withSessionDo { session =>
      session.execute("USE geodb;")
      val cellinMap = cellService.obtainCellsInMap()
      for (i <- 0 to cellinMap.size()- 1) {
        //session.execute("delete from weather where cellid='"+cellinMap.get(i)+"' and day=2016030;")
        session.execute("delete from trainingdata where cellid='"+cellinMap.get(i)+"' and day=2016045;")

        
      }
    }
  }

}