package com.paradigma.processing.auxiliar


import com.datastax.spark.connector._
import org.apache.spark.rdd.RDD
import com.paradigma.processing.service._
import com.paradigma.utility.model.cassandra._


class TrainingDataAuxiliar {
  def obtainTrainingDataT3(t3Rdd : RDD[CassandraRow], today: Integer, numDayOfWeek: Integer, cellId: String) : TrainingDataWithoutWeatherVO={
    var trainingDataWithoutWeather= new TrainingDataWithoutWeatherVO()
    val rddIncidentNone= t3Rdd.filter{row =>row.getInt("severity") == -1}
    val totalAccidentRdd = rddIncidentNone.filter { row => row.getInt("type") == 3  || row.getInt("type")==8}.filter {row => row.getString("keydesc").equals("TOTAL") }.map { row => row.getDouble("value") }
    var totalAccident = 0.0
    if(!totalAccidentRdd.isEmpty()){
      totalAccident=totalAccidentRdd.sum()
    }
    val totalTrafficJamRdd = rddIncidentNone.filter { row => row.getInt("type") == 6 }.filter {row => row.getString("keydesc").equals("TOTAL") }.map { row => row.getDouble("value") }
    var totalTrafficJam=0.0
    if(!totalTrafficJamRdd.isEmpty()){
      totalTrafficJam=totalTrafficJamRdd.sum()
    }
    val totalMetresDelayRdd = t3Rdd.filter {row =>row.getInt("severity") == -1  &&  row.getInt("type") == -1  &&  row.getString("keydesc").equals("TOTAL_M") }.map { row => row.getDouble("value") }
    var totalMetresDelay = 0.0
    if(!totalMetresDelayRdd.isEmpty()){
      totalMetresDelay=totalMetresDelayRdd.sum()
    }
    
    trainingDataWithoutWeather.setCellId(cellId)
    trainingDataWithoutWeather.setDay(today.toInt+ 1)
    trainingDataWithoutWeather.setDelayT3(totalMetresDelay.toDouble/3)
    trainingDataWithoutWeather.setNumAccidentT3(totalAccident.toDouble/3)
    trainingDataWithoutWeather.setNumTrafficJamsT3(totalTrafficJam.toDouble/3)
    if(numDayOfWeek==1 || numDayOfWeek==7){
      trainingDataWithoutWeather.setDayWorkable(0.0)
    }
    else{
      trainingDataWithoutWeather.setDayWorkable(1.0)
    }
        
    trainingDataWithoutWeather
  }
  def obtainTrainingDataTargets(yesterdayRdd: RDD[CassandraRow], yesterday: Integer, cellId: String): TrainingDataWithoutWeatherVO={
    var trainingDataWithoutWeather= new TrainingDataWithoutWeatherVO()
    val totalMetresDelayRdd= yesterdayRdd.filter{row =>row.getInt("severity") == -1 && row.getInt("type") == -1 &&  row.getString("keydesc").equals("TOTAL_M")}.map { row => row.getDouble("value") }
    var totalMetresDelay = 0.0
    if(!totalMetresDelayRdd.isEmpty()){
      totalMetresDelay=totalMetresDelayRdd.sum()
    }
    val totalIncidentRdd= yesterdayRdd.filter{row =>row.getInt("severity") == -1 && row.getInt("type") == -1 &&  row.getString("keydesc").equals("TOTAL")}.map { row => row.getDouble("value") }
    var totalIncident = 0.0
    if(!totalIncidentRdd.isEmpty()){
      totalIncident=totalIncidentRdd.sum()
    }
    if(totalIncident!=0){
      totalMetresDelay=totalMetresDelay.toDouble/totalIncident.toDouble;
    }
    else{
      totalMetresDelay=0.0
    }
    
    val accidentRdd = yesterdayRdd.filter { row => (row.getInt("type")==3 || row.getInt("type")==8) && row.getInt("severity") == -1}
    var haveAccident=0
    if(!accidentRdd.isEmpty() && accidentRdd.count()>0){
      haveAccident=1
    }
    trainingDataWithoutWeather.setCellId(cellId)
    trainingDataWithoutWeather.setDay(yesterday)
    trainingDataWithoutWeather.setTargetDelay(totalMetresDelay)
    trainingDataWithoutWeather.setTargetAccident(haveAccident.toDouble)
    trainingDataWithoutWeather
  }
}